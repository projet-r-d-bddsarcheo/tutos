## Conseils, bugs et problèmes

### nettoyage les tables attributaire

Au sein de la table attributaire, vérifiez que les valeurs sont propres. Prenez le temps de les corriger si nécessaire. 
Si une valeur est vide, c'est à dire si votre champ n'affiche rien, cliquez dans le champ puis appuyez sur la touche suppr de votre clavier. La valeur *NULL* devrait apparaitre à la place. Ceci est nécessaire afin d'éviter des erreurs d'export. 

Si vous utilisez l'interface mobile bad'mobil, vous pouvez être confronté à des valeurs étranges, tel '()'. N'hésitez pas à les supprimer, bad'mobil ne traduisant pas tout à fait les valeurs prévues dans Qgis de la même manière. Ainsi, pour les champs booléen, Bad'mobil renseigne 1 ou 0, tandis que Qgis renseigne vrai ou faux

### Au secours, mon projet ne s'ouvre plus !!

Malheureusement, ca arrive. Ca peut être du à une couche mal lue, mal comprise, ce qui peut arriver en particulier avec les vues. 

Une solution a retenir, limitant la perte de données, est la suivante : 

1. Lancez votre projet et notez les couches dont Qgis dit qu'elles ne sont pas utilisables, puis fermez Qgis.  
2. **Renommez votre base de données badass, afin de casser le lien entre votre projet et la base**. Une légère modification du nom de fichier suffit (ajouter un 0 devant par exemple)
3. A l'ouverture de votre projet, Qgis va vous dire qu'il ne retrouve pas les couches. C'est exactement ce que l'on veut, car il s'agit **d'ouvrir le projet pour y apporter quelques modifications sans avoir à lire la donnée**. Vous passez donc outre le message d'alerte de Qgis en cliquant sur **"conserver les couches inutilisables"**.

![](ressources/couches_inutilisables.jpg)

3. Maintenant que le projet est ouvert, **supprimez les couches problématiques** dont vous avez noté le nom précédemment, en particulier celles sur lesquelles qgis bloque à l'ouverture du projet. 
4. **Enregistrez et fermez** votre projet. 
5. Rétablissez le lien avec la base de données en **renommant votre fichier de base de données** (en supprimant le 0 que vous avez mis devant dans l'exemple précédent).
6. Votre projet s'ouvre dès lors normalement, sauf si d'autres couches posent problème. Dans ce cas répétez les étapes 1 à 5. Une fois toutes les couches problématiques supprimées de votre projet, celui-ci s'ouvrira normalement. 
 
### Je n'accède pas au bouton ok de la fenêtre du formulaire

Maximisez la taille de votre fenêtre, soit en double cliquant sur la barre de titre, soit en cliquant sur l'icone carré de la barre de titre. Le bouton "Ok" sera alors accessible en bas à droite

### Je ne parviens pas à créer un nouvel enregistrement et à l'enregistrer. 

Le problème peut venir de la présence, en mode formulaire, d'un champ id_xxx, clé primaire de la table attributaire.  
Il est prévu, dans la base de données, que ces champs s'autoincrémentent automatiquement.   
Mais étrangement, si ils sont présents dans un formulaire, aucune numérotation automatique ne se produit et le champ reste vide. Et puisque ce champ ne peut pas être vide, Qgis refuse d'enregistrer votre donnée.  
Supprimez le champ id_xxx de votre formulaire devrait résoudre le problème. Il sera quand même complété automatiquement par la base de données. 

Ce comportement ne se produit pas en mode table. 

### Qgis me dit "Commit Error". Qu'est ce que ca veut dire ? 

En général, cela signifie qu'il ne parvient pas à enregistrer les modifications de la base de données, soit parce qu'un critère n'est pas respecté (unicité ou type de donnée par exemple), soit parce que la base est verouillée.  
Dans le premier cas, vérifier que les données que vous souhaitez enregistrer sont bien conformes (les chiffres sont des chiffres par exemple). Dans le second cas, vous n'avez pas d'autre choix que de fermer Qgis et de réouvrir par la suite. 

### Je ne parviens pas à exporter en CSV/ODS/XLSX. Qgis me renvoit une erreur. 

Si vous avez un message de ce type : 
>Erreur lors de la conversion de la valeur () pour le champ d'attribut epais : Impossible de convertir la valeur "" vers le type destination "float"

cela signifie que certains de vos champs (ici le champ epais) ont une valeur vide et non une valeur NULL.  
La différence entre *NULL* et vide est subtile. 
Une valeur NULL informe du fait que le champ ne contient rien.  
A contrario, une valeur vide ne porte aucune information.  
Et comme un champ doit contenir une information, s'il est vide, vous rencontrerez une erreur au moment des exports.   
En bref, si un champ ne contient pas de valeur, alors il doit afficher *NULL*. 
Pour corriger ceci dans qgis, il suffit de cliquer dans le champs concerné et de taper sur la touche "suppr" de votre clavier. 

### J'ai supprimé une couche par erreur du projet. Comment puis-je la réimporter ? 

Aïe...Bon, vous n'avez pas perdu de données, mais vous risquez d'avoir perdu vos styles...

Considérons que vous avez supprimé la couche t_axe de votre projet : 
- utilisez le bouton d'ajout de couche spatialite (petite plume avec un +), ou dans le menu Couche -> ajouter une couche -> ajoutez une couche spatialite
- Dans la nouvelle fenêtre qui s'ouvre, vous devez connecter la base de données (badass donc) avec le projet. 
- cliquez sur ***nouveau*** puis, dans la nouvelle fenêtre qui s'ouvre, choisissez votre base de données. Elle se trouve normalement juste à côté de votre projet. 
- une fois la base séléctionnée, cliquez sur ***connecter***. La liste de l'ensemble des tables présentes dans la base de données est affichée. 
- ***Cliquez sur la table souhaitée***, puis sur ***Ajouter***, en bas de la fenêtre. Si il s'agit d'une couche sans geométrie (l'inventaire des photos par exemple), cochez la case "Lister les tables sans géométrie".  
- La couche est ajoutée dans votre projet. 

![](ressources/fen_ajout_bdd.png)

