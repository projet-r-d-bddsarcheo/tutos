# Les six couches minimales d'une opération archéologique

La définition d'une structure de données minimales commune à toutes les opérations archéologiques vise, d'une part, à guider les utilisateurs dans la mise en oeuvre d'un SIG et d'autre part, à permettre la constitution d'un catalogue de données national répondant à une logique d'inventaire. 

**Il s'agit d'une structure minimale : l'utilisateur est libre d'ajouter autant de champs ou de couches de données supplémentaires que nécessaire** 

> NB : les valeurs des champs "typoly", "typoint" et "interpret" des couches numSGA_poly/numSGA_point ne font pas l'objet d'une harmonisation. Seules les données « primaires » - dont on ne peut discuter la forme et la localisation une fois l'opération terminée - sont concernées par cette structuration.

## Structure attributaire

- La liste des champs minimaux peut-être complétée en fonction de la problématique scientifique 
- Les noms des tables sont toujours précédés par le numéro SGA de l'opération. 
- les clés primaires sont en gras
- des exemples de valeur sont proposés en italique

### numSGA_emprise : 

Couche de polygone destinée à renseigner les emprises de l'opération

**Liste minimale de champs**

| champs    | contenu                                                                               | type  |
| --------- | ------------------------------------------------------------------------------------- | ----- |
| surface   | Surface de l’entité                                                                   | Réel  |
| annee     | année d’intervention terrain ou de dernière année si intervention pluriannuelle       | texte |
| ro        | nom prenom du ro (minuscules)                                                         | texte |
| **numope**    | numero sga de l’opération (*Dxxxx* ; *Fxxxx*)                                             | texte |
| nomope    | nom usuel de l’operation(lieu-dit, rue, ZAC,..) figurant sur l’arrêté de prescription | texte |
| typeope   | *bâti* ; *diagnostic* ; *fouille* ; *surveillance* ; *projet* ; *sauvetage* ; *évaluation*          | texte |
| typemp    | type d’emprise : prescrite ; réelle                                                   | texte |
| numoa     | numéro d’OA figurant sur l’arrêté de prescription                                     | texte |
| numprescr | numéro de l’arrêté de prescription figurant sur de dernier arrêté édité               | texte |

Une emprise prescrite est celle figurant à l'arrêté de prescription du SRA. Une emprise réelle est celle effectivement accessible aux équipes archéologiques. 

### numSGA_ouverture :

Couche de polygone renseignant tout creusement (sondage, tranchée...) réalisée à des fins d'observation ou zone d'observation (secteur)

| champs    | contenu                                                                               | type  |
| --------- | ------------------------------------------------------------------------------------- | ----- |
| **numouvert**   | numéro attribué à l'ouverture sur le terrain                                        | Entier (recommandé)  |
| tyouvert     | nature de l'ouverture réalisée : *tranchée* ; *sondage*; *fenêtre* ; *sondage profond* ; *carré* ; *décapage* ; *secteur* ; *pallier* | texte |

### numSGA_poly :

Couche de polygone renseignant toute unité d'observation archéologique (fait, us...)

| champs    | contenu                                                                    | type  |
| --------- | -------------------------------------------------------------------------- | ----- |
| **numpoly** | numéro attribué à l'unité archéologique sur le terrain                   | Entier (recommandé)  |
| typoly   | type de l'observation : *structure* ; *fait* ; *US*...                      | Texte  |
| interpret | interprétation : *fosse* ; *trou de poteau* ; *sépulture*                  | Texte  |
| datedebut | datation absolue inférieure, entier négatif ou positif, en année (-10 000) | Entier  |
| datefin   | datation absolue supérieure, entier négatif ou positif, en année (850)     | Entier  |

### numSGA_point :

Couche de point  renseignant les unités d'observation archéologiques isolées (souvent dédiées au mobilier)

| champs    | contenu                                                                    | type  |
| --------- | -------------------------------------------------------------------------- | ----- |
| **numpoint** | numéro attribué à l'isolat sur le terrain                      | Entier (recommandé)  |
| typoint   | type de donnée : *artefact* ; *écofact* ; *isolat* ; *lot* ...             | Texte  |
| interpret | interprétation : *céramique* ; *silex* ; *éclat* ; *lame*                  | Texte  |
| datedebut | datation absolue inférieure, entier négatif ou positif, en année (-10 000) | Entier  |
| datefin   | datation absolue supérieure, entier négatif ou positif, en année (850)     | Entier  |

### numSGA_axe :

Couche de ligne matérilisant les différents axes crées

| champs    | contenu                                                | type  |
| --------- | ------------------------------------------------------ | ----- |
| **numaxe** | numéro attribué à l'axe             | Entier (recommandé)  |
| typaxe   | nature de l'axe : *coupe* ; *profil* ...                | Texte  |

### numSGA_log :

Couche de point localisant l'emplacement des prélèvements, des logs geomorphologiques, des sondages profonds...

| champs    | contenu                                                | type  |
| --------- | ------------------------------------------------------ | ----- |
| **numlog** | numéro attribué au log                                   | Entier (recommandé)  |
| typlog   | type de données : *prélèvement* ; *log* ...                | Texte  |
| alti   | altitude de la donnée (m,  NGF)                              | Réel  |
| typalti   | information sur l'altitude : *sommet du log* ; *pélèvement* ...  | Texte  |

