![logo_badmobil](screenshot/BadMobil_small.png) **Interface web Bad'Mobil**



# <span style="color: #73a12d">***C'est quoi ?***</span>



Tout simplement, c'est une interface web de Badass.
L'interface *Bad'Mobil* a été developpée avec l'outil [SQLPage](https://sql.ophir.dev/).
*SQLPage* permet de créer des pages web en utilisant uniquement des requêtes SQL. Pas besoin d'avoir des connaissances spécifiques dans les languages HTML, PHP ou CSS. *SQLPage* a été créé par le développeur indépendant [Ophir Lojkine](https://github.com/lovasoa).

Il s'agit tout simplement d'un serveur web installé en local, lancé grâce à un simple fichier exécutable (sqlpage.exe). La connexion est effectué sur une base de données SQL au format PostgreSQL, MySQL ou encore SQLite. Ce dernier nous intéresse ici tout particulièrement car la base de données *Badass* s'appuie sur le moteur SQLite avec sa composante spatiale Spatialite. 

Le fonctionnement de SQLPage, et donc de l'interface *Bad'Mobil*, s'appuie sur un ensemble de fichier au format *.sql* qui permettent d'afficher des formulaires, de réaliser des requêtes, de créer de la données dans la base, ou encore de modifier une donnée existante...




# <span style="color: #73a12d">***Pourquoi ?***</span>



En gros, **pourquoi la création de *Bad'Mobil* ?**
*Bad'Mobil* répond à une lacune dans l'utilisation de la base de données Spatialite Badass. L'exploitation de cette dernière est initialement effectuée avec Qgis. Cela permet d'en avoir une utilisation très complète. Mais, cet aspect est surtout vrai au bureau. Un bon PC, plusieurs écrans... et on profite d'une expérience utilisateur plutôt ergonomique et avec tout le potentiel d'un SIG. 
Mais, sur le terrain, qu'en est-il ? 
L'utilisation de Qgis sur les chantiers (diagnostic ou fouille) peut s'avérer quelque peu laborieuse. Si on a la chance d'avoir une tablette durcie Windows, la taille de l'écran est très peu adaptée à un confort d'utilisation et la puissance est rarement au rendez-vous. Qgis est en effet très gourmand en ressource système. 
Sur le terrain, il est également possible d'utiliser des tablettes durcies Android, quand elles sont disponibles, ou tout simplement son smartphone Android. Il est alors possible d'utiliser l'application Qfield, téléchargeable sur le store. La dernière version de Qfield est très performante, mais pas toujours des plus adaptée d'un point de vue ergonomie et elle n'est pas toujours très intuitive. 

Les développeurs de Badass cherchaient donc depuis une solution d'interface simple, ergonomique et intuitive, utilisable sur l'ensemble des terminaux disponibles sur le terrain comme en post-fouille.



# <span style="color: #73a12d">Avantages ?</span>


Quel est l'intérêt d'utiliser Bad'Mobil ?
- C'est un **serveur local** : pas besoin d'une gestion particulière (utilisateurs et support technique) comme c'est le cas pour les serveurs en ligne, comme postgresql par exemple.
- **Ultra léger**, peu gourmand en ressource système.
- **Un dossier** avec tout dedans.
- Tu as un PC ? tu as un serveur... En réalité, ton PC peut même **héberger plusieurs serveurs**, donc plusieurs bases de données.
- Qui dit serveur, dit connexion !!! On peut bien sûr utiliser BAd'Mobil sur le poste sur lequel a été allumé le serveur. Mais surtout, il possible de **se connecter à distance** à ce serveur, donc à la base de données, **sur d'autres terminaux** (windows, mac, linux, linux) grâce à une simple url connue.
- Badass est mono-utlisateur car installé sur un poste. Grâce à Bad'Mobil, l'outil devient **multi-utilisateur**.



# <span style="color: #73a12d">La préparation</span>

Mettons nous dans l'ambiance :smile:
On commence une opération, diagnostic ou fouille. On va donc préparer sa base de données et faire le nécessaire pour l'exploiter via Bad'Mobil.



## <span style="color: #2dbce9">Badass</span>

Bien entendu, il faut préparer une base de données Badass pour recueillir les données archéologiques.
On rappellera ici la marche à suivre. Mais on pourra se référer aux [tutoriels](https://gitlab.com/projet-r-d-bddsarcheo/tutos/-/blob/main/00_version%20definitive/badass.md?ref_type=heads) plus complets et très précis concernant Badass.

1. Lancer **Qgis**.
2. Installer l'**extension Badass** depuis de Gestionnaire d'Extensions de Qgis.
3. Ouvrir l'extension Badass.
4. **Créer une nouvelle base de données**.
5. Choisir l'**emplacement du dossier** qui va contenir le pack projet Qgis et base de données SQLite. S'il est fortement conseillé de choisir la **racine de votre dossier opération** pour une utlisation courante, pour une utilisation de Bad'Modil cela devient <span style="color: #FF0000">**OBLIGATOIRE**</span>.
6. Choisir les **noms pour la base et le projet**. On peut conseiller de donner le même nom aux deux fichiers, c'est plus simple et plus intuitif puisque les deux fonctionnent ensemble (par exemple : NumSGA_badass.qgz et NumSGA_badass.sqlite). 

On peut dès à présent ouvrir le fichier de projet Qgis. 



## <span style="color: #2dbce9">Le dossier de Bad'Mobil</span>

Il faut récupérer le dossier complet [SQLPage_Badass](https://gitlab.com/projet-r-d-bddsarcheo/tutos/-/blob/03bbafd4c68aa83883fe5851edc20d7f2408e9b8/SQLPage_Badass_v1.4.1.zip). 
Il s'agit de le placer dans son dossier opération. <span style="color: #FF0000">**MAIS, ATTENTION PAS N'IMPORTE OÙ !!!!**</span>
Le dossier SQLPage_Badass doit impérativement être placé dans le sous-dossier **enregistrement** qui se trouve dans le dossier **donnee** de votre dossier opération.

![dossier](screenshot/dossier.png)

Il est vraiment très important de respecter ce chemin d'accès. De même, il ne faut pas renommer les dossiers dans l'arborescence par défaut du dossier opération et ne pas renommer le dossier SQLPage.



## <span style="color: #2dbce9">Configuration</span>

Pour utiliser BAD'Mobil, il faut en tout premier lieu paramétrer la connexion entre l'interface et la base de données Badass.  
Pour ne pas modifier à la main les paramètres de connexion, un petit programme a été créé afin de nous faciliter la tâche. 
Dans le dossier **SQLPage_Badass**, choisir le dossier sqlpage.
Dans ce dossier **sqlpage**, lancer le fichier exécutable **connexionSETUP.exe**. 

![dossier](screenshot/connexionSETUP.png)

Une petite fenêtre de configuration apparaît. Elle se nomme "Bad'Mobil : configuration de la connexion".

![dossier](screenshot/fenetre_connexionSETUP.png)

Dans cette fenêtre, on peut intervenir sur trois paramètres : 
- ***Port*** : un port de connexion, dans le contexte d'une base de données SQLite, est essentiellement un numéro d'identification attribué à la communication entre notre application/interface Bad'Mobil et la base de données Badass. C'est comme une porte spécifique sur le serveur où les données entrent et sortent. Dans notre cas, le **port par défaut** est le numéro **8080**, ce qui signifie que notre application communique avec la base de données sur cette porte particulière du serveur.
- ***Nom de la base de données Badass*** : il s'agit du nom défini plus tôt, à saisir manuellement. On ne met pas l'extension du fichier de base données, à savoir le .sqlite.
- ***Chemin d'accès du dossier donnee*** : spécifier ce chemin d'accès est indispensable. Il permettra, à Bad'Mobil, d'avoir accès aux minutes de terrain et aux photos de l'opération, situées respectivement dans le dossier **minute** et dans le dossier **image**. Ces deux dossiers se trouvent en effet dans le dossier donnee. Il suffit de cliquer sur Parcourir et aller sélectionner le dossier donnee.
Valider ensuite en cliquant sur **OK**.

Dans les faits, ce programme modifie le contenu du fichier *sqlpage.json*. Ce dernier est essentiel pour permettre la **communication entre Bad'Mobil et Badass**.

```json
{
  "port": 8080,
  "sqlite_extensions": ["mod_spatialite"],
  "database_url": "sqlite://../../../NumSGA_badass.sqlite",
  "web_root": "C:/users/john_doe/departement/dossier_operation/donnee"
}
```

Le programme de configuration crée également un fichier *index.sql* situé dans le dossier donnee. Il est essentiel au fonctionnement de Bad'Mobil. Il ne faut donc ni le supprimer, ni le renommer.

![dossier](screenshot/index.png)

Une fois cette étape de configuration réalisée rapidement et simplement, on peut utiliser Bad'Mobil.



# <span style="color: #73a12d">Utiliser Bad'Mobil</span>



## <span style="color: #2dbce9">En local</span>

Pour utiliser l'interface Bad'Mobil sur le poste informatique sur lequel se trouve le dossier opération (avec la base SQLite, le projet Qgis et le dossier SQLPage), il n'y a rien de plus simple.
Il suffit d'ouvrir un navigateur internet de votre choix, par exemple Chrome.
Dans la barre d'adresse, il faut taper la chaîne suivant : **localhost:8080**.

![dossier](screenshot/local_url.png) 

Lorsque nous disons **localhost:8080**, *localhost* est simplement un moyen de se référer à notre propre machine. Le numéro *8080* est le port auquel notre application écoute pour les connexions sur cette machine. En d'autres termes, c'est comme l'adresse spécifique sur notre propre ordinateur où notre application est prête à recevoir des requêtes. Donc, quand nous accédons à 'localhost:8080' dans un navigateur, nous contactons directement notre propre machine sur le port 8080 pour interagir avec l'application qui y écoute.


## <span style="color: #2dbce9">À distance</span>

Avec Bad'Mobil, grâce au serveur stand-alone SQLPage, vous avez également, et surtout, la possibilité de vous connecter à distance, depuis n'importe où, tant que vous disposez d'une connexion Internet.
Encore une fois, il suffit de saisir une URL dans la barre d'adresse de votre navigateur internet.
Il faut en fait se connecter à l'ordinateur qui héberge le serveur. On a donc besoin de son **adresse IP**, c'est à dire un identifiant numérique unique attribué à chaque appareil connecté à un réseau informatique qui utilise le protocole Internet pour la communication.
Il faut cependant que le réseau utilisé soit protégé. On va alors utliser le **VPN Inrap** comme réseau commun protégé pour établir la communication.
Le VPN doit être installé et ouvert sur tous les terminaux : celui qui héberge le serveur et celui ou ceux qui s'y connectent.

***Petit rappel concernant l'installation du VPN sur un PC professionnel***.
Aller à l'adresse suivante : [https://firewall.inrap.fr/](https://firewall.inrap.fr/).
Entrer votre **identifiant Inrap** et Connecter.
Entrer ensuite le **mot de passe** Inrap et OK. 

![dossier](screenshot/firewall_inrap.png) 

Dans la fenêtre suivante, cliquer sur **VPN SSL Client**. 

![dossier](screenshot/vpn_ssl_client.png) 

Choisir ensuite la langue et la version du système d'exploitation.
On peut ensuite télécharger le fichier suivant : 
*Stormshield_SSLVPN_Client_3.2.3_win10_fr_x64.msi*
Installer le programme VPN.

Une fois le client VPN installé, il faut activer ce dernier. Pour cela, dans la barre d'état Windows, cliquer sur *Afficher les icônes cachées*.
**Clic droit** et **Connecter**.

![dossier](screenshot/activer_vpn.png) 

Dans la petite fenêtre qui apparaît, il suffit de saisir à nouveau son identifiant et son mot de passe. Valider par OK.

![dossier](screenshot/connect_vpn.png) 

Le VPN se connecte. Si tout est OK, un message doit apparaître. Il faut alors bien noter l'adresse IP générée par le programme. 

![dossier](screenshot/vpn_ok.png) 

Ainsi, dans son navigateur internet préféré, on peut saisir *adresseIP:8080*.

![dossier](screenshot/distant_url.png) 

On accède alors à la page d'accueil de Bad'Mobil.


## <span style="color: #2dbce9">Sur un terminal Android</span>

Pour utiliser Bad'Modil sur mobile Android, smartphone ou tablette, il faut installer le VPN sur le terminal en question. La procédure est quelque peu différente. 

À partir du Play Store, sur le terminal Android, il faut installer l'application Open VPN Connect.

![dossier](screenshot/openvpn.png) 

Toujours sur le terminal Android, ouvir un navigateur internet et, comme précédemment, aller à l'adresse [https://firewall.inrap.fr/](https://firewall.inrap.fr/).
Entrer ensuite identifiant et mot de passe Inrap

![dossier](screenshot/profilvpn.png) 

Il faut télécharger le fichier *Profil VPN SSL pour clients mobile OpenVPN Connect (fichier unique .ovpn)*.

Ouvrir l'application Open VPN.
Choisir l'onglet *Upload File*.
Cliquer sur *Browse* pour aller chercher le profil Inrap.

![dossier](screenshot/import_profil.png) 

Dans les méandres du terminal, sélectionner le fichier ***openvpn_mobile_client.ovpn***.

![dossier](screenshot/import_ovpn.png) 

Un fois le fichier importé, il faut rentrer son identifiant, cocher *Save password*, saisir son mot de passe Inrap et cliquer sur **Connect**.

![dossier](screenshot/imported_profile.png) 

La connexion au VPN Inrap doit se faire automatiquement.
Par la suite, l'état du VPN sera soit connecté ou déconnecté. Il suffira de switcher entre les deux.

![dossier](screenshot/disconnected.png) 

À partir de là, dans un explorateur web, on entre l'URL adresseIP:8080 et on a accès à Bad'Mobil. 



## <span style="color: #2dbce9">Le multi-utilisateur</span>

Utiliser l'interface web Bad'Mobil pour exploiter la base de données Badass permet de fait de se connecter à plusieurs à distance et en même temps sur différents types de terminal. 
Les deux seules limites connues sont :

- La **connexion et la qualité du réseau**, que ce soit entrant ou sortant. Les éventuels "saut" de réseau peut déconnecter le VPN et donc la connexion entre le serveur et les clients.
- **Saisir en même temps, avec plusieurs terminaux**, ne pose techniquement aucun problème. Seule la création d'une entité dans la même de manière synchrone peut être source de conflit. La numérotation étant incrémentée automatiquement, deux entités initiés silmultanément auront le même identifiant. C'est l'utilisateur qui validera en premier qui aura gagné, le second obtiendra une "commit error". Un retour arrière sauvera la situation.


## <span style="color: #2dbce9">Plusieurs bases de données</span>

Sur un même poste servant de serveur, il est possible de lancer **plusieurs serveurs SQLPage**. Pour ne pas qu'il y ait de souci de connexion, il suffit, et c'est obligatoire, de déclarer des ports différents pour chaque base de données au moment de la première configuration des serveurs.
A chaque fois que l'on exécute le programme connexionSETUP.exe, on veille à donner un numéro de **port différent** : 8080, 8081, 8082...




# <span style="color: #73a12d">Que fait Bad'Mobil ?</span>



L'interface BadMobil, en accédant à une base de données Badass, permet de saisir, consulter et modifier la donnée de terrain suivante :
- les <span style="color: #FF0000">tranchées</span>,
- les <span style="color: #FF0000">sondages</span>,
- les **<span style="color: #2fb344">faits</span> archéologiques**,
- les **unités stratigraphiques (<span style="color: #2fb344">US</span>)**
- le **<span style="color: #2fb344">mobilier isolé</span>**,
- les **<span style="color: #f59f00">Logs</span>**,
- les **<span style="color: #f59f00">Prélèvements</span>**,
- les **<span style="color: #4299e1">Photos</span> numériques**,
- les **<span style="color: #4299e1">Minutes</span> de terrain**,
- les **<span style="color: #4299e1">Axes</span> de relevé**.

