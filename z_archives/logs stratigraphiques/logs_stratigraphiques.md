## Les logs dans Badass

### Les couches concernées

La génération automatique des logs repose sur plusieurs couches : 
- **log** : table des 6 couches qui comporte l'information topographique des logs. Elle comprend les champs *id_log, numlog, typlog, alti, typalti* et *date_leve*. Les informations **num_log** et **alti** doivent impérativemetnt avoir été complétées.
- **t_log** : table de *"l'enregistrement archéologique et technique"*. Listant les logs réalisés sur le terrain, elle comprend les champs *id_log, le numéro du log, le numéro de tranchée, le numéro de sondage, l'atitude maximale du log, la profondeur maximale (en m) du log, l'atitude minimale du log (auto-calculée), les objectifs, commentaires,* et si un numéro d'axe a été attribué, le dit *numéro*. 
- **j_us_log** : table présente dans le groupe "table de jonction", elle fait la relation entre les US et les logs. Elle comprend un champ d'id, le uméro de l'us, le numéro du log, la profondeur (en mètres) du toit de l'us, la profondeur (en mètres) du plancher de l'US, l'épaisseur de l'US (en mètres), L'atitude maximale de l'US, l'atitude minimale de l'US, l'altitude du sommet du log. 
- **t_us** : la table des US dans le groupe "enregistrement archéo". Elle doit comporter le numéro des US identifiées dans les logs. 
- **vue_log_stratigraphique** dans le groupe "coupe > VUES". Il s'agit d'un calcul réalisé à partir des données fournies dans les autres tables, qui génère automatiquement le dessin des logs.  

### Les éléments à renseigner

1) **Créez les logs** : dans la couche t_log, renseignez le numéro, le numéro de tranchée, l'altitude maximale, sa profondeur
2) **Créez les us des logs** : avec leur description, dans la table t_us. 
3) **Vérifiez que les logs sont bien présents dans la couche log des 6 couches** : Ils doivent comporter le même numéro que dans la table t_log, les même altitudes, etc. Corrigez au besoin. 
3) **Créez la relation entre les US et les logs** : dans la table j_us_log, renseignez le numéro de l'us, le numéro du log, la profondeur en mètre du toit de l'US, la profondeur en mètre du plancher de l'US, l'épaisseur de l'US, l'atitude maximale de l'US. La profondeur minimale est autocalculée.  
Pour renseigner *l'altitude maximale de l'US*, passez en mode édition dans votre table attributaire, et ouvrez la calcaulatrice de champ. Mettez à jour le champ "zmax_uslog" avec le calcul suivant : 
>"log_alti" - "prof_toit" 

En cas d'erreur, n'enregistrez pas votre table attributaire et recommencez. 

![](logsstrati1.png)

4) Admirez le résultat dans la couche vue_log_stratigraphique
