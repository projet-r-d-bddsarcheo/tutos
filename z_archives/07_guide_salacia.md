# Salacia : guide des pratiques

Salacia est une machine virtuelle mise à disposition par la Direction des Services Informatiques (DSI) de l'Inrap. Cette machine virtuelle dispose d'un espace de stockage distant et de capacité matérielle qui la rend plus performante qu'un stockage sur un NAS.
Le disque distant peut être consulté depuis l'explorateur de fichiers des postes informatiques. Salacia accueille les différentes occurences de lancement de serveur SQLPage de Bad'mobil.

## Mise en place opérationnelle

La machine virtuelle Salacia est administrée par la DSI et le collectif Ramen possède des droits de création de dossiers et de fichiers ainsi que de partage sur le disque distant. Les serveurs SQLPage qui permettent l'utilisation de Bad'Mobil sur le terrain sont lancés par le collectif Ramen pour chaque opération qui en fait la demande. Associé à chaque occurence de SQLPage, un port unique est attribué pour chaque opération.

L'adresse de connexion à Bad'Mobil depuis un navigateur web, commencera toujours par **salacia.inrap.fr** et sera suivie du numéro de port, par exemple **:8080**.

L'adresse du dossier lié à l'opération et qui contient la base de données SpatiaLite se trouve à l'adresse : **//salacia/operations/** suivi du nom de dossier de l'opération.

Qu'il s'agisse de l'accès à Bad'Mobil sur la page web, ou de l'accès au dossier d'opération sur le disque distant, il est impératif : soit d'être connecté au réseau en base, soit, si l'accès se fait depuis l'extérieur ou en distanciel, d'être connecté au VPN.

## Demande d'accès

L'accès à Bad'Mobil depuis Salacia est réservé aux seuls agents de l'Inrap et ne peut en aucun cas être proposé aux externes.

La préparation technique nécessaire à la mise en place opérationnelle nécessite du temps :
- création du dossier sur le disque distant
- ajout des droits de lecture et d'écriture pour le ou les agents concernés
- création de la base de données
- enregistrement de l'emprise de l'opération dans la base
- enregistrement d'au moins un agent de l'équipe et d'un identifiant et mot de passe associé dans la base de données
- installation de l'arborescence liée à Bad'Mobil
- paramétrage du serveur SQLPage
- lancement du serveur

Le nombre de ports disponibles pour les différents serveurs est limité et la distribution des ports peut être par conséquent refusé, faute de port disponible.
Aussi, dans la mesure du possible, prévoir le délai nécessaire entre le démarrage de l'opération et la demande d'ouverture pour que les choses s'organisent sereinement (minimum une semaine).

La demande peut-être adressée au collectif Ramen.

## Pendant l'opération

Si, pendant l'opération,l'inutilité de Bad'Mobil est constatée, il est impératif de le signaler au collectif le plus rapidement possible pour libérer l'espace de stockage et le port sur Salacia.

## Après l'opération

À l'issue du terrain, la base de données à vocation à être stockée sur le NAS, aussi, il est demandé à l'équipe de :
- copier/coller le fichier .sqlite, 
- le cas échéant de copier/coller le projet Badass
- de copier/coller le dossier **donnee** qui contient l'arborescence de Bad'Mobil.
- d'avertir le plus tôt possible le collectif Ramen afin que le serveur SQLPage soit clôturé et le disque distant vidé

## Relancer Bad'Mobil depuis le NAS

Le serveur SQLPage de Bad'Mobil peut-être lancé pour la saisie de post-fouille depuis le NAS. 

En base, un agent lance le serveur sur son poste et communique l'adresse IP de son poste informatique à ces collaborateurs. Cette adresse IP pourra être saisie dans un navigateur web, suivi du numéro de port défini par la configuration du serveur. Par exemple : 172.2.10:8080

L'agent qui lance le serveur, quant à lui, peut se contenter de saisir : localhost:8080 ou le numéro du port défini, si différent.

À distance, qu'il s'agisse de l'agent qui lance le serveur, ou d'un agent qui souhaite accéder à Bad'Mobil, le lancement préalable du VPN est impératif.