### Fiche technique 1 : enregistrer un fait

Ouvrir le formulaire de la table t_fait et se placer en mode édition pour créer un nouvel enregistrement ou modifier un enregistrement existant. On utilisera dans ce cas l'icone d'ajout d'entité (2) 

<img src="ressources/ajout_entite.png"  width="500">

* **Le numéro du fait** (nombre) est renseigné en haut. 
* **L'interprétation du fait** est renseignée en dessous, avec deux champs d'interprétation possibles, et une case à cocher en cas de caractère douteux. 
Un second groupe de champs permet de réaliser le suivi de la fouille et de l'enregistrement des faits : fouillé, relevé, topographié, photographié, annulé, sous forme de cases à cocher ou listes déroulantes

Un groupe d'onglet permet l'enregistrement des informations métriques du fait, de réaliser la description du fait (formes et profils), de renseigner la datation estimée, et récapitule les us contenues dans le fait ainsi que les photos réalisées. 
Ces deux derniers onglets se présentent sous la forme de sous formulaires qui permettent la consultation des données liées, mais également leur modification ou leur création. 
