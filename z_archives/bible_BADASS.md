# Base Archéologique de Données Attributaires et SpatialeS (Badass)

> Caroline Font, Thomas Guillemard, Florent Mercey, Anne Moreau, Christelle Seng

![Base Archéologique de Données Attributaires et SpatialeS][000]

La Base Archéologique de Données Attributaires et SpatialeS (Badass), s'inscrit dans la continuité de travaux autour de l'enregistrement des données archéologiques issues des opérations de diagnostics et de fouilles. L'expérience des archéologues, les technologies disponibles et les concepts du monde numérique actuels ont, à la fois, motivé, mais également, orienté les parties pris pour l’élaboration de cet outil.

Badass se veut mono-opération et doit permettre l'exploitation des données archéologiques du terrain jusqu'à l'archivage sur le CAtalogue de Visualisation de l'Information ARchéologique (Caviar). 

Ce texte fait état de travaux en cours et il est amené à être complété ou modifié au fur et à mesure que le projet évolue.
Le projet de développement de cette base de données est assuré par le collectif Ramen (Recherches archéologiques en modélisation de l'enregistrement numérique) constitué de Caroline Font (Inrap, St-Cyr-en-Val), Thomas Guillemard (Inrap, St-Cyr-en-Val), Florent Mercey (Inrap, St-Cyr-en-Val), Christelle Seng (Inrap, Croissy-Beaubourg). 

![Collectif Ramen][000b]

---

## Introduction - Les piliers de Badass

La conception et la philosophie de Badass s'appuient sur quatre grands principes fondateurs :

* Un enregistrement des données archéologiques classique autour de l’unité stratigraphique (US), (**Galinié, Randoin 1987**) qui ne peut se concevoir qu'avec une base de données relationnelles. À ce titre, la base de données relationnelle CADoc, développée par Thomas Guillemard (**Bolo 2014**) sous Filemaker™, ainsi que l'expérience et les retours utilisateurs recueillis, ont permis d'établir un modèle logique des données pertinent et robuste.
* Désormais encouragé et de plus en plus systématisé à l'Inrap, la structuration des données spatiales minimales intègre le modèle logique de Badass. Pilotés et formalisés par  le réseau des référents SIG (**Moreau 2016**), les Six Couches - *emprise, ouverture, axe, log,  poly, point* - constituent l'entrée spatiale de Badass.
* Les formations dispensées sur le logiciel QGIS auprès de **plus de 1000** collègues et collaborateurs de l'Inrap et la maîtrise de plus en plus approfondie des fonctionnalités du logiciel, associées à des formats d'enregistrement s'appuyant sur le Structured Query Language (SQL - **Codd 1970**) ont orienté les choix logiciels et d'enregistrement pour Badass. Les utilisateurs de Badass seront donc en mesure d'en modifier la structure en ajoutant des champs manquants, des tables, ou même des requêtes, de modifier les affichages des formulaires pour en personnaliser l'usage.
* Toute discipline archéologique spécialisée (anthropologie, céramologie, archéozoologie...) et dont les concepts d'enregistrements rejoignent les principes stratigraphiques peuvent intégrer Badass sous forme d'extensions. Les tables ainsi ajoutées sont en relation avec la structure logique de Badass considérée comme le noyau incontournable.

---

## Réconcilier l'enregistrement spatial et l'enregistrement archéologique

L'enregistrement archéologique et sa restitution au sein d'une base de données doit rendre compte des données sur un plan horizontal (le plan) et vertical (la coupe, la succession des dépôts sédimentaires). Il est donc nécessaire de prévoir une adéquation entre les deux pour permettre au maximum la restitution des informations enregistrées en coupe pour une restitution en plan. Les géométries acquises lors des levers topographiques permettent la représentation en plan et une base de données relationnelle permet d'enregistrer les subtilités de la stratigraphie. Deux systèmes d'enregistrement sont donc souvent mis en parallèle et aujourd'hui, la majorité des bases d'enregistrements archéologiques n'intègrent pas la représentation spatiale dans leur modèle logique. Certains logiciels choisis (Système de Gestion de Base de Données - SGBDR), peu compatibles avec les logiciels de Systèmes d'Information Géographique (SIG), nécessitent souvent un export vers d'autres formats de données pour permettre leur utilisation dans un SIG. Problème majeur, puisque deux versions de la base de données, une pour chaque logiciel, voire davantage, existent. Difficile, dans ces conditions, de garantir l'intégrité des données et l'utilisateur s'encombre de contraintes d'utilisation importantes, notamment le sens unique de parcours de la donnée : 

*base de données relationnelle permettant la saisie → export de table → intégration dans un SIG (jointures ou relations)*.

La première préoccupation de Badass est donc de réconcilier l'enregistrement spatial et l'enregistrement archéologique et de s'affranchir une fois pour toute de ces contraintes en gagnant en souplesse d'utilisation tout en garantissant l'intégrité des données. Badass intègre également des tables qui permettent de représenter par des géométries, les coupes stratigraphiques issues des relevés de terrain. Les deux dimensions, plan et coupe, sont donc accessible par une représentation graphique dans Badass.

Une "donnée" peut être définie par un ensemble de propriétés ou d’attributs : géométrique (forme), topographique (localisation) et descriptive (attributaire). Le schéma conceptuel de Badass s’appuie sur le principe de décomposition de la donnée en propriétés : ce ne sont pas les données qui sont gérées dans la base de données mais les propriétés. L’addition des différentes propriétés permet de restituer la donnée. Le principe de décomposition de la donnée utilisé dans Badass est déjà largement mis en œuvre dans le processus actuel de collecte des données de terrain qui dissocie l’enregistrement des propriétés spatiales des données (= le lever topographique) de leur description (l’enregistrement archéologique).

Si l'ont s'appuie sur les pratiques observées à l'Inrap depuis la systématisation des Six Couches dans l'acquisition spatiale, l'enregistrement des données de terrain se sépare selon deux voies, qui, la plupart du temps, ne sont pas parcourus par les mêmes personnes :

* l'enregistrement spatial assuré par le topographe (le plan)
* la description des vestiges assurée par les archéologues (la stratigraphie et la gestion de l'opération)

De fait, ces enregistrements se mènent de manière parallèle sur le terrain. Un aller/retour doit donc être possible à tout moment d'une opération archéologique, du terrain à la phase d'étude et de rédaction jusqu'à la finalisation du rapport d'opération.
À cette contrainte d'aller/retour, s'ajoute également une contrainte de structure puisqu'en l'état, les Six Couches ne sont pas suffisantes pour décrire totalement les données archéologiques. Les besoins des archéologues sont plus larges et rajoutent des tables et des descriptifs à ce modèle spatial. 

Par exemple, la table ***poly*** des Six Couches renvoie bien à une réalité spatiale : toute entité sous forme d'anomalie de surface ayant une forme et une localisation. En revanche, ces entités spatiales correspondent à deux notions emboîtées de l'enregistrement archéologiques : l'***US*** et le ***Fait***.

![Modèle conceptuel des relations autour de la table ***poly***][001]

Lors de la saisie, l'archéologue doit pouvoir ajouter, éditer les géométries depuis les tables ***fait*** ou ***US***. Un retour doit donc être possible vers la table ***poly***. En effet, le modèle des Six Couches sert de support d'archive sur le CAtalogue de Visualisation de l'Information ARchéologique (Caviar). À l'usage, nous avons constaté que des relations entre tables, telles que figurées dans la figure ci-dessus, ne sont pas forcément d'une utilisation souple dans les logiciels de SIG, notamment à cause de cet aller/retour nécessaire. 

D'autres fonctionnalités dans le langage SQL permettent d'automatiser la saisie de données attributaires dans les tables d'une base de données. Il s'agit en particulier d'instructions SQL appelées *triggers* ou *déclencheurs* (**Lozano & Georges 2019**). Considérant que la géométrie est contenue dans un champ d'une table, le contenu de ce dernier peut être écrit de manière automatique dans une ou plusieurs tables sous certaines conditions incluses dans l'instruction du déclencheur. Cette mise à jour est donc automatique et dispense l'utilisateur de la gestion des relations entre les tables des Six Couches et les tables archéologiques.

Dans Badass et grâce aux *triggers*, lorsque l'utilisateur crée et décrit un fait, muni d'un numéro, considéré comme la clé primaire de la table ***fait***, lors de l'import des Six Couches dans la base de données, si une entité dans la table ***poly*** des Six Couches définie comme un fait et identifiée par le même numéro existe, le déclencheur rempli automatiquement le champ géométrie. Si l'archéologue modifie la géométrie d'un ou plusieurs faits, cette dernière est mise à jour dans la table ***poly*** par le biais de ces même *triggers*.

![Fonctionnement d'un trigger autour de la table ***poly***][002]

### Fonctionnement des *triggers* dans Badass

Les premières lignes des instructions SQL de *triggers* permettent d'indiquer à quel moment l'instruction se déclenche :

* AFTER INSERT : enclenche le *trigger* après une création d'entité
* AFTER UPDATE : enclenche le *trigger* après une mise à jour sur une entité
* AFTER DELETE : enclenche le *trigger* après une suppression d'entité

L'instruction AFTER DELETE n'a finalement pas été retenu dans le code afin d'éviter les suppressions en cascades irréversibles.

Exemple d'instruction commentée d'un *trigger* qui permet de mettre à jour la table **t_fait** après l'ajout d'une entité dans la couche **poly** des Six Couches :

```sql
-- les triggers à mettre en place pour la table poly (qui doivent mettre à jour les tables t_fait et t_us)

-- AFTER INSERT
-- qui met à jour la table t_fait après CREATION d'une entité dans poly
CREATE TRIGGER trgai_poly_maj_t_fait /*déclaration de création d'un nouveau trigger qui a pour nom...*/
    AFTER INSERT /*qui sera exécuté après l'ajout d'une nouvelle entité*/
    ON poly /*sur/dans la table*/
FOR EACH ROW /*commande obligatoire : pour tous les enregistrement*/
WHEN (NEW.typoly = 'fait') /*cette condition permet de restreindre les enregistrements concernés aux seuls 'fait' ; NEW correspond à une copie temporaire des nouveaux éléments de la table t_poly effectuée lors de l'exécution du trigger*/
BEGIN /* debut de l'action déclenchée*/
UPDATE t_fait /*avec une modification de la table t_fait*/
SET geometry = NEW.geometry /*qui redéfini la valeur du champ "geom" de la table t_fait par la valeur du champ "geom" de la copie temporaire NEW de la table t_poly*/
WHERE NEW.numpoly = numfait ; /*à chaque fois que la valeur du champ "num_fait" de la table t_fait est égale à la valeur du champ "numpoly" de la copie temporaire NEW de la table t_poly*/
END ; /*fin de l'action et fin du trigger*/ 

```


Les géométries de l'ensemble des tables sont donc mises à jour selon ce principe. Près d'une quarantaine de déclencheurs ont intégré le code SQL de Badass pour gérer ces mises à jour des géométries. 
Le schéma ci-après résume le fonctionnement de l'ensemble des *triggers* de Badass :

![Fonctionnement de l'ensemble des *triggers* de Badass entre les tables archéologiques et les Six Couches][003]

---

## Présentation de la structure logique de Badass

### Modèle logique de données (MLD)

Ce modèle logique présente l'ensemble des tables et des relations prévues par ce modèle ainsi que les *triggers* mis en place.

#### Légende

![Légende du MLD de Badass][004]

#### Diagramme

![Modèle logique de données de Badass][005]

### Description des tables, champs et relations

La base de données a été conçue pour respecter la logique d'enregistrement sur le terrain. Concrètement, en cours d'opération, l'équipe sur le terrain, enregistre des entités au fur et à mesure de l'avancée de l'exploration archéologique. Le topographe passe ponctuellement pour en acquérir la géométrie.
Dans la base de données, cette logique est retranscrite dans le code SQL. Cet ordre d'enregistrement doit donc être suivi pour que les déclencheurs fonctionnent correctement :

1. Enregistrement des données archéologiques dans les tables oranges
2. Enregistrement des données spatiales issues du lever topographique, les Six Couches, en bleu. Un simple copier/coller des shapes fournis par le topographe sur les tables "Six couches" de Badass est possible.

#### Les tables d'enregistrements des données archéologiques

Comme évoqué plus haut, ces tables, leur structure et leurs relations, s'appuient sur l'expérience d'utilisation de la base de données CADoc, développé par Thomas Guillemard. Cette base, utilisée et perfectionnée par plusieurs dizaines de collègues archéologues depuis 14 ans, disposent de ce fait de nombreux retours qui nous ont permis de reprendre et d'améliorer la structure dans Badass.

* **t_tranchee**

Cette table permet d'enregistrer toutes les informations concernant les tranchées de diagnostic.

| Champ | Typage | Description | Contrainte |
|-----------|-----------|-----------|-----------|
| **id_tr** | entier | clé primaire à incrémentation automatique gérée par SpatiaLite | unique, non nul |
| numtr | entier | numéro attribué par le responsable d'opération | unique |
| larg | réel | largeur maximale de la tranchée exprimée en mètre | |
| long | réel | longueur de la tranchée exprimée en mètre | |
| prof_min | réel | profondeur minimale de la tranchée exprimée en mètre | |
| prof_max | réel | profondeur maximale de la tranchée exprimée en mètre | |
| surface | réel | surface de la tranchée exprimée en m² | |
| note | texte | commentaires divers | |
| geometry | polygone | géométrie de l'entité | |

##### Tables filles

> **t_sondage** : un ou plusieurs sondages peuvent être associés à une tranchée - *relation de 1 à n*  
> **t_log** : un ou plusieurs logs peuvent être associés à une tranchée - *relation de 1 à n*  
> **j_rel_tranchee** : table fille permettant d'associer les faits et les US aux tranchées - *relation de n à n*  
> **j_rel_photo** : table fille permettant d'associer les photos aux tranchées - *relation de n à n*  
> **j_rel_minute** : table fille permettant d'associer les minutes aux tranchées - *relation de n à n*  

* **t_sondage**

Cette table permet d'enregistrer toutes les informations concernant les sondages.

| Champ | Typage | Description | Contrainte |
|-----------|-----------|-----------|-----------|
| **id_sd** | entier | clé primaire à incrémentation automatique gérée par SpatiaLite | unique, non nul |
| numsd | entier | numéro attribué par le responsable d'opération | unique |
| type | chaîne de caractères | type de sondage : sondage, sondage profond, etc. | |
| prof | réel | profondeur du sondage exprimé en mètre | |
| note | texte | commentaires divers | |
| geometry | polygone | géométrie de l'entité | |
| ***numtr*** | entier | clé étrangère vers la table mère **t_tranchee** | |

##### Tables filles

> **t_log** : un ou plusieurs logs peuvent être associés à un sondage - *relation de 1 à n*  
> **j_rel_sondage** : table fille permettant d'associer les faits et les US aux sondages - *relation de n à n*  
> **j_rel_photo** : table fille permettant d'associer les photos aux sondages - *relation de n à n*  
> **j_rel_minute** : table fille permettant d'associer les minutes aux sondages - *relation de n à n*  

* **t_axe**

Cette table permet d'enregistrer toutes les informations concernant les axes. 

> :exclamation: ATTENTION !
> Les axes doivent être enregistrés avec un certain nombre de contraintes géométriques :
> Un axe est défini par un ligne constituée de deux points dont l'altitude est sensiblement la même. L'axe est en effet horizontal et possède une unique direction. Un changement de direction, ou un changement d'altitude entraîne un changement d'axe et donc la création d'une nouvelle entité.
> Le premier point constitue l'origine de l'axe, c'est à dire l'origine du relevé de la coupe qui définit le sens de lecture de cette dernière.

| Champ | Typage | Description | Contrainte |
|-----------|-----------|-----------|-----------|
| **id_axe** | entier | clé primaire à incrémentation automatique gérée par SpatiaLite | unique, non nul |
| numaxe | entier | numéro attribué par le responsable d'opération | unique |
| long | réel | longueur, exprimée en mètre | |
| alti | réel |altitude de l'axe, exprimée en mètre selon le référentiel altimétrique choisi (NGF - IGN69 par exemple) | |
| note | texte | commentaires divers | |
| geometry | polyligne | géométrie de l'entité | |

##### Tables filles

> **j_rel_axe** : table fille permettant d'associer les faits et les US aux axes - *relation de n à n*  
> **coupe_axe** : table fille permettant d'associer l'axe en plan à sa représentation en coupe - *relation de 1 à 1*  
> **coupe_poly** : table fille permettant d'associer l'axe en plan aux polygones représentant les US en coupe - *relation de 1 à n*  
> **coupe_line** : table fille permettant d'associer l'axe en plan aux polylignes représentant les US en coupe - *relation de 1 à n*  

* **t_log**

Cette table permet d'enregistrer toutes les informations concernant les logs stratigraphiques.

| Champ        | Typage               | Description                                                                                                      | Contrainte      |
|--------------|----------------------|------------------------------------------------------------------------------------------------------------------|-----------------|
| **id_log**   | entier               | clé primaire à incrémentation automatique gérée par SpatiaLite                                                   | unique, non nul |
| alti         | réel                 | altitude du sommet du log, exprimée en mètre selon le référentiel altimétrique choisi (NGF - IGN69 par exemple)  |                 |
| objectif_log | chaîne de caractères | spécialité liée au log stratigraphique                                                                           |                 |
| prof_log     | réel                 | profondeur du log, exprimée en mètres                                                                            |                 |
| zmin_log     | réel                 | altitude de la base du log, exprimée en mètre selon le référentiel altimétrique choisi (NGF - IGN69 par exemple) |                 |
| note_log     | texte                | commentaires divers                                                                                              |                 |
| geometry     | point                | géométrie de l'entité                                                                                            |                 |
| ***numlog*** | entier               | clé étrangère vers la table mère **log**                                                                         |                 |
| ***numtr***  | entier               | clé étrangère vers la table mère **t_tranchee**                                                                  |                 |
| ***numsd***  | entier               | clé étrangère vers la table mère **t_sondage**                                                                   |                 |

##### Tables filles

> **j_us_log** : table fille permettant d'associer les US aux logs - *relation de n à n*  

* **t_prelevement**

Cette table permet d'enregistrer toutes les informations concernant les prélèvements.

| Champ          | Typage               | Description                                                    | Contrainte      |
|----------------|----------------------|----------------------------------------------------------------|-----------------|
| **id_prel**    | entier               | clé primaire à incrémentation automatique gérée par SpatiaLite | unique, non nul |
| numprel        | entier               | numéro attribué par le responsable d'opération                 | unique          |
| type           | chaîne de caractères | type de prélèvement                                            |                 |
| alti           | réel                 | altitude du prélèvement                                        |                 |
| objectif       | chaîne de caractères | objectif du prélèvement                                        |                 |
| contenant      | chaîne de caractères | contenant du prélèvement                                       |                 |
| creator        | chaîne de caractères | auteur du prélèvement                                          |                 |
| volume         | réel                 | volume du prélèvement                                          |                 |
| note           | texte                | commentaires divers                                            |                 |
| ***numfait*** | entier               | clé étrangère vers la table mère **t_fait**                    |                 |
| ***numus***   | entier               | clé étrangère vers la table mère **t_us**                      |                 |

##### Tables filles

> néant

* **t_ens**

Cette table permet d'enregistrer toutes les informations concernant les ensembles.

| Champ       | Typage               | Description                                                              | Contrainte      |
|-------------|----------------------|--------------------------------------------------------------------------|-----------------|
| **id_ens**  | entier               | clé primaire à incrémentation automatique gérée par SpatiaLite           | unique, non nul |
| numens      | entier               | numéro attribué par le responsable d'opération                           | unique          |
| typens      | chaîne de caractères | type d'ensemble (fonctionnel, englobant)                                 |                 |
| interpret   | chaîne de caractères | interprétation de l'ensemble (bâtiment, palissade, batterie de fours...) |                 |
| description | chaîne de caractères | courte description de l'ensemble                                         |                 |
| note        | chaîne de caractères | commentaires divers                                                      |                 |
| geometry    | polygone             | géométrie de l'entité                                                    |                 |
| ***ens_englob***  | entier                     | clé étrangère de relation imbriquée vers la table **j_rel_ens**                                                                        |                 |

##### Tables filles

> **j_rel_ens** : table fille permettant d'associer les ensembles, les faits et les US aux ensembles - *relation de n à n*  
> **j_rel_photo** : table fille permettant d'associer les photos aux ensembles - *relation de n à n*  
> **j_rel_minute** : table fille permettant d'associer les minutes aux ensembles - *relation de n à n*  

* **t_fait**

Cette table permet d'enregistrer toutes les informations concernant les faits archéologiques.

| Champ           | Typage               | Description                                                                                                                     | Contrainte      |
|-----------------|----------------------|---------------------------------------------------------------------------------------------------------------------------------|-----------------|
| **id_fait**     | entier               | clé primaire à incrémentation automatique gérée par SpatiaLite                                                                  | unique, non nul |
| numfait         | entier               | numéro attribué par le responsable d'opération                                                                                  | unique, non nul |
| interpret       | chaîne de caractères | interprétation fonctionnelle du fait (liste de valeurs disponible et modifiable dans la table **thesaurus_Badass**)             |                 |
| interpret_alter | chaîne de caractères | interprétation alternative fonctionnelle du fait (liste de valeurs disponible et modifiable dans la table **thesaurus_Badass**) |                 |
| douteux         | entier               | 0 : fait avéré ; 1 : fait douteux                                                                                               |                 |
| equiv_diag      | chaîne de caractères | identifiant attribué lors du diagnostic si nécessaire                                                                           |                 |
| statut          | chaîne de caractères | statut de fouille du fait (liste de valeurs disponible et modifiable dans la table **thesaurus_Badass**)                        |                 |
| rais_annule     | chaîne de caractères | raison de l'annulation du fait                                                                                                  |                 |
| fouille         | chaîne de caractères | mode de fouille du fait : 50%, 100 %, sondage                                                                                   |                 |
| enr_fini        | entier               | 0 : enregistrement en cours ; 1 : enregistrement achevé                                                                         |                 |
| relev_fini      | entier               | 0 : non relevé ; 1 : relevé                                                                                                     |                 |
| photo_fini      | entier               | 0 : non photographié ; 1 : photographié                                                                                         |                 |
| topo_fini       | entier               | 0 : non levé ; 1 : levé                                                                                                         |                 |
| profil          | chaîne de caractères | description du profil du fait (liste de valeurs disponible et modifiable dans la table **thesaurus_Badass**)                    |                 |
| forme           | chaîne de caractères | description de la forme en plan du fait (liste de valeurs disponible et modifiable dans la table **thesaurus_Badass**)          |                 |
| orient          | chaîne de caractères | description de l'orientation en plan du fait (liste de valeurs disponible et modifiable dans la table **thesaurus_Badass**)     |                 |
| orient_calc     | réel                 | azimuth (angle par rapport au Nord à 0°) majoritaire du fait, exprimé en degrés                                                 |                 |
| descrip         | chaîne de caractère  | courte description du fait                                                                                                      |                 |
| prof_app        | réel                 | profondeur d'apparition du fait, exprimée en mètres                                                                             |                 |
| diam            | réel                 | diamètre du fait, exprimé en mètres                                                                                             |                 |
| dim_max         | réel                 | dimension maximale du fait, exprimée en mètres                                                                                  |                 |
| dim_min         | réel                 | dimension minimale du fait, exprimée en mètres                                                                                  |                 |
| epais           | réel                 | épaisseur maximale conservée du fait, exprimée en mètres                                                                        |                 |
| prof_haut       | réel                 | profondeur/hauteur conservée du fait, exprimée en en mètre                                                                      |                 |
| periode         | chaîne de caractère  | grande période d'attribution du fait (liste de valeurs disponible et modifiable dans la table **thesaurus_Badass**)             |                 |
| note          | texte                | commentaires divers                                                                                                             |                 |
| geometry        | polygone             | géométrie de l'entité                                                                                                           |                 |

##### Tables filles

> **t_us** : une ou plusieurs unités stratigraphiques peuvent être associés à un fait - *relation de 1 à n*  
> **t_mobilier** : un ou plusieurs mobiliers peuvent être associés à un fait - *relation de 1 à n*  
> **t_prelevement** : table fille permettant d'associer des prélèvements au fait - *relation de 1 à n*
> **j_rel_fait** : table fille permettant d'associer les faits entre eux pour en préciser la relation stratigraphique - *relation de n à n*  
> **j_rel_ens** : table fille permettant d'associer les faits aux ensembles - *relation de n à n*  
> **j_rel_tranchee** : table fille permettant d'associer les faits aux tranchées - *relation de n à n*  
> **j_rel_sondage** : table fille permettant d'associer les faits aux sondages - *relation de n à n*  
> **j_rel_axe** : table fille permettant d'associer les faits aux axes - *relation de n à n*  
> **j_rel_photo** : table fille permettant d'associer les photos aux faits - *relation de n à n*  
> **j_rel_minute** : table fille permettant d'associer les minutes aux faits - *relation de n à n*  

* **t_us**

Cette table permet d'enregistrer toutes les informations concernant les unités stratigraphiques (US).

| Champ            | Typage               | Description                                                                                                            | Contrainte      |
|------------------|----------------------|------------------------------------------------------------------------------------------------------------------------|-----------------|
| **id_us**        | entier               | clé primaire à incrémentation automatique gérée par SpatiaLite                                                         | unique, non nul |
| numus            | entier               | numéro attribué par le responsable d'opération                                                                         | unique, non nul |
| type_us          | chaîne de caractères | type d'US : couche, négatif, altération (liste de valeurs disponible et modifiable dans la table **thesaurus_Badass**) |                 |
| nature_us        | chaîne de caractères | nature d'US (liste de valeurs disponible et modifiable dans la table **thesaurus_Badass**)                             |                 |
| interpret        | chaîne de caractères | interprétation de l'US (liste de valeurs disponible et modifiable dans la table **thesaurus_Badass**)                  |                 |
| description      | chaîne de caractères | courte description de l'US                                                                                             |                 |
| datsup_interpret | entier               | borne chronologique supérieure en année                                                                                |                 |
| datinf_interpret | entier               | borne chronologique inférieure en année                                                                                |                 |
| datsup_mobilier  | entier               | borne chronologique supérieure du mobilier en année                                                                    |                 |
| datinf_mobilier  | entier               | borne chronologique inférieure du mobilier en année                                                                    |                 |
| datsup_14c       | entier               | borne chronologique supérieure issue d'analyse radio-carbone en année                                                  |                 |
| datinf_14c       | entier               | borne chronologique inférieure issue d'analyse radio-carbone en année                                                  |                 |
| note_dat         | chaîne de caractères | commentaires sur la datation                                                                                           |                 |
| forme            | chaîne de caractères | description de la forme en plan de l'US (liste de valeurs disponible et modifiable dans la table **thesaurus_Badass**) |                 |
| diam             | réel                 | diamètre de l'US, exprimée en mètres                                                                                   |                 |
| dim_max          | réel                 | dimension maximale de l'US, exprimée en mètres                                                                         |                 |
| dim_min          | réel                 | dimension minimale de l'US, exprimée en mètres                                                                         |                 |
| prof_app         | réel                 | profondeur d'apparition de l'US, exprimée en mètres                                                                    |                 |
| zmax             | réel                 | altitude maximale de l'US, exprimée en mètre selon le référentiel altimétrique choisi (NGF - IGN69 par exemple)        |                 |
| zmin             | réel                 | altitude minimale de l'US, exprimée en mètre selon le référentiel altimétrique choisi (NGF - IGN69 par exemple)        |                 |
| epais            | réel                 | épaisseur maximale conservée du fait, exprimée en mètres                                                               |                 |
| compo_sediment   | chaîne de caractères | multivarié, composition du sédiment (liste de valeurs disponible et modifiable dans la table **thesaurus_Badass**)     |                 |
| texture          | chaîne de caractères | texture du sédiment (liste de valeurs disponible et modifiable dans la table **thesaurus_Badass**)                     |                 |
| couleur          | chaîne de caractères | multivarié, couleurs du sédiment (liste de valeurs disponible et modifiable dans la table **thesaurus_Badass**)        |                 |
| valeur_couleur   | chaîne de caractères | valeur des couleurs du sédiment (liste de valeurs disponible et modifiable dans la table **thesaurus_Badass**)         |                 |
| creator          | chaîne de caractères | auteur de l'enregistrement de l'US                                                                                     |                 |
| datcreation      | date                 | date de création de l'US                                                                                               |                 |
| note           | texte                | commentaires divers                                                                                                    |                 |
| geometry         | polygone             | géométrie de l'entité                                                                                                  |                 |
| ***numfait***  | entier               | clé étrangère vers la table mère **t_fait**                                                                            |                 |
| ***num_seq***  | entier               | clé étrangère vers la table mère **t_seq**                                                                             |                 |
| u_ordre_seq      | entier               | ordre des US défini par les relations stratigraphiques                                                                 |                 |

##### Tables filles

> **t_mobilier** : un ou plusieurs mobiliers peuvent être associés à une US - *relation de 1 à n*  
> **t_prelevement** : table fille permettant d'associer des prélèvements à l'US - *relation de 1 à n*
> **j_rel_us** : table fille permettant d'associer les US entre elles pour en préciser la relation stratigraphique - *relation de n à n*  
> **j_rel_ens** : table fille permettant d'associer les US aux ensembles - *relation de n à n*  
> **j_rel_tranchee** : table fille permettant d'associer les US aux tranchées - *relation de n à n*  
> **j_rel_sondage** : table fille permettant d'associer les US aux sondages - *relation de n à n*  
> **j_us_log** : table fille permettant d'associer les US aux logs stratigraphiques - *relation de n à n*  
> **j_rel_axe** : table fille permettant d'associer les US aux axes - *relation de n à n*  
> **coupe_poly** : table fille permettant d'associer les US à leurs représentations graphiques en coupe - *relation de 1 à n*  
> **coupe_line** : table fille permettant d'associer les US à leurs représentations graphiques en coupe - *relation de 1 à n*  
> **j_rel_photo** : table fille permettant d'associer les photos aux US - *relation de n à n*  
> **j_rel_minute** : table fille permettant d'associer les minutes aux US - *relation de n à n*  

* **t_mobilier**

Cette table permet d'enregistrer toutes les informations concernant le mobilier archéologique. À l'heure actuelle, cette table ne nous satisfait pas totalement. La structure est amenée à évoluer.

| Champ              | Typage               | Description                                                                                                                | Contrainte      |
|--------------------|----------------------|----------------------------------------------------------------------------------------------------------------------------|-----------------|
| **id_mob**         | entier               | clé primaire à incrémentation automatique gérée par SpatiaLite                                                             | unique, non nul |
| numiso             | entier               | numéro attribué par le responsable d'opération                                                                             | unique          |
| iso_lot            | entier               | 0 : isolat ; 1 : lot de mobilier                                                                                           |                 |
| iso_ident          | chaîne de caractères | interprétation du mobilier (liste de valeurs disponible et modifiable dans la table **thesaurus_Badass**)                  |                 |
| iso_ssindent       | chaîne de caractères | interprétation un peu plus fine                                                                                            |                 |
| catego             | chaîne de caractères | grande catégorie du mobilier (liste de valeurs disponible et modifiable dans la table **thesaurus_Badass**)                |                 |
| sscatego           | chaîne de caractères | sous catégorie du mobilier (liste de valeurs disponible et modifiable dans la table **thesaurus_Badass**)                  |                 |
| alt                | réel                 | altitude de mise au jour du mobilier, exprimée en mètre selon le référentiel altimétrique choisi (NGF - IGN69 par exemple) |                 |
| dim_max            | réel                 | dimension maximale du mobilier, exprimée en mètres                                                                         |                 |
| dim_min            | réel                 | dimension minimale du mobilier, exprimée en mètres                                                                         |                 |
| diam               | réel                 | diamètre du mobilier, exprimé en mètres                                                                                    |                 |
| masse              | réel                 | masse du mobilier, exprimée en grammes                                                                                     |                 |
| nr                 | entier               | nombre de reste                                                                                                            |                 |
| pr                 | entier               | 0 : mobilier non prélevé ; 1 : mobilier prélevé                                                                            |                 |
| etatcons           | chaîne de caractères | état de conservation du mobilier (liste de valeurs disponible et modifiable dans la table **thesaurus_Badass**)            |                 |
| datsup             | entier               | borne chronologique supérieure du mobilier en année                                                                        |                 |
| datinf             | entier               | borne chronologique inférieure du mobilier en année                                                                        |                 |
| dat_note           | chaîne de caractères | commentaires sur la datation                                                                                               |                 |
| note               | texte                | commentaires divers                                                                                                        |                 |
| ***numfait***      | entier               | clé étrangère vers la table mère **t_fait**                                                                                |                 |
| ***numus***        | entier               | clé étrangère vers la table mère **t_us**                                                                                  |                 |
| ***numpoint*** | entier               | clé étrangère vers la table mère **point**                                                                                 |                 |

##### Tables filles

> **t_mobilier** : un ou plusieurs mobiliers peuvent être associés à une US - *relation de 1 à n*  
> **j_rel_us** : table fille permettant d'associer les US entre elles pour en préciser la relation stratigraphique - *relation de n à n*  
> **j_rel_ens** : table fille permettant d'associer les US aux ensembles - *relation de n à n*  
> **j_rel_tranchee** : table fille permettant d'associer les US aux tranchées - *relation de n à n*  
> **j_rel_sondage** : table fille permettant d'associer les US aux sondages - *relation de n à n*  
> **j_us_log** : table fille permettant d'associer les US aux logs stratigraphiques - *relation de n à n*  
> **j_rel_axe** : table fille permettant d'associer les US aux axes - *relation de n à n*  
> **coupe_poly** : table fille permettant d'associer les US à leurs représentations graphiques en coupe - *relation de 1 à n*  
> **coupe_line** : table fille permettant d'associer les US à leurs représentations graphiques en coupe - *relation de 1 à n*  
> **j_rel_photo** : table fille permettant d'associer les photos aux US - *relation de n à n*  
> **j_rel_minute** : table fille permettant d'associer les minutes aux US - *relation de n à n*  

* **t_seq**

Cette table permet d'enregistrer toutes les informations concernant les séquences stratigraphiques.

| Champ | Typage | Description | Contrainte |
|-----------|-----------|-----------|-----------|
| **numseq** | entier | clé primaire à incrémentation automatique gérée par SpatiaLite | unique, non nul |
| titre | chaîne de caractères | intitulé de la séquence | |
| datation | chaîne de caractères | datation de la séquence | |
| crit_dat | chaîne de caractères | critère de datation de la séquence | |
| note | chaîne de caractères | commentaires divers | |

##### Tables filles

> **j_seq_phase** : table fille permettant d'associer les phases aux séquences - *relation de n à n*  

* **t_phase**

Cette table permet d'enregistrer toutes les informations concernant les phases stratigraphiques.

| Champ | Typage | Description | Contrainte |
|-----------|-----------|-----------|-----------|
| **id_phase** | entier | clé primaire à incrémentation automatique gérée par SpatiaLite | unique, non nul |
| numphase | entier | clé primaire définie par l'archéologue | unique, non nul |
| titre | chaîne de caractères | intitulé de la phase | |
| dat | chaîne de caractères | datation de la phase | |
| tpq | chaîne de caractères | terminus post quem de la phase | |
| taq | chaîne de caractères | terminus ante quem de la phase | |
| note | chaîne de caractères | commentaires divers | |

##### Tables filles

> **j_phase_per** : table fille permettant d'associer les phases aux périodes - *relation de n à n*  

* **t_periode**

Cette table permet d'enregistrer toutes les informations concernant les périodes stratigraphiques.

| Champ     | Typage               | Description                                                    | Contrainte      |
|-----------|----------------------|----------------------------------------------------------------|-----------------|
| id_period | entier               | clé primaire à incrémentation automatique gérée par SpatiaLite | unique, non nul |
| numperiod | entier               | clé primaire définie par l'archéologue                         | unique, non nul |
| titre     | chaîne de caractères | intitulé de la période                                         |                 |
| datation  | chaîne de caractères | datation de la période                                         |                 |
| date_inf  | entier                     | borne chronologique absolue minimale                                                               |                 |
| date_sup  | entier                     | borne chronologique absolue maximale                                                               |                 |
| note  | chaîne de caractères | commentaires divers                                            |                 |

##### Tables filles

> **j_phase_per** : table fille permettant d'associer les phases aux périodes - *relation de n à n*  

* **t_minute**

Cette table permet d'enregistrer toutes les informations concernant les minutes.

| Champ         | Typage               | Description                                                                       | Contrainte      |
|---------------|----------------------|-----------------------------------------------------------------------------------|-----------------|
| **id_minut**  | entier               | clé primaire à incrémentation automatique gérée par SpatiaLite                    | unique, non nul |
| numinute      | entier               | numéro attribué par le responsable d'opération                                    | unique          |
| descr         | chaîne de caractères | courte description de la minute                                                   |                 |
| echelle       | chaîne de caractères | échelle du relevé                                                                 |                 |
| contenu       | chaîne de caractères | récapitulatif du contenu de la minute                                             |                 |
| creator       | chaîne de caractères | auteur du relevé                                                                  |                 |
| format        | chaîne de caractères | format de la page du relevé                                                       |                 |
| support       | chaîne de caractères | type de support                                                                   |                 |
| scan          | entier               | 0 : pas scanné ; 1 : scanné                                                       |                 |
| dao           | entier               | 0 : pas vectorisé ; 1 : vectorisé                                                 |                 |
| ***numodel*** | entier               | clé étrangère de relation avec le modèle photogrammétrique utilisé pour le relevé |                 |

##### Tables filles

> **j_rel_minute** : table fille permettant d'associer les minutes aux ensembles, faits, isolats, sondages, tranchées ou unités stratigraphiques - *relation de n à n*  

* **t_photo**

Cette table permet d'enregistrer toutes les informations concernant les photographies.

| Champ        | Typage               | Description                                                    | Contrainte      |
|--------------|----------------------|----------------------------------------------------------------|-----------------|
| **id_photo** | entier               | clé primaire à incrémentation automatique gérée par SpatiaLite | unique, non nul |
| numphoto     | chaîne de caractères | numéro du cliché                                               | unique          |
| nomfichier   | chaîne de caractères | nom du fichier                                                 |                 |
| chemin_url   | chaîne de caractères | lien hypertexte vers le fichier                                |                 |
| legend       | chaîne de caractères | courte description de la photo                                 |                 |
| vue_vers     | chaîne de caractères | sens de prise de vue de la photo                               |                 |
| creator      | chaîne de caractères | auteur de la photo                                             |                 |
| datephoto    | date                 | date de la photo                                               |                 |
| type         | chaîne de caractères | type de cliché                                                 |                 |
| support      | chaîne de caractères | support de la photo                                            |                 |


##### Tables filles

> **j_rel_photo** : table fille permettant d'associer les photos aux ensembles, faits, isolats, sondages, tranchées ou unités stratigraphiques - *relation de n à n*  

* **t_photogram**

Cette table permet d'enregistrer toutes les informations concernant les modèles issues des post-traitements photogrammétriques.

| Champ            | Typage               | Description                                                    | Contrainte      |
|------------------|----------------------|----------------------------------------------------------------|-----------------|
| **id_photogram** | entier               | clé primaire à incrémentation automatique gérée par SpatiaLite | unique, non nul |
| marqueAPN | chaîne de caractères | marque de l'appareil photo |   |
| modelcam | chaîne de caractères | modèle d'appareil photo |    |
| larg_captmm | réel | largeur du capeur en mm |    |
| haut_captmm | réel | hauteur du capteur en mm |    |
| larg_impx | entier | largeur de l'image en pixels |    |
| haut_impx | entier | hauteur de l'image en pixels |    |
| iso | entier | iso de l'APN |    |
| ouverture | chaîne de caractères | ouverture de l'APN |    |
| focale | entier | focale de l'APN |    |
| wb | chaîne de carctères | balance des blancs |    |
| modecam | chaîne de caractères | mode de l'APN lors des prises de vues, multivarié (manuel, priorité ouverture, priorité vitesse, automatique) |    |
| obj_model | chaîne de caractères | multivarié, objectif(s) du modèle (calibration, ortho zenithale, ortho coupe, mns, profils) |    |
| legend | chaîne de caractères | description du ou des sujet(s) de la scène aussi bref et limpide que possible |    |
| nomodel          | chaîne de caractères | nom du modèle attribué par l'opérateur                      | unique          |
| urljob | chaîne de caractères | url du dossier de travail |    |
| datemodel | date | date de début du traitement du modèle |    |
| creator | chaîne de caractères | auteur du traitement |    |
| convers | entier | étape de conversion raw réalisée = 1 |    |
| calib | entier | étape de calibration réalisée = 1 |    |
| aerotr | entier | étape d'aérotriangulation réalisée = 1 |    |
| georef | entier | étape géoréférencement réalisée = 1 |    |
| nuagedense | entier | nombre de nuages denses créés |    |
| mns | entier | nombre de modèles numériques de surface créés |    |
| orthoimage | entier | nombre d'orthoimages créées |    |
| maillage | entier | nombre de maillages créés |    |
| texture | entier | nombre de textures créées |    |
| comment | texte | commentaires divers, étapes de calcul (erreur, réussites etc.) |    | 


##### Tables filles

> **t_prodphotogram** : table fille permettant d'associer les produits issus de la photogrammétrie (orthoimages, modèles numériques de surface...)
> **j_rel_photo** : table fille permettant d'associer les photos aux modèles de photogrammétrie - *relation de n à n*
> **j_rel_photogram** : table fille permettant d'associer les points topos aux modèles de photogrammétrie - *relation de n à n*

* **t_prodphotogram**

Cette table permet d'enregistrer tous les produits issus de la photogrammétrie (orthoimages, modèles numériques de surface...).

| Champ            | Typage               | Description                                                    | Contrainte      |
|------------------|----------------------|----------------------------------------------------------------|-----------------|
| **id_prodphotogram** | entier               | clé primaire à incrémentation automatique gérée par SpatiaLite | unique, non nul |
| ***id_photogram*** | entier | clé étrangère, référence de la table **t_photogram** |    |
| url_prod | chaîne de caractères | chemin vers le produit |    |
| nomprod | chaîne de caractères | nom du produit |    |
| typrod | chaîne de caractères | type(s) de produit (orthoimage, mns, nuage de points etc.) |    |
| note | texte | (orthoimage, mns, nuage de points etc.) |    |


* **thesaurus_badass**

Cette table permet d'enregistrer toutes les listes de valeurs de Badass. La base de données possède déjà une série de liste de valeurs inscrites dans ce thésaurus. Il est possible de modifier, ajouter des valeurs au besoin selon les problématiques de l'opération.

| Champ | Typage | Description | Contrainte |
|-----------|-----------|-----------|-----------|
| **id_thes** | entier | clé primaire à incrémentation automatique gérée par SpatiaLite | unique, non nul |
| tabl_thes | chaîne de caractères | table où on trouve le champs qui propose la valeur | |
| field_thes | chaîne de caractères | champ qui propose le terme en liste de valeur | |
| val_thes | chaîne de caractères | il s'agit de la valeur / terme / modalité | |
| def_thes | chaîne de caractères | si possible, définition du terme | |
| cat_thes | chaîne de caractères | catégorie de regroupement des termes | |
| comment | chaîne de caractères | observations diverses | |

##### Tables filles

> néant

#### Fonctionnement du thésaurus

Comme exposé *supra*, le thésausus permet d'enregistrer dans la base de données des valeurs normalisées à saisir dans les champs de certaines tables. Ce thésaurus peut-être compléter et modifier au besoin de chaque utilisateur.
Les modifications s'effectuent comme suit :

* éditer la couche thesaurus_badass et ouvrir la table d'attributs
* faire un tri sur la colonne "tabl_thes" et sur le champ "field_thes" pour afficher les listes par table attributaire avec l'expression

![Tri sur les colonnes du thésaurus][005c]

```sql
 "tabl_thes" || "field_thes"
 ```
* repérer la table et le champ sur lequel els modifications doivent être et modifier la valeur dans le champ "val_thes"
* si une nouvelle valeur doit apparaître, ajouter un enregistrement, remplir les champs "tabl_thes" et "field_thes" à l'identique et remplir avec la nouvelle valeur le champ "val_thes" (ex : pour modifier les valeurs pour le champ "interpret" de la table **t_fait** remplir la nouvelle valeur 't_fait' pour le champ "tabl_thes", 'interpret' pour le champ "field_thes" et la nouvelle valeur à ajouter dans le champ "val_thes" )
* il est possible d'ajouter une définition (champ "def_thes") et un commentaire (champ "comment") pour aider l'opérateur de saisie dans l'utilisation de la valeur
* enregistrer et quitter le mode édition
* réouvrir le formulaire dont la liste a été modifiée pour vérifier l'application de la nouvelle valeur 



#### Les tables de création rapides d'entités

À l'origine du projet, notre souhait était de développer la base de données pour une mise en œuvre dès le terrain. La mise à disposition de smartphones et de tablettes durcies par l'institut constituait un moyen de saisie informatique lors de la fouille. 
La saisie sur le terrain implique des contraintes difficilement contournables. Le temps disponible pour l'enregistrement doit être optimisé pour être compétitif par rapport à un enregistrement classique : papier/crayon puis enregistrement numérique au bureau. L'enregistrement numérique sur smartphone ou tablette se fait sur des écrans de faibles dimensions ce qui oblige à penser des interfaces logiciels les plus restreintes possibles. De plus, sans clavier ou souris, l'accès aux rubriques et l'acquisition des données doit être ergonomique et adapté aux interfaces tactiles. 

Plusieurs pistes ont été envisagées : 
- l'utilisation d'une interface web offrant les formulaires de saisie dans la base de données
- l'utilisation de QField (![QField](https://qfield.org/)), application Android, Apple et Windows associée à QGIS pour une saisie et une consultation des données attributaires et spatiales

Une première interface web a été développée par Florent Mercey en PHP (Eater : Enregistrement Archéologique de TERrain). Cette première expérimentation fût concluante, mais nécessite des connaissances en informatique poussée et une réinterprétation totale de la base de données et des fonctions à inclure dans l'interface.

QField fait l'objet d'un développement constant et très actif qui mérite d'éprouver l'application très régulièrement pour en appréhender les possibilités. Qfield fonctionne avec un projet développé sur QGIS et reconnaît une grande partie des fonctionnalités attendues pour une base de données en plus de celles de QGIS et d'ajouts propres à l'application :
- reconnaissance des relations
- formulaires
- numérisation et interface pensés pour les écrans tactiles et de petites tailles
- utilisation du positionnement GPS du dispositif de saisie

À l'origine, le projet ouvert dans QField interroge une base de données au format Geopackage. Ce dernier est une variante du format SpatiaLite. À l'occasion d'un test de l'application, nous avons pu constater que QField était en mesure d'ouvrir directement un projet et une base de données Badass tels quels, sans aucune conversion. Les formulaires mis en place dans QGIS sont accessibles dans QField avec l'ensemble des fonctionnalités attendues. Par contre, il n'est pas possible à l'heure actuelle, d'acquérir des données dans des tables qui possèdent une géométrie sans passer au préalable par la numérisation d'une forme. Cet écueil contredit le postulat de départ de développement de Badass, à savoir, la saisie des données archéologiques attributaires dont les géométries sont ajoutées a posteriori, lors de la restitution des levés topographiques.

Lors d'une discussion entre Bénédicte Guillot (Inrap, Grand-Quevilly), Clément Féliu (Inrap, Strasbourg) et le collectif Ramen lors de la préparation de l'enregistrement pour une opération archéologique préventive, l'idée fût amenée de développer des tables et un formulaire unique pour créer des entités en séries. Sans géométrie, cette table, très succinte, peut parfaitement prendre place dans QField.
Cet outil comporte deux tables et une vue associée.

![Tables et vues ajoutées à Badass pour la création rapide d'entités][005b]

* **serie**

Cette table permet d'enregistrer les séries qui définiront la numérotation des entités. Ses séries devront être prévues avant l'enregistrement. Par exemple, pour la numérotation des faits de 1 à n, pour la numérotation des US du secteur 1 de 1000 à 1999, pour la numérotation des US du secteur 2 de 2000 à 2999, etc.

| Champ | Typage | Description | Contrainte |
|-----------|-----------|-----------|-----------|
| **id_serie** | entier | clé primaire à incrémentation automatique gérée par SpatiaLite | unique, non nul |
| nombre_depart | entier | nombre de départ de la numérotation (par exemple, 1000) | |
| nombre_arrivee | entier | nombre d'arrivée de la numérotation (par exemple, 1999) | |
| pas | entier | pas de l'incrémentation. Peut-être 2 si numérotation paire, ou impaire | |
| tablentite | chaîne de caractères | le nom de la table cible de l'enregistrement | |
| note | chaîne de caractères | observations diverses | |

##### Tables filles

> **entite_rapide** : table de création d'entités


* **entite_rapide**

Cette table permet d'enregistrer les entités selon une série. L'incrémentation du numéro se base sur la série définie ainsi que sur les valeurs déjà présentes dans les tables ciblées.

| Champ | Typage | Description | Contrainte |
|-----------|-----------|-----------|-----------|
| **id_entite** | entier | clé primaire à incrémentation automatique gérée par SpatiaLite | unique, non nul |
| ***id_serie*** | entier | clé étrangère qui renvoie à la table mère **serie** | |
| numentite | entier | numéro attribué à la nouvelle entité | |
| interpret | chaîne de caractère | valeur d'interprétation issue du thésaurus (pour les faits et les US seuls) | |
| note | chaîne de caractères | observations diverses | |

##### Tables filles

> néant

##### La vue associée (**vue_numaximum_serie**)

Cette vue permet, de trouver, dans les tables cibles, les valeurs maximales déjà saisies par série définie dans la table **serie**. Un nouveau numéro est alors calculé dans la colonne "new_numentite". S'il n'y a pas de données déjà saisies dans les tables filles, alors le numéro de départ de série sera utilisé comme nouveau numéro.

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| vue_numaximum_serie              | Vue d'aggrégation des valeurs maximales des numéros d'entité par séries définies dans la table série                                                                                                                                                                                          | null      |

#### SQL

```sql
CREATE VIEW vue_numaximum_serie AS
SELECT s."id_serie", 't_us' AS "tablentite", max(t."numus") AS "numaximum",
				CASE WHEN max(t."numus") IS NULL THEN s."nombre_depart" ELSE max(t."numus")+s."pas" END AS "new_numentite"
				FROM serie AS s 
				LEFT JOIN t_us AS t ON t."numus" >= s."nombre_depart" AND t."numus" <= s."nombre_arrivee"
				WHERE s."tablentite" like 't_us'
				GROUP BY s."id_serie"
				UNION
				SELECT s."id_serie", 't_fait' AS "tablentite", max(t."numfait") AS "numaximum",
				CASE WHEN max(t."numfait") IS NULL THEN s."nombre_depart" ELSE max(t."numfait")+s."pas" END AS "new_numentite"
				FROM serie AS s 
				LEFT JOIN t_fait AS t ON t."numfait" >= s."nombre_depart" AND t."numfait" <= s."nombre_arrivee"
				WHERE s."tablentite" like 't_fait'
				GROUP BY s."id_serie"
				UNION
				SELECT s."id_serie", 't_axe' AS "tablentite", max(t."numaxe") AS "numaximum",
				CASE WHEN max(t."numaxe") IS NULL THEN s."nombre_depart" ELSE max(t."numaxe")+s."pas" END AS "new_numentite"
				FROM serie AS s
				LEFT JOIN t_axe AS t ON t."numaxe" >= s."nombre_depart" AND t."numaxe" <= s."nombre_arrivee"
				WHERE s."tablentite" like 't_axe'
				GROUP BY s."id_serie"
				UNION
				SELECT s."id_serie", 't_ens' AS "tablentite", max(t."numens") AS "numaximum",
				CASE WHEN max(t."numens") IS NULL THEN s."nombre_depart" ELSE max(t."numens")+s."pas" END AS "new_numentite"
				FROM serie AS s
				LEFT JOIN t_ens AS t ON t."numens" >= s."nombre_depart" AND t."numens" <= s."nombre_arrivee"
				WHERE s."tablentite" like 't_ens'
				GROUP BY s."id_serie"
				UNION
				SELECT s."id_serie", 't_log' AS "tablentite", max(t."numlog") AS "numaximum",
				CASE WHEN max(t."numlog") IS NULL THEN s."nombre_depart" ELSE max(t."numlog")+s."pas" END AS "new_numentite"
				FROM serie AS s
				LEFT JOIN t_log AS t ON t."numlog" >= s."nombre_depart" AND t."numlog" <= s."nombre_arrivee"
				WHERE s."tablentite" like 't_log'
				GROUP BY s."id_serie"
				UNION
				SELECT s."id_serie", 't_mobilier' AS "tablentite", max(t."numiso") AS "numaximum",
				CASE WHEN max(t."numiso") IS NULL THEN s."nombre_depart" ELSE max(t."numiso")+s."pas" END AS "new_numentite"
				FROM serie AS s
				LEFT JOIN t_mobilier AS t ON t."numiso" >= s."nombre_depart" AND t."numiso" <= s."nombre_arrivee"
				WHERE s."tablentite" like 't_mobilier'
				GROUP BY s."id_serie"
				UNION
				SELECT s."id_serie", 't_prelevement' AS "tablentite", max(t."numprel") AS "numaximum",
				CASE WHEN max(t."numprel") IS NULL THEN s."nombre_depart" ELSE max(t."numprel")+s."pas" END AS "new_numentite"
				FROM serie AS s
				LEFT JOIN t_prelevement AS t ON t."numprel" >= s."nombre_depart" AND t."numprel" <= s."nombre_arrivee"
				WHERE s."tablentite" like 't_prelevement'
				GROUP BY s."id_serie"
				UNION
				SELECT s."id_serie", 't_sondage' AS "tablentite", max(t."numsd") AS "numaximum",
				CASE WHEN max(t."numsd") IS NULL THEN s."nombre_depart" ELSE max(t."numsd")+s."pas" END AS "new_numentite"
				FROM serie AS s
				LEFT JOIN t_sondage AS t ON t."numsd" >= s."nombre_depart" AND t."numsd" <= s."nombre_arrivee"
				WHERE s."tablentite" like 't_sondage'
				GROUP BY s."id_serie"
				UNION
				SELECT s."id_serie", 't_tranchee' AS "tablentite", max(t."numtr") AS "numaximum",
				CASE WHEN max(t."numtr") IS NULL THEN s."nombre_depart" ELSE max(t."numtr")+s."pas" END AS "new_numentite"
				FROM serie AS s
				LEFT JOIN t_tranchee AS t ON t."numtr" >= s."nombre_depart" AND t."numtr" <= s."nombre_arrivee"
				WHERE s."tablentite" like 't_tranchee'
				GROUP BY s."id_serie"
```


##### Les triggers associés

Deux triggers ont été mis en place pour ces tables. Le premier permet d'insérer la valeur du nouveau numéro d'entité dans la table **entite_rapide** puis de la vider pour éviter des modifications de valeurs sur cette table qui viendrait risquer d'altérer l'intégrité des données. Une fois l'enregistrement ajouté dans la table **entite_rapide** et selon la série définie, le second trigger crée l'entité correspondante dans la table cible.

```sql
/*Trigger associé aux tables de création rapide pour l'ajout du nouveau numéro d'entité selon la vue vue_numaximum_serie*/
DROP TRIGGER IF EXISTS trgai_entite_rapide_newnumentite;
CREATE TRIGGER trgai_entite_rapide_newnumentite
	AFTER INSERT
	ON entite_rapide
	FOR EACH ROW
	WHEN NEW."id_serie" IS NOT NULL
		BEGIN
		UPDATE entite_rapide SET "numentite" = 
			(SELECT "new_numentite" 
			FROM (
				SELECT e."id_entite", 't_us' AS "tablentite", max(t."numus") AS "numaximum",
				CASE WHEN max(t."numus") IS NULL THEN s."nombre_depart" ELSE max(t."numus")+s."pas" END AS "new_numentite"
				FROM entite_rapide AS e 
				LEFT JOIN serie AS s ON e."id_serie" = s."id_serie"
				LEFT JOIN t_us AS t ON t."numus" >= s."nombre_depart" AND t."numus" <= s."nombre_arrivee"
				WHERE s."tablentite" like 't_us'
				GROUP BY s."id_serie"
				UNION
				SELECT e."id_entite", 't_fait' AS "tablentite", max(t."numfait") AS "numaximum",
				CASE WHEN max(t."numfait") IS NULL THEN s."nombre_depart" ELSE max(t."numfait")+s."pas" END AS "new_numentite"
				FROM entite_rapide AS e
				LEFT JOIN serie AS s ON e."id_serie" = s."id_serie"
				LEFT JOIN t_fait AS t ON t."numfait" >= s."nombre_depart" AND t."numfait" <= s."nombre_arrivee"
				WHERE s."tablentite" like 't_fait'
				GROUP BY s."id_serie"
				UNION
				SELECT e."id_entite", 't_axe' AS "tablentite", max(t."numaxe") AS "numaximum",
				CASE WHEN max(t."numaxe") IS NULL THEN s."nombre_depart" ELSE max(t."numaxe")+s."pas" END AS "new_numentite"
				FROM entite_rapide AS e 
				LEFT JOIN serie AS s ON e."id_serie" = s."id_serie"
				LEFT JOIN t_axe AS t ON t."numaxe" >= s."nombre_depart" AND t."numaxe" <= s."nombre_arrivee"
				WHERE s."tablentite" like 't_axe'
				GROUP BY s."id_serie"
				UNION
				SELECT e."id_entite", 't_ens' AS "tablentite", max(t."numens") AS "numaximum",
				CASE WHEN max(t."numens") IS NULL THEN s."nombre_depart" ELSE max(t."numens")+s."pas" END AS "new_numentite"
				FROM entite_rapide AS e
				LEFT JOIN serie AS s ON e."id_serie" = s."id_serie"
				LEFT JOIN t_ens AS t ON t."numens" >= s."nombre_depart" AND t."numens" <= s."nombre_arrivee"
				WHERE s."tablentite" like 't_ens'
				GROUP BY s."id_serie"
				UNION
				SELECT e."id_entite", 't_log' AS "tablentite", max(t."numlog") AS "numaximum",
				CASE WHEN max(t."numlog") IS NULL THEN s."nombre_depart" ELSE max(t."numlog")+s."pas" END AS "new_numentite"
				FROM entite_rapide AS e
				LEFT JOIN serie AS s ON e."id_serie" = s."id_serie"
				LEFT JOIN t_log AS t ON t."numlog" >= s."nombre_depart" AND t."numlog" <= s."nombre_arrivee"
				WHERE s."tablentite" like 't_log'
				GROUP BY s."id_serie"
				UNION
				SELECT e."id_entite", 't_mobilier' AS "tablentite", max(t."numiso") AS "numaximum",
				CASE WHEN max(t."numiso") IS NULL THEN s."nombre_depart" ELSE max(t."numiso")+s."pas" END AS "new_numentite"
				FROM entite_rapide AS e
				LEFT JOIN serie AS s ON e."id_serie" = s."id_serie"
				LEFT JOIN t_mobilier AS t ON t."numiso" >= s."nombre_depart" AND t."numiso" <= s."nombre_arrivee"
				WHERE s."tablentite" like 't_mobilier'
				GROUP BY s."id_serie"
				UNION
				SELECT e."id_entite", 't_prelevement' AS "tablentite", max(t."numprel") AS "numaximum",
				CASE WHEN max(t."numprel") IS NULL THEN s."nombre_depart" ELSE max(t."numprel")+s."pas" END AS "new_numentite"
				FROM entite_rapide AS e
				LEFT JOIN serie AS s ON e."id_serie" = s."id_serie"
				LEFT JOIN t_prelevement AS t ON t."numprel" >= s."nombre_depart" AND t."numprel" <= s."nombre_arrivee"
				WHERE s."tablentite" like 't_prelevement'
				GROUP BY s."id_serie"
				UNION
				SELECT e."id_entite", 't_sondage' AS "tablentite", max(t."numsd") AS "numaximum",
				CASE WHEN max(t."numsd") IS NULL THEN s."nombre_depart" ELSE max(t."numsd")+s."pas" END AS "new_numentite"
				FROM entite_rapide AS e
				LEFT JOIN serie AS s ON e."id_serie" = s."id_serie"
				LEFT JOIN t_sondage AS t ON t."numsd" >= s."nombre_depart" AND t."numsd" <= s."nombre_arrivee"
				WHERE s."tablentite" like 't_sondage'
				GROUP BY s."id_serie"
				UNION
				SELECT e."id_entite", 't_tranchee' AS "tablentite", max(t."numtr") AS "numaximum",
				CASE WHEN max(t."numtr") IS NULL THEN s."nombre_depart" ELSE max(t."numtr")+s."pas" END AS "new_numentite"
				FROM entite_rapide AS e
				LEFT JOIN serie AS s ON e."id_serie" = s."id_serie"
				LEFT JOIN t_tranchee AS t ON t."numtr" >= s."nombre_depart" AND t."numtr" <= s."nombre_arrivee"
				WHERE s."tablentite" like 't_tranchee'
				GROUP BY s."id_serie")
			WHERE NEW."id_entite" = "id_entite");
			DELETE FROM entite_rapide;
END;

/*Trigger associé à la table de création d'entité rapide qui permet d'ajouter les enregistrements dans les tables ciblées*/

--pour t_axe
CREATE TRIGGER trgau_t_axe_maj_entite_rapide
   AFTER UPDATE OF "numentite"
   ON entite_rapide
FOR EACH ROW
	WHEN NEW."numentite" IS NOT NULL AND (SELECT s."tablentite" FROM entite_rapide AS e JOIN serie AS s ON OLD."id_serie" = s."id_serie") like 't_axe'
	BEGIN
	INSERT INTO t_axe("numaxe","geometry") VALUES (NEW."numentite", NULL);
END;

--pour t_ens
CREATE TRIGGER trgau_t_ens_maj_entite_rapide
   AFTER UPDATE OF "numentite"
   ON entite_rapide
FOR EACH ROW
	WHEN NEW."numentite" IS NOT NULL AND (SELECT s."tablentite" FROM entite_rapide AS e JOIN serie AS s ON OLD."id_serie" = s."id_serie") like 't_ens'
	BEGIN
	INSERT INTO t_ens("numens","typens","geometry") VALUES (NEW."numentite", NEW."interpret",NULL);
END;

--pour t_fait
CREATE TRIGGER trgau_t_fait_maj_entite_rapide
   AFTER UPDATE OF "numentite"
   ON entite_rapide
FOR EACH ROW
	WHEN NEW."numentite" IS NOT NULL AND (SELECT s."tablentite" FROM entite_rapide AS e JOIN serie AS s ON OLD."id_serie" = s."id_serie") like 't_fait'
	BEGIN
	INSERT INTO t_fait("numfait","interpret","geometry") VALUES (NEW."numentite",NEW."interpret",NULL);
END;

--pour t_log
CREATE TRIGGER trgau_t_log_maj_entite_rapide
   AFTER UPDATE OF "numentite"
   ON entite_rapide
FOR EACH ROW
	WHEN NEW."numentite" IS NOT NULL AND (SELECT s."tablentite" FROM entite_rapide AS e JOIN serie AS s ON OLD."id_serie" = s."id_serie") like 't_log'
	BEGIN
	INSERT INTO t_log("numlog","geometry") VALUES (NEW."numentite", NULL);
END;

--pour t_mobilier
CREATE TRIGGER trgau_t_mobilier_maj_entite_rapide
   AFTER UPDATE OF "numentite"
   ON entite_rapide
FOR EACH ROW
	WHEN NEW."numentite" IS NOT NULL AND (SELECT s."tablentite" FROM entite_rapide AS e JOIN serie AS s ON OLD."id_serie" = s."id_serie") like 't_mobilier'
	BEGIN
	INSERT INTO t_mobilier("numiso","matiere","geometry") VALUES (NEW."numentite", NEW."interpret",NULL);
END;

--pour t_prelevement
CREATE TRIGGER trgau_t_prelevement_maj_entite_rapide
   AFTER UPDATE OF "numentite"
   ON entite_rapide
FOR EACH ROW
	WHEN NEW."numentite" IS NOT NULL AND (SELECT s."tablentite" FROM entite_rapide AS e JOIN serie AS s ON OLD."id_serie" = s."id_serie") like 't_prelevement'
	BEGIN
	INSERT INTO t_prelevement("numprel","geometry") VALUES (NEW."numentite", NULL);
END;

--pour t_sondage
CREATE TRIGGER trgau_t_sondage_maj_entite_rapide
   AFTER UPDATE OF "numentite"
   ON entite_rapide
FOR EACH ROW
	WHEN NEW."numentite" IS NOT NULL AND (SELECT s."tablentite" FROM entite_rapide AS e JOIN serie AS s ON OLD."id_serie" = s."id_serie") like 't_sondage'
	BEGIN
	INSERT INTO t_sondage("numsd","geometry") VALUES (NEW."numentite", NULL);
END;

--pour t_tranchee
CREATE TRIGGER trgau_t_tranchee_maj_entite_rapide
   AFTER UPDATE OF "numentite"
   ON entite_rapide
FOR EACH ROW
	WHEN NEW."numentite" IS NOT NULL AND (SELECT s."tablentite" FROM entite_rapide AS e JOIN serie AS s ON OLD."id_serie" = s."id_serie") like 't_tranchee'
	BEGIN
	INSERT INTO t_tranchee("numtr","geometry") VALUES (NEW."numentite", NULL);
END;

--pour t_us
CREATE TRIGGER trgau_t_us_maj_entite_rapide
   AFTER UPDATE OF "numentite"
   ON entite_rapide
FOR EACH ROW
	WHEN NEW."numentite" IS NOT NULL AND (SELECT s."tablentite" FROM entite_rapide AS e JOIN serie AS s ON OLD."id_serie" = s."id_serie") like 't_us'
	BEGIN
	INSERT INTO t_us("numus","nature_us","geometry") VALUES (NEW."numentite", NEW."interpret",NULL);
END;
```

#### Les tables d'enregistrement des coupes dans la base de données

Considérant que les données spatiales liées à une base de données relationnelle d'enregistrement archéologique comprennent également l'ensemble des relevés stratigraphiques effectués pendant l'exploration archéologique, les procédés de numérisation et d'intégration dans un SIG développés depuis 2020 dans le réseau des référents SIG ont logiquement pris place dans Badass.

Le guide complet de ce travail peut-être consulté ici :

[Coupes stratigraphiques et QGIS](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/coupe_SIG/coupes_QGIS.html)

##### Quelques rappels

Les coupes stratigraphiques sont par définition des données verticales. Il n'existe pas de 3ème dimension dans QGIS mais il est néanmoins possible de détourner le système en deux dimensions du logiciel. En effet, le repère orthonormé d'origine se présente en abscisse en x et en ordonnée en y. Il est possible de considérer l'axe des ordonnées comme l'axe des z. Ainsi un objet dont les coordonnées seraient en x : 526262 m, en y : 6701985 m et en z : 98 m se trouverait en deux endroits dans le canevas de QGIS :

* en plan, aux coordonnées x et y habituelles selon le système de projection définie
* en pseudo-projection, aux coordonnées x (d'origine ou recalculé selon un axe de projection) et y = z soit pour cet exemple 98 m.

La vectorisation des relevés de coupe dans QGIS a de nombreux avantages. Parmi ceux-ci, les plus évidents sont :

* la possibilité de faire du traitement de données spatiales en exploitant l'enregistrement stratigraphique des archéologues
* l'exploitation possible des informations spatiales et géométriques (surface, profondeurs et altitudes...)
* du fait de la numérisation à l’échelle 1:1, la possibilité d’export dans toutes les échelles sans avoir recours à une mise à l’échelle des annotations et symboles (contrairement aux logiciels de DAO)
* la possibilité de gérer les symboles selon les attributs des données sur l’ensemble des entités numérisés pour une mise en page homogène et immédiate
* l'exploitation des outils d'atlas et de variables dans QGIS qui permettent de constituer des catalogues intégrant plans, coupes, données attributaires et clichés par exemple

##### Description

Trois tables constituent la structure des données spatiales liées aux coupes stratigraphiques dans Badass. Le lien entre la représentation géométrique des coupes et le plan se fait par relation de un à un entre la représentation des axes en plan, **t_axe** et la représentation des axes en coupe, **coupe_axe**.

![Extrait du modèle logique de Badass, recentré sur les tables liées aux coupes stratigraphiques][006]

* **t_axe**

[cf. chapitre de description de la table **t_axe**](https://gitlab.com/projet-r-d-bddsarcheo/bdds_archeo/-/blob/master/reflexions_methodologiques/bible/reseauSIG_BADASS.md#tables-filles-1)

* **coupe_axe**

Cette table permet de visualiser et d'enregistrer les informations associées à la représentation de l'axe en plan.

| Champ      | Typage    | Description                                                    | Contrainte                                                 |
|------------|-----------|----------------------------------------------------------------|------------------------------------------------------------|
| **id_axe** | entier    | clé primaire à incrémentation automatique gérée par SpatiaLite | unique, non nul, **doit être égal à ***t_axe***."id_axe"** |
| numminute  | entier    | n° de minute associé à la coupe                                |                                                            |
| alti       | décimal   | altitude de l'axe de relevé                                    |                                                            |
| azimuth    | décimal   |                                                                |                                                            |
| geometry   | polyligne | géométrie de l'entité                                          |                                                            |

##### Tables filles

> **coupe_poly** : table fille permettant d'associer l'ensemble des polygones numérisés à l'axe - *relation de 1 à n*
> **coupe_line** : table fille permettant d'associer l'ensemble des polylignes numérisés à l'axe - *relation de 1 à n*

* **coupe_poly**

Il s’agit d’une couche vectorielle de polygones, ou de multipolygones, qui représente les surfaces observées ou restituées sur la coupe. Ces surfaces sont de deux natures : 

* les US 
* les US dont on a pas vues les limites à la fouille (encaissant par exemple)
* les inclusions (mobilier, matériaux divers, écofacts…). 

| Champ        | Typage              | Description                                                                                                                                               | Contrainte      |
|--------------|---------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------|
| **id_cpoly** | entier              | clé primaire à incrémentation automatique gérée par SpatiaLite                                                                                            | unique, non nul |
| numfait      | entier              | n° de fait, s'il existe, indispensable seulement pour la représentation des unités stratigraphiques                                                       |                 |
| numus        | entier              | n° d'US, s'il existe, indispensable seulement pour la représentation des unités stratigraphiques                                                          |                 |
| numsd        |                     |                                                                                                                                                           |                 |
| typolyc      | chaîne de caractère | distingue les polygones selon leur nature, US, inclusion                                                                                                  |                 |
| detail       | chaîne de caractère | permet d'ajouter une description succincte du polygone dans le cas où le lien avec les autres tables n'est pas possible (pour les inclusions par exemple) |                 |
| geometry     | polygone            | géométrie de l'entité                                                                                                                                     |                 |
| ***id_axe*** | entier              | clé étrangère vers la table mère **coupe_axe**                                                                                                            |                 |

##### Tables filles

> **t_us** : une ou plusieurs unités stratigraphiques peuvent être associés à une coupe - *relation de 1 à n*  

* **coupe_line**

Il s’agit d’une couche vectorielle de polylignes, ou de multipolylignes, et représentent les interfaces observées ou restituées. Elles sont de trois natures : 

* la limite d’observation de la coupe (limite de décapage, profondeur du sondage…), souvent représentée en trait-point. 
* les US négatives (limite de creusement par exemple), souvent représentées par un trait plus épais
* les restitutions d’interfaces d’US, souvent représentées par un pointillé  
Cette table contient la clé étrangère de l’identifiant d'axe. Le numéro de fait et d'US sont nécessaires pour les interfaces correspondants aux US négatives seulement.

| Champ         | Typage              | Description                                                                                         | Contrainte      |
|---------------|---------------------|-----------------------------------------------------------------------------------------------------|-----------------|
| **id_cpline** | entier              | clé primaire à incrémentation automatique gérée par SpatiaLite                                      | unique, non nul |
| numfait       | entier              | n° de fait, s'il existe, indispensable seulement pour la représentation des unités stratigraphiques |                 |
| numus         | entier              | n° d'US, s'il existe, indispensable seulement pour la représentation des unités stratigraphiques    |                 |
| numsd         |                     |                                                                                                     |                 |
| typline       | chaîne de caractère | distingue les polygones selon leur nature, US, inclusion                                            |                 |
| geometry      | polyligne           | géométrie de l'entité                                                                               |                 |
| ***id_axe***  | entier              | clé étrangère vers la table mère **coupe_axe**                                                      |                 |

##### Tables filles

> **t_us** : une ou plusieurs unités stratigraphiques peuvent être associés à une coupe - *relation de 1 à n*  

##### Exemple de représentation de la coupe dans QGIS

* Exemple de représentation d'une coupe dans le canevas de QGIS

![Exemple de représentation d'une coupe dans le canevas de QGIS][007]

* Exemple d'export de coupe depuis QGIS

![Exemple d'export de coupe depuis QGIS][008]

#### Les Six couches

Pour ces six couches, la structure des tables s'appuient sur le modèle préconisé pour les données spatiales d'une opération. Un simple copier/coller entre les shapes produits par le topographe vers les couches de Badass permet leur importation dans la base.

* **emprise**

Il s'agit d'unité technique qui représente la ou les emprises de l’opération sous forme de polygone. Il s'agit de l'entrée administrative de l'opération à la réception d'un arrêté de prescription.

| Champ | Typage | Description | Contrainte |
|-----------|-----------|-----------|-----------|
| **id_emprise** | entier | clé primaire à incrémentation automatique gérée par SpatiaLite | unique, non nul |
| numope | chaîne de caractères | numéro attribué par le Système de Gestion Administratif (SGA) de l'Inrap | |
| typemp | chaîne de caractères | origine de l'emprise (prescrite, réelle) | |
| typope | chaîne de caractères | nature de l'opération archéologique (diagnostic, fouille) | |
| nomope | chaîne de caractères | nom générique de l'opération | |
| ro | chaîne de caractères | nom, prénom du responsable d'opération | |
| annee | entier | année de début de l'opération sur le terrain | |
| surface | réel | surface en m² | |
| numoa | chaîne de caractères | numéro d'opération archéologique (OA) attribué par le SRA | |
| numprescr | chaîne de caractères | dernier numéro de prescription archéologique attribué par le SRA | |
| geometry | polygone | géométrie de l'entité | |

* **ouverture**

Il s'agit d'unité technique qui représente la ou les ouvertures réalisées pour explorer l'opération archéologique. Il s'agit de surfaces ouvertes, sous forme de polygones. Il peut s'agir de tranchées de diagnostic, de sondages, de décapage, de palier...

| Champ | Typage | Description | Contrainte |
|-----------|-----------|-----------|-----------|
| **id_ouverture** | entier | clé primaire à incrémentation automatique gérée par SpatiaLite | unique, non nul |
| numouvert | chaîne de caractères | numéro attribué par le responsable d'opération | unique, non nul |
| typouvert | chaîne de caractères | nature de l'ouverture (tranchée, sondage, etc.) | |
| surface | réel | surface en m² | |
| geometry | polygone | géométrie de l'entité | |

* **poly**

Il s'agit des anomalies de surfaces apparues à la réalisation de l'exploration archéologique. Il peut s'agir de fait, d'US ou éventuellement de mobilier si nécessaire sous forme de polygones. Ce sont les données archéologiques brutes.

| Champ | Typage | Description | Contrainte |
|-----------|-----------|-----------|-----------|
| **id_poly** | entier | clé primaire à incrémentation automatique gérée par SpatiaLite | unique, non nul |
| numpoly | chaîne de caractères | numéro attribué par le responsable d'opération | unique, non nul |
| typoly | chaîne de caractères | nature de l'anomalie de surface (fait, US, etc.) | |
| interpret | chaîne de caractères | interprétation de l'anomalie de surface (fosse, fossé, trou de poteau, puits, sol, épandage, etc.) | |
| datedebut | entier | année absolue, borne inférieure | |
| datefin | entier | année absolue, borne supérieure | |
| geometry | polygone | géométrie de l'entité | |

* **point**

Il s'agit des anomalies ponctuelles apparues à la réalisation de l'exploration archéologique. Il peut s'agir de mobilier isolé ou de lot de mobiliers sous forme de points. Ce sont les données archéologiques brutes.

| Champ | Typage | Description | Contrainte |
|-----------|-----------|-----------|-----------|
| **id_point** | entier | clé primaire à incrémentation automatique gérée par SpatiaLite | unique, non nul |
| numpoint | chaîne de caractères | numéro attribué par le responsable d'opération | unique, non nul |
| typoint | chaîne de caractères | nature de l'anomalie ponctuelle (mobilier isolé, lot de mobilier, etc.) | |
| interpret | chaîne de caractères | interprétation de l'anomalie de surface (fosse, fossé, trou de poteau, puits, sol, épandage, etc.) | |
| datedebut | entier | année absolue, borne inférieure | |
| datefin | entier | année absolue, borne supérieure | |
| geometry | point | géométrie de l'entité | |

* **log**

Il s'agit des observations ponctuelles : log stratigraphique, prélèvement pour analyse. Ces données sont représentées sous forme de points.

| Champ | Typage | Description | Contrainte |
|-----------|-----------|-----------|-----------|
| **id_log** | entier | clé primaire à incrémentation automatique gérée par SpatiaLite | unique, non nul |
| numlog | chaîne de caractères | numéro attribué par le responsable d'opération | unique, non nul |
| typlog | chaîne de caractères | nature de l'observation ponctuelle (log stratigraphique, prélèvement, etc.) | |
| alti | réel | altitude de l'observation ponctuelle, exprimée en mètre selon le référentiel altimétrique choisi (NGF - IGN69 par exemple) | |
| typalti | chaîne de caractères | position de la mesure d'altitude (sommet, base, etc.) | |
| geometry | point | géométrie de l'entité | |

* **axe**

Il s’agit d’une couche de ligne qui représente une lecture des observations faites sur le terrain : axe de relevé, profil, transect, etc.

> :exclamation: ATTENTION !
> Les axes doivent être enregistrés avec un certain nombre de contraintes géométriques :
> Un axe est défini par un ligne constituée de deux points dont l'altitude est sensiblement la même. L'axe est en effet horizontal. Un changement de direction, ou un changement d'altitude entraîne un changement d'axe et donc la création d'une nouvelle entité.. 
> Le premier point constitue l'origine de l'axe, c'est à dire l'origine du relevé de la coupe qui définit le sens de lecture de cette dernière.>

| Champ | Typage | Description | Contrainte |
|-----------|-----------|-----------|-----------|
| **id_axe** | entier | clé primaire à incrémentation automatique gérée par SpatiaLite | unique, non nul |
| numaxe | chaîne de caractères | numéro attribué par le responsable d'opération | unique |
| typaxe | chaîne de caractères | type d'axe : relevé, transect, profil, etc. | |
| longu_axe | réel | longueur de l'axe exprimée en mètre | |
| z_axe | réel | altitude de l'axe, exprimée en mètre selon le référentiel altimétrique choisi (NGF - IGN69 par exemple) | |
| geometry | polyligne | géométrie de l'entité | |

* **pts_topo**

Il s'agit d'une septième couche ajoutée pour l'enregistrement de l'ensemble des points topographiques de l'opération.

| Champ | Typage | Description | Contrainte |
|-----------|-----------|-----------|-----------|
| **id_ptopo** | entier | clé primaire à incrémentation automatique gérée par SpatiaLite | unique, non nul |
| m | chaîne de caractères | matricule attribué par le topographe | |
| x | réel | coordonnée x du point | |
| y | réel | coordonnée y du point | |
| z | réel | coordonnée z du point, exprimée en mètre selon le référentiel altimétrique choisi (NGF - IGN69 par exemple) | |
| code | chaîne de caractères | code attribué par le topographe | |
| date_leve | entier | date du levé topographique exprimée en mode anglo-saxon. Ex : 230303 pour 3 mars 2023 | |
| note | chaîne de caractères | notes diverses | |
| geometry | point | géométrie de l'entité | |

##### Tables filles

> **j_rel_photogram** : table de jonction entre les modèles de photogrammétries, les photos et les points topo utilisés - *relation de n à n* 

---

## Les requêtes intégrées à Badass

Le langage SQL permet l'écriture de requêtes, appelées également vues, qui permettent de visualiser des entités selon des critères de calculs et/ou de sélections. Non éditable, elles présentent néanmoins l'avantage de se mettre à jour au fur et à mesure de la saisie dans les tables qui sont appelées dans les requêtes.

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| vue_recap_tranchee              | Vue de récapitulation des logs, faits et US enregistrés par tranchée. La clé primaire correspond au numéro de tranchée auquel est associé la liste des numéros des entités séparées par une virgule                                                                                                                                                                                          | null      |

### SQL

```sql
CREATE VIEW vue_recap_tranchee AS
SELECT "numtr",
GROUP_CONCAT("numlog",', ') OVER (ORDER BY "numlog") AS recap_log, -- OVER (ORDER BY "numlog") permet d'ordonner la liste (ASC par défaut) selon le champ indiqué
GROUP_CONCAT("numfait",', ') OVER (ORDER BY "numfait") AS recap_fait,
GROUP_CONCAT("numus",', ') OVER (ORDER BY "numus") AS recap_us
FROM j_rel_tranchee 
GROUP BY "numtr"
ORDER BY "numtr"
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| vue_recap_us_fait               | Vue de récapitulation des US enregistrés par fait : la clé primaire correspond au numéro de fait auquel est associé la liste des numéros d'US séparés par une virgule                                                                                                                                                                                                                        | null      |

### SQL

```sql
CREATE VIEW vue_recap_us_fait AS
SELECT "numfait" as numfait,
GROUP_CONCAT("numus",', ') OVER (ORDER BY "numus") as recap_us
FROM t_us
GROUP BY "numfait"
ORDER BY "numus"
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| vue_recap_relationus            | Vue de récapitulation des relations stratigraphiques : la clé primaire correspond au numéro d'US auquel sont associées la liste des US antérieures, la liste des US postérieures, et la liste des US en relation horizontales séparées par des virgules                                                                                                                                      | null      |

### SQL

```sql
CREATE VIEW vue_recap_relationus AS
SELECT "numus1" as numus,
GROUP_CONCAT(CASE WHEN "typrel" LIKE 'sous' THEN "numus2" ELSE null END, ', ') OVER (ORDER BY "numus") AS us_posterieur,
GROUP_CONCAT(CASE WHEN "typrel" LIKE 'sur' THEN "numus2" ELSE null END, ', ') OVER (ORDER BY "numus") AS us_anterieur,
GROUP_CONCAT(CASE WHEN "typrel" IN('égale','équivalente','synchrone') THEN "numus2" ELSE null END, ', ') OVER (ORDER BY "numus") AS rel_horizontal
FROM j_rel_us
GROUP BY numus1
ORDER BY numus1
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| vue_j_rel_us_inverse            | Vue d'inversion des relations stratigraphiques à partir de la table j_rel_us                                                                                                                                                                                                                                                                                                                 | null      |

### SQL

```sql
CREATE VIEW vue_j_rel_us_inverse AS
SELECT "id_rel_us", "numus2" as numus1,
CASE WHEN "typrel" LIKE 'sur' THEN 'sous'
WHEN "typrel" LIKE 'sous' THEN 'sur' end as typrel,
"numus1" as numus2, incert
FROM j_rel_us
WHERE "typrel" IN ('sur','sous')
```
---


| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| vue_recap_relationus_deduite    | Vue de récapitulation des relations stratigraphiques inversées par US                                                                                                                                                                                                                                                                                                                        | null      |

### SQL

```sql
CREATE VIEW vue_recap_relationus_deduite AS
SELECT "numus1" as numus,
GROUP_CONCAT(CASE WHEN "typrel" LIKE 'sous' THEN "numus2" ELSE null END, ', ') OVER (ORDER BY "numus") AS us_posterieur,
GROUP_CONCAT(CASE WHEN "typrel" LIKE 'sur' THEN "numus2" ELSE null END, ', ') OVER (ORDER BY "numus") AS us_anterieur,
GROUP_CONCAT(CASE WHEN "typrel" IN('égale','équivalente','synchrone') THEN "numus2" ELSE null END, ', ') OVER (ORDER BY "numus") AS rel_horizontal
FROM vue_j_rel_us_inverse
GROUP BY numus1
ORDER BY numus1
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| vue_recap_us_log                | Vue de récapitulation des US par log stratigraphique                                                                                                                                                                                                                                                                                                                                         | null      |

### SQL

```sql
CREATE VIEW "vue_recap_us_log" AS 
SELECT "numlog", 
GROUP_CONCAT("numus",', ') OVER (ORDER BY "numus") as recap_us,
MAX("prof_base") AS prof_max,
MIN("z_min") AS z_min 
FROM j_us_log 
GROUP BY "numlog" 
ORDER BY "numlog"
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| vue_recap_photo                 | Vue de récapitulation des tranchées, sondages, faits, us et iso par photo                                                                                                                                                                                                                                                                                                                    | null      |

### SQL

```sql
CREATE VIEW vue_recap_photo AS
select p.*, m1."numphoto", 
group_concat(m1."numtr", ', ') OVER (ORDER BY "numtr") as recap_tr, 
group_concat(m1."numsd", ', ') OVER (ORDER BY "numsd") as recap_sd,
group_concat(m1."numlog", ', ') OVER (ORDER BY "numlog") as recap_log,
group_concat(m1."numfait", ', ') OVER (ORDER BY "numfait") as recap_fait, 
group_concat(m1."numus", ', ') OVER (ORDER BY "numus") as recap_us, 
group_concat(m1."numiso", ', ') OVER (ORDER BY "numiso") as recap_iso
from j_rel_photo as m1
join t_photo as p on m1."numphoto" = p."id_photo"
group by m1."numphoto"
order by p."id_photo"
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| vue_recap_minute                 | Vue de récapitulation des tranchées, sondages, faits, us et iso par minute                                                                                                                                                                                                                                                                                                                    | null      |

### SQL

```sql
CREATE VIEW vue_recap_minute AS
select m.*, m1."numinute", 
group_concat(m1."numtr", ', ') OVER (ORDER BY "numtr") as recap_tr, 
group_concat(m1."numsd", ', ') OVER (ORDER BY "numsd") as recap_sd, 
group_concat(m1."numlog", ', ') OVER (ORDER BY "numlog") as recap_log,
group_concat(m1."numfait", ', ') OVER (ORDER BY "numfait") as recap_fait, 
group_concat(m1."numus", ', ') OVER (ORDER BY "numus") as recap_us, 
group_concat(m1."numiso", ', ') OVER (ORDER BY "numiso") as recap_iso
from j_rel_minute as m1
join t_minute as m on m1."numinute" = m."numinute"
group by m1."numinute"
order by m1."numinute"
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| ExportUS                        | Vue d'exportation de la table t_us pour le Stratifiant (**Desachy 2008**). Manque quelques champs : FPAestime (date estimée au plus ancien de fin de formation de l'US) ; FPRestime (date estimée au plus récent de fin de formation de l'US) ; REF_PhaseDebut (n° d'ordre de la phase au plus ancien attribué à l'US) ; REF_PhaseFin (n° d'ordre de la phase au plus récent attribué à l'US | null      |

### SQL

```sql
CREATE VIEW ExportUS AS
SELECT "numus" AS ID_US, REPLACE(REPLACE( "type_us",'couche physique','couche'),'négative','négatif') AS Type_US, "datinf_interpret" AS FPA, "datsup_interpret" AS FPR
FROM t_us
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| ExportRelations                 | Vue d'exportation de la table de relations stratigraphiques pour les relations d'antériorité/postériorié. Manque le champ RelationIncertaine rempli par NULL ou '?'                                                                                                                                                                                                                          | null      |

### SQL

```sql
CREATE VIEW ExportRelations AS
SELECT DISTINCT CASE WHEN "typrel" LIKE 'sur' THEN "numus2" WHEN "typrel" LIKE 'sous' THEN "numus1" END AS REF_USanterieure, CASE WHEN "typrel" LIKE 'sur' THEN "numus1" WHEN "typrel" LIKE 'sous' THEN "numus2" END AS REF_USposterieure
FROM j_rel_us
WHERE "typrel" IN ('sur', 'sous')
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| ExportSynchros                  | Vue d'exportation de la table de relations stratigraphiques pour les relations horizontales. Manque le champ RelationIncertaine rempli par NULL ou '?'                                                                                                                                                                                                                                       | null      |

### SQL

```sql
CREATE VIEW ExportSynchros AS
SELECT DISTINCT CASE WHEN "typrel" IN ('égal','équivalent','égale','équivalente','synchrone') THEN "numus1" END AS REF_USsynchro1, CASE WHEN "typrel" IN ('égal','équivalent','égale','équivalente','synchrone') THEN "numus2" END AS REF_USsynchro2
FROM j_rel_us
WHERE "typrel" IN ('égal','équivalent','égale','équivalente','synchrone')
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| erreur_saisie_strati            | Vue de détection des erreurs d'écritures de relations stratigraphiques : sur une même US ou relations contradictoires                                                                                                                                                                                                                                                                        | null      |

### SQL

```sql
CREATE VIEW erreur_saisie_strati AS
SELECT *, CASE WHEN r."REF_USanterieure" = r."REF_USposterieure"  THEN 'relation verticale sur la même US' WHEN s."REF_USsynchro1" = s."REF_USsynchro2" THEN 'relation horizontale sur la même US' WHEN r."REF_USanterieure"||r."REF_USposterieure" = r."REF_USposterieure"||r."REF_USanterieure" THEN 'relations stratigraphiques contradictoires' END AS nature_erreur
FROM ExportRelations AS r, ExportSynchros AS s
WHERE r."REF_USanterieure" = r."REF_USposterieure"
OR r."REF_USanterieure"||r."REF_USposterieure" = r."REF_USposterieure"||r."REF_USanterieure"
OR s."REF_USsynchro1" = s."REF_USsynchro2"
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| recap_us_seq                    | Vue recapitulative des US par séquence                                                                                                                                                                                                                                                                                                                                                       | null      |

### SQL

```sql
CREATE VIEW recap_us_seq AS
SELECT "num_seq", GROUP_CONCAT("numus",', ') OVER (ORDER BY "numus") as recap_us 
FROM t_us 
GROUP BY "num_seq" 
ORDER BY "numus"
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| recap_pha_seq                   | Vue recapitulative des séquences par phase                                                                                                                                                                                                                                                                                                                                                   | null      |

### SQL

```sql
CREATE VIEW recap_pha_seq AS
SELECT "numphase" as numphase, GROUP_CONCAT("numseq",', ') OVER (ORDER BY "numseq") as recap_seq
FROM j_seq_phase
GROUP BY "numphase" 
ORDER BY "numseq"
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| recap_per_pha                   | Vue recapitulative des phases par période                                                                                                                                                                                                                                                                                                                                                    | null      |

### SQL

```sql
CREATE VIEW recap_per_pha AS
SELECT "numperiode" as periode, GROUP_CONCAT("numphase",', ') OVER (ORDER BY "numphase") as recap_phases
FROM j_phase_per as p
GROUP BY "numphase" 
ORDER BY "numperiod"
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| inventaire_us                   | Vue d'inventaire d'US                                                                                                                                                                                                                                                                                                                                                                        | null      |

### SQL

```sql
CREATE VIEW inventaire_us AS
SELECT t."numtr",u."numus",u."numfait",u."type_us",u."nature_us",u."interpret",u."description",u."forme",u."diam",u."dim_max",u."dim_min",u."epais",u."prof_app",
u."datinf_mobilier",u."datsup_mobilier",
CASE WHEN v."us_anterieur" IS NULL AND d."us_anterieur" IS NULL THEN '' 
WHEN v."us_anterieur" IS NULL THEN d."us_anterieur"
WHEN d."us_anterieur" IS NULL THEN v."us_anterieur"
WHEN d."us_anterieur"=v."us_anterieur" THEN d."us_anterieur"
ELSE v."us_anterieur"||', '||d."us_anterieur" END 
AS sur, 
CASE WHEN v."us_posterieur" IS NULL AND d."us_posterieur" IS NULL THEN '' 
WHEN v."us_posterieur" IS NULL THEN d."us_posterieur"
WHEN d."us_posterieur" IS NULL THEN v."us_posterieur"
WHEN d."us_posterieur"=v."us_posterieur" THEN d."us_posterieur"
ELSE v."us_posterieur"||', '||d."us_posterieur" END 
AS sous, 
CASE WHEN v."rel_horizontal" IS NULL AND d."rel_horizontal" IS NULL THEN '' 
WHEN v."rel_horizontal" IS NULL THEN d."rel_horizontal"
WHEN d."rel_horizontal" IS NULL THEN v."rel_horizontal"
WHEN d."rel_horizontal"=v."rel_horizontal" THEN d."rel_horizontal"
ELSE v."rel_horizontal"||', '||d."rel_horizontal" END 
AS synch, 
u."note",
u."num_seq" as numseq, ph."numphase", pe."numperiod"
FROM t_us AS u
LEFT JOIN j_rel_tranchee as t
ON u."numus"=t."numus"
LEFT JOIN vue_recap_relationus as v
ON u."numus"=v."numus"
LEFT JOIN vue_recap_relationus_deduite AS d
ON u."numus" =d."numus"
JOIN j_seq_phase AS ph ON ph."numseq" = u."num_seq"
JOIN j_phase_per AS pe ON ph."numphase" = pe."numphase" 
JOIN j_rel_us AS ur ON ur."numus1" = u."numus"
GROUP BY u."numus"
ORDER BY u."numus"
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| vue_intersects_fait             | Vue de relations spatiales entre la table des faits et les tables t_tranchee, t_sondage et t_axe. Peut servir pour générer des atlas plan/coupe                                                                                                                                                                                                                                              | null      |

### SQL

```sql
CREATE VIEW "vue_intersects_fait" AS
select f."numfait", t."numtr", s."numsd", a."id_axe", a."numaxe", "x_min_coupe", "y_min_coupe", "x_max_coupe", "y_max_coupe"
from t_fait as f
join t_tranchee as t on st_intersects(f.geometry, t.geometry)
join t_sondage as s on st_intersects(f.geometry, s.geometry)
join t_axe as a on st_intersects(f.geometry, a.geometry)
join (select "id_coupe", x(startpoint(geometry)) as x_min_coupe, y(startpoint(geometry)) as y_min_coupe, x(endpoint(geometry)) as x_max_coupe, y(endpoint(geometry)) as y_max_coupe from coupe_axe) as c
on c."id_coupe" = a."id_axe"
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| vue_intersects_us               | Vue de relations spatiales entre la table des US et les tranchées, sondages et axe                                                                                                                                                                                                                                                                                                           | null      |

### SQL

```sql
CREATE VIEW "vue_intersects_us" AS
SELECT u."numus", u."numfait", t."numtr", s."numsd", a."id_axe", a."numaxe"
from t_us as u
join t_fait as f on f."numfait" = u."numfait"
join t_tranchee as t on case when u.geometry is null then st_intersects(f.geometry, t.geometry) else st_intersects(u.geometry, t.geometry) end
join t_sondage as s on case when u.geometry is null then st_intersects(f.geometry, s.geometry) else st_intersects(u.geometry, s.geometry) end
join t_axe as a on case when u.geometry is null then st_intersects(f.geometry, a.geometry) else st_intersects(u.geometry, a.geometry) end
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| vue_numaximum_serie             | Vue d'aggrégation des valeurs maximales des numéros d'entité par séries définies dans la table serie                                                                                                                                                                                                                                                                                         | null      |

### SQL

```sql
CREATE VIEW "vue_numaximum_serie" AS
select "id_serie", 't_us' as "tablentite", max("numus") as numaximum
from t_us
join serie on "numus" >= "nombre_depart" and "numus" <= "nombre_arrivee" and "tablentite" like 't_us'
group by "id_serie"
union
select "id_serie", 't_fait' as "tablentite", max("numfait") as numaximum
from t_fait
join serie on "numfait" >= "nombre_depart" and "numfait" <= "nombre_arrivee" and "tablentite" like 't_fait'
group by "id_serie"
union
select "id_serie", 't_axe' as "tablentite", max("numaxe") as numaximum
from t_axe
join serie on "numaxe" >= "nombre_depart" and "numaxe" <= "nombre_arrivee" and "tablentite" like 't_axe'
group by "id_serie"
union
select "id_serie", 't_ens' as "tablentite", max("numens") as numaximum
from t_ens
join serie on "numens" >= "nombre_depart" and "numens" <= "nombre_arrivee" and "tablentite" like 't_ens'
group by "id_serie"
union
select "id_serie", 't_log' as "tablentite", max("numlog") as numaximum
from t_log
join serie on "numlog" >= "nombre_depart" and "numlog" <= "nombre_arrivee" and "tablentite" like 't_log'
group by "id_serie"
union
select "id_serie", 't_mobilier' as "tablentite", max("numiso") as numaximum
from t_mobilier
join serie on "numiso" >= "nombre_depart" and "numiso" <= "nombre_arrivee" and "tablentite" like 't_mobilier'
group by "id_serie"
union
select "id_serie", 't_sondage' as "tablentite", max("numsd") as numaximum
from t_sondage
join serie on "numsd" >= "nombre_depart" and "numsd" <= "nombre_arrivee" and "tablentite" like 't_sondage'
group by "id_serie"
union
select "id_serie", 't_tranchee' as "tablentite", max("numtr") as numaximum
from t_tranchee
join serie on "numtr" >= "nombre_depart" and "numtr" <= "nombre_arrivee" and "tablentite" like 't_tranchee'
group by "id_serie"
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| vue_entite_rapide_serie         | Vue de jointure entre la table entite_rapide et serie pour récupérer les infos de la table serie. Cette table sert de support aux triggers liés aux créations rapide                                                                                                                                                                                                                         | null      |

### SQL

```sql
CREATE VIEW "vue_entite_rapide_serie" AS
SELECT e."id_entite",e."id_serie",e."numentite",e."interpret", s."nombre_depart",s."nombre_arrivee",s."pas",s."tablentite", 
CASE WHEN v."new_numentite" IS NULL THEN s."nombre_depart" ELSE v."new_numentite" END AS "new_numentite" -- permet de générer le nouveau numéro à attribuer à l'enregistrement. Si aucune entité n'est déjà présente dans les tables cibles, alors le premier numéro de la série sera attribué
FROM serie AS s
LEFT JOIN entite_rapide AS e ON e."id_serie" = s."id_serie" --LEFT JOIN pour peupler la vue dès que les séries sont crées
LEFT JOIN vue_numaximum_serie AS v ON e."id_serie" = v."id_serie"
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| vue_log_stratigraphique         | Vue de génération de géométrie pour la visualisation des logs stratigraphiques                                                                                                                                                                                                                                                                                                               | polygone  |

### SQL

```sql
CREATE VIEW "vue_log_stratigraphique" AS
SELECT t.*,l.numtr,l.zmin_log, CASE WHEN "zmin_uslog" is null THEN BuildMbr(t."numlog"-1, l."zmin_log", t."numlog"-0.75, "zmax_uslog", 2154) ELSE BuildMbr(t."numlog"-1, "zmin_uslog", t."numlog"-0.75, "zmax_uslog", 2154) END AS geometry 
FROM j_us_log AS t JOIN t_log AS l 
ON t."numlog" = l."numlog"
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| caviar_prescription             | Pour Caviar table prescription                                                                                                                                                                                                                                                                                                                                                               | polygone  |

### SQL

```sql
CREATE VIEW caviar_prescription AS
   SELECT 
   "gid_caviar" as gid,
   "typemp",
   "numoa",
   "tranche",
   "geometry"
   FROM emprise
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| caviar_ouverture                | Pour Caviar table ouverture                                                                                                                                                                                                                                                                                                                                                                  | polygone  |

### SQL

```sql
CREATE VIEW caviar_ouverture AS
   SELECT 
   e."gid_caviar" as gidoperef,
   o."numouvert",
   o."typouvert",
   o."geometry",
   o."id_ouverture"
   FROM emprise AS e, ouverture AS o
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| caviar_uniteobservation         | Pour Caviar table uniteobservation                                                                                                                                                                                                                                                                                                                                                           | polygone  |

### SQL

```sql
CREATE VIEW caviar_uniteobservation AS
   SELECT 
   e."gid_caviar" as gidoperef,
   p."numpoly" AS "numunobs",
   p."typoly" AS "typunobs",
   p."interpret",
   p."datedebut",
   p."datefin",
   p."geometry",
   p."id_poly"
   FROM emprise AS e, poly AS p
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| caviar_uniteobservation_point   | Pour Caviar table uniteobservation point                                                                                                                                                                                                                                                                                                                                                     | point     |

### SQL

```sql
CREATE VIEW caviar_uniteobservation_point AS
   SELECT 
   e."gid_caviar" as gidoperef,
   p."numpoint" AS "numunobs",
   p."typoint" AS "typunobs",
   p."interpret",
   p."datedebut",
   p."datefin",
   p."geometry",
   p."id_point"
   FROM emprise AS e, point AS p
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| vue_axe_coupe_translate         | Vue de translation des coupes sur le plan pour la couche des axes                                                                                                                                                                                                                                                                                                                            | polyligne |

### SQL

```sql
CREATE VIEW vue_axe_coupe_translate AS 
SELECT c."id_axe",a."numaxe",c."numinute",c."azimuth",c."alti", 
st_translate(c."geometry", x(startpoint(a."geometry"))-x(startpoint(c."geometry")),y(startpoint(a."geometry"))-y(startpoint(c."geometry")),0) AS geometry --requête de translation des coordonnées en coupe vers les coordonnées en plan
FROM coupe_axe AS c 
JOIN t_axe AS a ON a."id_axe" = c."id_axe"
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| vue_line_coupe_translate        | Vue de translation des coupes sur le plan pour la couche des lignes                                                                                                                                                                                                                                                                                                                          | polyligne |

### SQL

```sql
CREATE VIEW vue_line_coupe_translate AS 
SELECT l."id_cpline",l."id_axe",a."numaxe","numfait","numus","typline","numsd", 
st_translate(l."geometry", x(startpoint(a."geometry"))-x(startpoint(c."geometry")),y(startpoint(a."geometry"))-y(startpoint(c."geometry")),0) AS geometry --requête de translation des coordonnées en coupe vers les coordonnées en plan
FROM coupe_line AS l 
JOIN coupe_axe AS c ON c."id_axe" = l."id_axe" 
JOIN t_axe AS a ON a."id_axe" = l."id_axe"
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| vue_poly_coupe_translate        | Vue de translation des coupes sur le plan pour la couche des polygones                                                                                                                                                                                                                                                                                                                       | polygone  |

### SQL

```sql
CREATE VIEW vue_poly_coupe_translate AS 
SELECT p."id_cpoly",p."id_axe",a."numaxe","numfait","numus","typoly","detail","numsd", 
st_translate(p."geometry", x(startpoint(a."geometry"))-x(startpoint(c."geometry")),y(startpoint(a."geometry"))-y(startpoint(c."geometry")),0) as geometry --requête de translation des coordonnées en coupe vers les coordonnées en plan
FROM coupe_poly AS p 
JOIN coupe_axe AS c ON c."id_axe" = p."id_axe" 
JOIN t_axe AS a ON a."id_axe" = c."id_axe"
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| vue_line_coupe_translate_rotate | Vue de translation et rotation des coupes sur le plan pour la couche des lignes                                                                                                                                                                                                                                                                                                              | polyligne |

### SQL

```sql
CREATE VIEW vue_line_coupe_translate_rotate AS 
SELECT l."id_cpline",l."id_axe",a."numaxe","numfait","numus","typline","numsd", 
shiftcoords( rotatecoords( shiftcoords(l.geometry,-x(startpoint(a.geometry)),-y(startpoint(a.geometry))) ,c.azimuth-90) ,x(startpoint(a.geometry)),y(startpoint(a.geometry))) AS geometry --requête de translation des coordonnées en coupe vers les coordonnées en plan
FROM vue_line_coupe_translate AS l 
JOIN coupe_axe AS c ON c."id_axe" = l."id_axe" 
JOIN t_axe AS a ON a."id_axe" = l."id_axe"
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| vue_poly_coupe_translate_rotate | Vue de translation et rotation des coupes sur le plan pour la couche des polygones                                                                                                                                                                                                                                                                                                           | polygone  |

### SQL

```sql
CREATE VIEW vue_poly_coupe_translate_rotate AS 
SELECT p."id_cpoly",p."id_axe",a."numaxe","numfait","numus","typoly","detail","numsd", 
shiftcoords(rotatecoords( shiftcoords(p.geometry,-x(startpoint(a.geometry)),-y(startpoint(a.geometry))) ,c.azimuth-90) ,x(startpoint(a.geometry)),y(startpoint(a.geometry))) as geometry --requête de translation des coordonnées en coupe vers les coordonnées en plan
FROM vue_poly_coupe_translate AS p 
JOIN coupe_axe AS c ON c."id_axe" = p."id_axe" 
JOIN t_axe AS a ON a."id_axe" = c."id_axe"
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| vue_cumulcoupe_us_negative      | Vue de translation des US négatives en coupe sur un axe médian                                                                                                                                                                                                                                                                                                                               | polyligne |

### SQL

```sql
CREATE VIEW vue_cumulcoupe_us_negative AS 
SELECT l."id_cpline",l."id_axe", "numaxe", "numus", "numfait", "numsd", 
shiftcoords(l."geometry", -x(st_centroid(l."geometry"))-10 ,0) AS geometry --requête de translation vers un axe médian à -10 m en X et en y = z
FROM coupe_line AS l 
JOIN coupe_axe AS a ON a.id_axe = l.id_axe 
WHERE "typline" LIKE 'US négative'
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| erreur_geometry                 | Vue de pointage des erreurs de géométrie                                                                                                                                                                                                                                                                                                                                                     | point     |

### SQL

```sql
CREATE VIEW erreur_geometry AS
SELECT rowid, 'coupe_line' as "table", ST_ISVALIDREASON(geometry) as erreur_geometrie, ST_ISVALIDDETAIL(geometry) as geometry
FROM coupe_line
WHERE ST_ISVALID(geometry) = 0 
UNION
SELECT rowid, 'coupe_poly' as "table", ST_ISVALIDREASON(geometry) as erreur_geometrie, ST_ISVALIDDETAIL(geometry) as geometry
FROM coupe_poly
UNION
SELECT rowid, 'emprise' as "table", ST_ISVALIDREASON(geometry) as erreur_geometrie, ST_ISVALIDDETAIL(geometry) as geometry
FROM emprise
WHERE ST_ISVALID(geometry) = 0
UNION
SELECT rowid, 'ouverture' as "table", ST_ISVALIDREASON(geometry) as erreur_geometrie, ST_ISVALIDDETAIL(geometry) as geometry
FROM ouverture
WHERE ST_ISVALID(geometry) = 0
UNION
SELECT rowid, 'poly' as "table", ST_ISVALIDREASON(geometry) as erreur_geometrie, ST_ISVALIDDETAIL(geometry) as geometry
FROM poly
WHERE ST_ISVALID(geometry) = 0
UNION
SELECT rowid, 't_ens' as "table", ST_ISVALIDREASON(geometry) as erreur_geometrie, ST_ISVALIDDETAIL(geometry) as geometry
FROM t_ens
WHERE ST_ISVALID(geometry) = 0
UNION
SELECT rowid, 't_fait' as "table", ST_ISVALIDREASON(geometry) as erreur_geometrie, ST_ISVALIDDETAIL(geometry) as geometry
FROM t_fait
WHERE ST_ISVALID(geometry) = 0
UNION
SELECT rowid, 't_orthophotogram' as "table", ST_ISVALIDREASON(geometry) as erreur_geometrie, ST_ISVALIDDETAIL(geometry) as geometry
FROM t_orthophotogram
WHERE ST_ISVALID(geometry) = 0
UNION
SELECT rowid, 't_photogram' as "table", ST_ISVALIDREASON(geometry) as erreur_geometrie, ST_ISVALIDDETAIL(geometry) as geometry
FROM t_photogram
WHERE ST_ISVALID(geometry) = 0
UNION
SELECT rowid, 't_sondage' as "table", ST_ISVALIDREASON(geometry) as erreur_geometrie, ST_ISVALIDDETAIL(geometry) as geometry
FROM t_sondage
WHERE ST_ISVALID(geometry) = 0
UNION
SELECT rowid, 't_tranchee' as "table", ST_ISVALIDREASON(geometry) as erreur_geometrie, ST_ISVALIDDETAIL(geometry) as geometry
FROM t_tranchee
WHERE ST_ISVALID(geometry) = 0
UNION
SELECT rowid, 't_us' as "table", ST_ISVALIDREASON(geometry) as erreur_geometrie, ST_ISVALIDDETAIL(geometry) as geometry
FROM t_us
WHERE ST_ISVALID(geometry) = 0
```

---

| Nom de la vue                   | Description                                                                                                                                                                                                                                                                                                                                                                                  | geometry  |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| vue_coupe_axe    | Vue de projection des axes en plan en coupe. Attention ! Cette vue ne doit pas servir de support pour le dessin des coupes, son contenu, une fois consolider, doit intégrer la couche coupe_axe                                                                                                                                                                                                                                                                                                                        | polyline      |

### SQL

```sql
CREATE VIEW vue_coupe_axe AS 
SELECT "id_axe", "numaxe",
degrees(st_azimuth(st_startpoint(geometry),st_endpoint(geometry))) as azimuth,
st_length(geometry) as long_axe,
"alti",
"note",
makeline(makepoint((SUM(st_length(geometry)) OVER(ORDER BY "id_axe" ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))-st_length(geometry)+"id_axe"* 5 - 5 , "alti"),
makepoint((SUM(st_length(geometry)) OVER(ORDER BY "id_axe" ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+"id_axe"* 5 - 5 ,"alti")) as geometry
FROM t_axe
```

---


A noter que pour les vues qui comportent une colonne de géométrie, il est nécessaire d'écrire dans la table ***view_geometry_columns*** généré par SpatiaLite. Cette table est structurée ainsi :

| Champ | Typage | Description | Contrainte |
|-----------|-----------|-----------|-----------|
| **rowid** | entier | clé primaire à incrémentation automatique gérée par SpatiaLite | unique, non nul |
| view_name | chaîne de caractères | le nom de la vue qui comporte une géométrie | |
| view_geometry | chaîne de caractères | le nom du champ dans la vue qui contient la géométrie | |
| view_rowid | chaîne de caractères | le nom du champ qui contient la clé primaire (obligatoire). Si la vue ne comporte pas de clé primaire, il est possible d'en créer une dans la vue directement avec la synthaxe *"rowid"* | |
| f_table_name | chaîne de caractères | le nom de la table de référence pour la géométrie (*cf. infra*) | |
| f_geometry_column | chaîne de caractères | le nom du champ dans la table de référence qui contient la géométrie (*cf. infra*) | |

### Les tables de références pour les vues géométriques

SpatiaLite a besoin de table de référence pour la création de vues géométriques. L'astuce, pour Badass, est d'ajouter dans les tables natives, trois tables vides pour chacune des géométries possibles. Ces tables ne comportent que deux champs : un champ **"id"** qui correspond à la clé primaire de la table et un champ **"geometry"** avec un SCR défini (EPSG : 2154).

* ***ghost_point_2154*** : table de référence pour les vues géométriques avec une géométrie de type point
* ***ghost_polyline_2154*** : table de référence pour les vues géométriques avec une géométrie de type polyline
* ***ghost_polygon_2154*** : table de référence pour les vues géométriques avec une géométrie de type polygone

Une ligne de commande est nécessaire en plus du code SQL de création de vues pour l'insertion de l'enregistrement dans la table ***view_geometry_columns***.

Par exemple pour enregistrer la géométrie de la vue ***erreur_geometry***, on pourra écrire :

```sql
INSERT INTO views_geometry_columns
    (view_name, view_geometry, view_rowid, f_table_name, f_geometry_column, read_only)
  VALUES ('erreur_geometry', 'geometry', 'rowid', 'ghost_point_2154', 'geometry','1')

```  
---

## Badass et SpatiaLite

Pour rappel, la base de données Badass, a été totalement rédigée dans le langage SQL qui permet à la fois de créer les tables vides, mais également les tables pleines (le thésaurus déjà rédigé par exemple), les *triggers* ainsi que les quelques requêtes (ou vues) décrites plus haut. Ce langage peut être lu par de nombreux systèmes de gestions de bases de données moyennant quelques modifications à la marge. Parmi les plus connus, et les logiciels libres : 

* SQLite avec son extension spatiale SpatiaLite et son cousin Geopackage (très plébiscité par QGIS) qui permettent tous deux le stockage de l'ensemble des données dans un seul fichier (extension .sqlite pour le premier, .gpkg pour le second)
* PostgreSQL avec son extension spatiale PostGIS qui fonctionne sur serveur et permet à plusieurs utilisateurs connectés sur ce serveur d'accéder aux données et de les modifier

Nous avons, pour le moment privilégié le format SpatiaLite, mais le passage vers une version serveur est possible.
L'utilisation de ***spatialite_gui.exe*** permet de générer la base de données SpatiaLite vide à partir du code SQL. Pour information, le suffixe *_gui*, signifie Graphical User Interface, c'est-à-dire interface utilisateur graphique. Nous avons également constaté une stabilité plus importante de la base générée via spatialite_gui.exe par rapport à une création de base depuis QGIS.
Cependant, Alex Baiet, ingénieur en informatique, a développé en 2022 une extension à QGIS qui permet de générer la base ainsi que le projet QGIS associé (*cf. infra*).

---

## Badass et QGIS

QGIS permet une connexion en direct aux données enregistrées dans le format SpatiaLite. 
En 2019, deux ateliers Archéomatiques – Ateliers organisés par le Réseau ISA - coordonnés par Sylvain Badey (Inrap – UMR 7324 CITERES-LAT) et Amélie Laurent (CD45 – UMR 7324 CITERES-LAT) –, ce sont déroulés pour former à l'utilisation conjointe de SpatiaLite et de QGIS. Depuis 2020, le réseau des référents SIG propose cette formation aux agents Inrap ayant suivi les deux premiers niveaux de formations à QGIS.
Cette formation a été conçue par Thomas Guillemard et se déroule sur deux jours.
Le support de cette formation est accessible ici : [Support de formation de la formation 3.1](https://formationsig.gitlab.io/sig31/)

La navigation sur le site web se fait soit en passant par la table des matières à gauche, soit en passant par la flèche grise à droite de la fenêtre :
![Support de formation SIG 3.1 - Navigation][009]

Il est fortement conseillé aux futurs utilisateurs de Badass de suivre cette formation afin de bénéficier des connaissances pour la manipulation de la base dans QGIS et pour la personnalisation de la structure et des interfaces.

Dans la mesure du possible, Badass peut-être utilisée avec les outils natifs de QGIS. Ce choix permet d'éviter une dépendance aux programmateurs informaticiens, et évite de changer des habitudes prises sur le logiciel par nos collègues de plus en plus utilisateur régulier de QGIS. Ainsi sont mis en place dans le projet QGIS associé à Badass :

* les relations entre les tables
* des formulaires avec des aides à la saisie :
    * listes déroulantes
    * cases à cocher
    * onglets
    * etc.
* une exploitation des styles sur les couches pour proposer des formulaires de saisie plus ou moins complets/simplifiés selon le contexte
* une exploitation des thèmes pour contrôler l'affichage coordonné de plusieurs couches

Le projet QGIS est en cours de perfectionnement et s'enrichit à chaque utilisations de nouvelles fonctionnalités issues des besoins et expériences des utilisateurs. Il est mis à disposition et quelques manipulations sont encore nécessaire manuellement pour sa mise en fonction en attendant de futurs développements.

---

## Les projets de développement de Badass

Depuis 2020 et le début du développement de Badass, plusieurs domaines de l'archéologie plus spécialisés se sont rapprochés du projet initial. Le point commun entre ces rapprochements est la volonté, y compris dans ces domaines spécialisées, de rapprocher et d'inclure l'apport des spécialités dans l'enregistrement initial et général des données. Ceci implique une compréhension et une adoption du principe de l'enregistrement stratigraphique qui se traduit par l'ajout de relations directes dans le modèle logique de Badass. Il existe ainsi une contrainte d'interaction forte entre les données archéologiques acquises de manière générale et les données particulières issues des travaux de spécialistes. Cette interaction forte entre les deux, apporte, de fait, une plus-value dans la compréhension globale d'une opération archéologique et dans l'exploitation transversale des données.

L'ajout de tables en relation avec ce qui est considéré comme le noyau de la base de données, à savoir la structure initiale de Badass, s'effectue par l'exécution d'instructions SQL via la boîte de dialogue de l'extension à QGIS prochainement disponible.

Deux projets sont aujourd'hui à mentionner :

* Le projet de la Nécropole Numérique
* Le projet Archéologie sur le bâti

### La Nécropole Numérique - Extension Of The Dead
> **Christelle Seng**, Cyrille Le Forestier, Rachid El Hajaoui, Frédéric Barenghi, Micheline Kerien, Caroline Font, Florent Mercey, Thomas Guillemard, Jean-Baptiste Barreau

> **Références bibliographiques** : ***Seng et al. 2022***

Ce projet a pour objectif la mise au point et l’expérimentation d’outils numériques et des protocoles liés, dédiés à l’enregistrement des données de terrain du site de Noisy-le-Grand (93), *les Mastraits*. Depuis plus de 10 ans, cette nécropole a bénéficié de plusieurs campagnes de fouilles préventives. De 2019 à 2021, une fouille programmée a permis la mise au jour de nouvelles sépultures, complétant ainsi les 803 inhumations fouillées jusqu’à maintenant. Ces outils doivent répondre à une volonté de dématérialiser l’enregistrement des données de terrain, en s’appuyant sur la géomatique pour gérer les données spatiales et attributaires, à partir de relevés issus de la photogrammétrie.  

Les Unités Stratigraphiques composant chaque sépulture (creusement, contenant, comblements, dépôt du corps, réductions…) ainsi que les données propres à l’archéo-anthropologie (aménagement de la sépulture, données descriptives du sarcophage, position du défunt, etc.) sont enregistrées au sein d’une même base de données. La saisie et l’interrogation de ces données se fait par l’interface de QGIS directement, sans recourir à des logiciels tiers. 

![Le projet associé à la base de données développée pour la fouille des *Mastraits*][010a]

Pour davantage d’ergonomie et d’accessibilité, les tables attributaires sont affichées sous forme de formulaire(s), régi(s) par des options ou des masques de saisie. Nous avons également souhaité tester l'intégration d'un mannequin dans la structure de la base de données. Ce mannequin figure un squelette selon les trois âges anthropologiques : adulte, immature et périnatal. 
Ce  mannequin sert de thésaurus pour la saisie des descriptions à l'échelle du squelette entier ou de l'os. Nous envisageons également d'ajouter au mannequin d'autres géométries pour permettre le même type de saisie pour les contenants, les aménagements et le mobilier lié à la sépulture. La représentation du squelette sous la forme classique d'une fiche de démontage anthropologique, permet d'afficher les os présents, les observations et mesures et des résultats des analyses archéo-anthropologiques principalement grâce aux outils de mises en page de QGIS (atlas et variables) qui permettent de générer une page par sépulture dont le contenu sera fonction des données enregistrées.

![Le mannequin adulte][010b]

![Exemple de génération d'un atlas à partir des données de Badass Of The Dead][010b2]

Parallèlement, des séries de photographies sont prises dans le but de créer un modèle 3D. Outre la possibilité d’archiver les volumes du “vestige archéologique”, sa position dans un espace géoréférencé, le modèle 3D permettra d’éditer une orthophotographie, qui se substituera aux minutes de terrain manuelles. L’orthophotographie est importée dans QGIS sous forme de raster, le tracé vectoriel des US est effectué directement sur le support numérique choisi. Grâce aux relations mises en place, il sera donc également possible d'afficher les résultats des analyses archéo-anthropologiques *in-situ*.

Ce travail de terrain sera complété par les analyses effectuées en laboratoire, notamment les données anthropologiques (sexe, âge, maladies, métrique crânienne, etc.). L’ensemble de cette démarche fournira des outils d’analyse plus fiables et plus rapides que les données archéo-anthropologiques collectées de manière traditionnelle. Le rassemblement de toutes les informations dans un projet QGIS permet l’édition et la diffusion rapides de cartes dynamiques et interactives, et de fait systématiquement synchronisées avec les données saisies.

L’association en charge de la fouille programmée, Archéologie des Nécropoles (ADN), est en partenariat conventionné avec l’Inrap. Elle dispose du matériel nécessaire pour mener à bien cette expérimentation et se propose de le mettre à disposition des participants de la fouille.

À l’issue de cette expérience, l’équipe de recherche rédigera un article sur la démarche méthodologique et présentera une communication dans des colloques spécialisés en archéométrie et/ou en archéo-anthropologie. Des stagiaires des universités travaillant sur le numérique et des étudiants des universités en archéologie pourront être invités et mobilisés pour participer au projet.


#### Modèle conceptuel de l'extension de la Nécropole Numérique et de ses relations avec Badass

![Modèle conceptuel de l'extension Nécropole Numérique][010c]

Nous ne rentrerons pas ici dans le détail de chaque table de l'extension, cependant, il est important de souligner le but recherché : pouvoir enregistrer les données anthropologiques spécifiques à la fouille des *Mastraits*, mais également dans d'autres contextes relevant du domaine de l'anthropologie. Ainsi, cette structure, reprise plusieurs fois, a été fortement simplifiée pour ne retenir que les entités présentes systématiquement dans une étude anthropologique à savoir :

* le squelette (table ***t_squelette***) qui permet de résumer l'ensemble des observations physiques du squelette
* la description taphonomique de la sépulture (table ***t_obs_sep***)
* la description du contenant (table ***t_contenant***)
* la description des os (table ***t_os***)
* les mesures et observations faites à l'échelle de l'os (table ***t_mesure***)

##### Spécificité d'enregistrements des tables ***obs_sep*** et ***contenant***

Afin de garantir une réutilisation possible de cette base de données dans d'autres contextes, et afin d'éviter de générer des tables avec un nombre de champs très importants qui ne seraient pas saisies systématiquement, nous avons procédé à une petite espièglerie conceptuelle pour les descriptions des spécialistes. Ces dernières étant très liées aux méthodes personnelles des spécialistes et aux contextes chronologiques, culturels et régionaux des sépultures, nous avons considéré les données non pas comme des champs à remplir, mais comme des enregistrements dont la liste seraient fonction de l'utilisateur. Pour chaque élément de description listé, un résultat est attendu et un commentaire peut-être ajouté. 

Prenons un exemple simple pour expliquer cette espièglerie, la table initialement structurée de cette manière :

| id_obs_sep | envergure_totale | envergure_epaule | envergure_bassin | envergure_pieds | connexions | stade_conservation | position_corps   | position_avantbras | position_membre_inferieur | linceul | etc... |
|------------|------------------|------------------|------------------|-----------------|------------|--------------------|------------------|--------------------|---------------------------|---------|--------|
| 1          | 145              | 35               | 42               | 15              | 1          | bon                | decubitus dorsal | 1_90               | 1_1                       | 1       |        |

Sera désormais structurée ainsi :

| id_obs_sep | numus | nature_observation              | resulat          | commentaire | id_modèle |
|------------|-------|---------------------------------|------------------|-------------|-----------|
| 1          | 7008  | envergure totale                | 145              |             | 1         |
| 1          | 7008  | envergure des épaule            | 35               |             | 2         |
| 1          | 7008  | envergure du bassin             | 40               |             | 3         |
| 1          | 7008  | envergure des pieds             | 22               |             | 4         |
| 1          | 7008  | connexions                      | 1                |             | 5         |
| 1          | 7008  | stade de conservation           | bon              |             | 6         |
| 1          | 7008  | position du corps               | décubitus dorsal |             | 7         |
| 1          | 7008  | position des avant-bras         | 135_180          |             | 8         |
| 1          | 7008  | position des membres inférieurs | 1_1              |             | 9         |
| 1          | 7008  | présence d'un linceul           | 1                |             | 10        |
| 1          | 7008  | ...                             |                  |             |           |

Cette liste est donc répétée pour chaque dépôt funéraire d'un corps. Elle est fonction du type de dépôt : primaire ou non primaire. Ceci change également la nature de la relation entre l'US du dépôt du corps et les observations taphonomiques qui en découlent, puisque la relation initialement de *un à un* devient relation de *un à plusieurs*.

Le fonctionnement est le même pour la description des contenants.

Cette liste peut être modifiée totalement dans la table ***model_anthropo*** qui figure dans la base de données avec l'extension Of The Dead.

Afin de faciliter la saisie, nous avons également eu recours à des *triggers*.

##### Fonctionnement des ***triggers*** dans Badass Of The Dead

![Schéma de fonctionnement des triggers dans Badass Of The Dead][010d]

Ces *triggers* interviennent à plusieurs niveaux :

* lorsqu'une unité stratigraphique est créée dans Badass (table ***t_us***) et que l'interprétation de cette US correspond à un 'dépôt primaire inhumation', les instructions permettent les actions suivantes :
    *  création automatique d'un enregistrement dans la table ***t_squelette*** avec le même numéro d'US
    *  création automatique de la liste d'enregistrements associée à l'US de la sépulture, selon la table ***model_anthropo*** correspondant à une inhumation primaire dans la table ***t_obs_sep*** (un équivalent d'un copier/coller des enregistrements répondant au qualificatif 'inhumation primaire' entre la table ***model_anthropo*** et la table ***t_obs_sep***) 
 * lorsqu'une unité stratigraphique est créée dans Badass (table ***t_us***) et que l'interprétation de cette US correspond à un 'dépôt non primaire inhumation', les instructions permettent les actions suivantes :
    *  création automatique d'un enregistrement dans la table ***t_squelette*** avec le même numéro d'US, s'il existe plusieurs squelettes (ou individus) dans le dépôt non primaire, un incrément est possible concaténé au numéro d'US
    *  création automatique de la liste d'enregistrements associée à l'US de la sépulture, selon la table ***model_anthropo*** correspondant à une inhumation non primaire dans la table ***t_obs_sep*** (un équivalent d'un copier/coller des enregistrements répondant au qualificatif 'inhumation non primaire' entre la table ***model_anthropo*** et la table ***t_obs_sep***) 
* lorsqu'une unité stratigraphique est créée dans Badass (table ***t_us***) et que l'interprétation de cette US correspond à un 'contenant inhumation', les instructions permettent les actions suivantes :
    *  création automatique de la liste d'enregistrements associée à l'US du contenant, selon la table ***model_anthropo*** correspondant à un contenant dans la table ***t_contenant*** (un équivalent d'un copier/coller des enregistrements répondant au qualificatif 'contenant inhumation' entre la table ***model_anthropo*** et la table ***t_contenant***) 
* lorsque la classe d'âge du squelette est définie dans la table ***t_squelette***, les instructions permettent les actions suivantes :
    *  création automatique de l'ensemble des os et de leur géométrie dans la table ***t_os*** avec le même numéro d'US selon la table ***mannequin*** (un équivalent d'un copier/coller de tous les os du ***mannequin*** vers la table ***t_os***)

Pour cette dernière instruction, la création d'une géométrie par os et par squelette permet de gérer pour chaque individu la présence ou l'absence d'os, et même une modification de géométrie dans le cas d'os partiellement conservés. Il est également possible de décrire pour chacun des os, les mesures et observations faites (détermination du sexe, de l'âge et description des pathologies entre autres).

### Archéologie sur le bâti
> **Caroline Font**, Carole Lallet, Victorine Mataouchek, Florence Tane

> **Références bibliographiques** : **Font *et al.* à paraître**, ***???***

Initié en 2021, à l'occasion d'une fouille d'archéologie sur le bâti de l'église Notre-Dame de Béthléem à Ferrières-en-Gâtinais (45) dirigée par Carole Lallet, ce projet de mise en place d'un enregistrement archéologique adapté à l'étude du bâti s'appuie sur un enregistrement stratigraphique classique. L'unité stratigraphique constitue la base de de l'enregistrement et à ce titre, les US sont en relations d'antériorité, de postériorité ou d'égalité les unes avec les autres. Une différence réside cependant dans la manière dont ces US sont décrites. Comme pour toutes les bases de données, le niveau d'abstraction est à définir en fonction de l'exploitation des données envisagées dans la problématique d'un sujet d'étude. Dans le cas de l'archéologie sur le bâti et de manière un peu différente par rapport au projet de la Nécropole Numérique, cette extension doit permettre une certaine souplesse pour s'adapter au contexte chronologique et au temps disponible pour la description puis pour l'exploitation des données. À ce titre, la description des US dépend totalement de l'opérateur de saisie puisqu'elle est extraite de la table d'US dans une table fille de description. Il est donc possible de décrire à l'envie tel ou tel élément d'une US en ajoutant autant de descripteurs que souhaités.
Par ailleurs, est dans un souci d'adaptabilité, l'ensemble des descripteurs figurent dans un thésaurus décrit et hiérarchisé qui permet, à l'envie, d'ajouter des valeurs manquantes en fonction du contexte chronologique et des problématiques scientifiques d'une opération.

L'autre spécificité de l'archéologie sur le bâti est l'intrusion de concepts étrangers à l'archéologie qui touchent davantage à l'architecture. Il s'agit des entités architecturales, ou EA, qui désignent des éléments liés à l'architecture d'un bâtiment : porte, fenêtre, cheminée, niche, etc... La difficulté réside dans le fait que les EA ne sont pas forcément cohérents d'un point de vue chronologique (plusieurs phases peuvent s'y inscrire). Leur réalité est donc plutôt décorrélée de l'archéologie, mais pour autant, il existe des relations logiques entre US et EA. En effet, une ou plusieurs US peuvent contenir une ou plusieurs EA, et, dans le cas où une EA aurait fait l'objet de réfection, plusieurs US peuvent être liées à une même EA. Les liens de cardinalités entre US et EA sont donc de plusieurs à plusieurs.
L'intérêt d'enregistrer ces EA est de permettre d'ajouter des repères architecturaux descriptifs qui sont bien utiles pour le confort de compréhension de l'étude sur le bâti. Les EA sont assez proches, d'un point de vue structurel, des ensembles archéologiques qui possèdent le même lien de cardinalité, et qui sont parfois, décorrélés d'interprétation archéologique. 

Par simplicité, les seules modifications dans la structure de Badass sont l'ajout de valeurs dans la liste de valeur pour les champs permettant d'indiquer la nature liée au bâti dans les tables **t_us** et **t_ens**. Cette distinction permet de filtrer le contenu du formulaire selon ce champ et de donner accès à la seule table ajoutée dans la structure d'origine : la table **bati_description**. Par ailleurs, la table contenant le thesaurus pour le bâti est, pour le moment, distinguée du thésaurus d'origine.

Ce travail est actuellement en cours et devra peut-être faire l'objet d'adaptation des interfaces dans QGIS pour en permettre l'utilisation sur le terrain sur des dispositifs de saisie réduits (tablettes PC, smartphone...).

#### Modèle conceptuel de l'extension bâti

![Modèle conceptuel de l'extension bâti][011]


### Programmation

Comme évoqué plus haut, notre volonté, au moins pour Badass, est de s'appuyer au maximum sur les outils natifs de QGIS afin de permettre aux utilisateurs une prise en main et une adaptation personnalisée de la base de données. Néanmoins, nous devons permettre à la communauté d'archéologues, un accès optimisé à Badass dans leur utilisation quotidienne. À ce titre, un développement informatique est nécessaire.

Le langage Python, langage de programmation de QGIS, permet d'exécuter des codes dans des langages différents. La mise à disposition d'une extension permettant à la fois de générer la base de données SpatiaLite, depuis QGIS ainsi que le projet déjà mis en forme est proposée parmi les plugins officielles expérimentales de QGIS depuis 2022. Cette extension permet, à partir du code SQL de création de base ainsi que le code XML d'un projet QGIS, de générer les deux fichiers consitutifs de Badass. Une mise à jour de l'un ou l'autre nous épargne donc de retoucher le code Python que nous ne maîtrisons pas et nous rend indépendant d'informaticiens lors de la mise à jour de la base de données ou du projet.

#### L'extension Badass

L'extension Badass est accessible dans QGIS *via* la fenêtre d'extension.

![Accès au plugin Badass dans QGIS][012]

Une fois le plugin installé, un bouton aux couleurs de Badass apparaît dans la barre d'outils de QGIS. L'interface du plugin permet de créer une base de données puis, de désigner le dossier d'enregistrement (1), le nom du projet (2) et le nom de la base données (3). Attention, le changement de nom est déconseillé a posteriori, donc, faites d'ors et déjà le bon choix pour ces trois paramètres. Le bouton Créer (4) lance le programme et une barre de progression permet de vérifier que le processus abouti correctement. 
Pour le moment, l'accès aux extensions Bdass Of The Dead pour les études archéo-anthropologiques et House of Badass n'est pas actif dans le plugin, cest dernières étant en cours de développement.
Une montée de version sur une base déjà créée n'est pas encore possible avec ce plugin.

### Prise en compte des nouvelles contraintes du numérique

La masse de données numériques produites nous pousse de plus en plus à tenir compte des recommandations de gestion de ce type de données.
Nous nous emploierons donc à inclure dans la structure de la base de données une table permettant de renseigner l'ensemble des métadonnées liées à l'opération selon les normes en vigueurs. 
Notre volonté d'intégrer les thésaurus, de les documenter, d'utiliser un langage ouvert et multi-plateforme nous semble déjà aller dans le sens de production de données FAIR, à savoir : Facile à trouver, Accessible, Interopérable et Réutilisable.
Nous comptons sur le soutien de notre institut pour poursuivre ce travail dans ce sens.

---

## Bibliographie

* **Bolo, De Muylder, Font, Guillemard 2014**

BOLO (A.), DE MUYLDER (M.), FONT (C.), GUILLEMARD (T.) - de la tablette pc à la cartographie de terrain: exemple de méthodologie sur le chantier d’archéologie préventive de Noyon (Oise), *Archeologia e Calcolatori*, Supplemento 5, 2014, 247.

* **Codd 1970**

CODD (T.) — A relational model of data for large shared data banks. *CACM*, 13(6):377–387, june 1970

* **Desachy 2008**

DESACHY (B.). — *De la formalisation du traitement des données stratigraphiques en archéologie de terrain*. Paris : Université Paris 1-Panthéon Sorbonne, 2008, 338 p.

* **Galinié, Randoin 1987**

GALINIE (H.), RANDOIN (B.) — « Enregistrement et exploitation des données de fouilles à Tours », *in Enregistrement des données de fouilles urbaines (première partie), Centre National d’archéologie Urbaine*, Tours : Centre National d’archéologie Urbaine, pp. 6173.

* **Lozano & Georges 2019**

LOZANO (V.), GEORGES (É.) — not Only SQL. Tout ce que vous avez toujours voulu savoir sur le SQL sans jamais avoir osé le demander. Framabook, mars 2019, 373 p. [Not Only SQL](https://framabook.org/docs/onlysql/onlysql-Lozano-Georges-ArtLibre-juin2019.pdf)

* **Moreau 2016**

MOREAU (A.) — GIS, An Answer to the Challenge of Preventive Archaeology? The Attempts of the French National Institute for Preventive Archaeology (Inrap) *Actes de colloque CAA2015 (43)*, Archaeopress Publishing Ltd, 2016, pp. 303

* **Seng et al. 2022**

SENG (C.), LE FORESTIER (C.), EL-HAJAOUI (R.), coll. FONT (C.), MERCEY (F.), GUILLEMARD (T.) — Acquisition numérique des données archéo-anthropologiques et spatiales - Le projet « Nécropole Numérique ». *Archéopages Hors Séries* n°2, 2022, p.328-333

[000]:file/figure/000_entete_badass.png
[000b]:file/figure/000b_ramen.png
[001]:file/figure/001_ex_poly_mobilier_us_fait.png
[002]:file/figure/002_poly_fait_trigger.png
[003]:file/figure/003_triggers_badass_2022.png
[004]:file/figure/004_legende_MLD.png
[005]:file/figure/005_mld_badass_230422.png
[005b]:file/figure/005b_mld_badass_230303.png
[005c]:file/figure/005c_thesaurus_tri.png
[006]:file/figure/006_mld_coupe_strati.png
[007]:file/figure/007_ex_representation_coupe.png
[008]:file/figure/008_ex_export_coupe_qgis.png
[009]:file/figure/009_support_formation_SIG3_1.png
[010a]:file/figure/010a_projetqgis_noisy2022.png
[010b]:file/figure/010b_mannequinadulte.png
[010b2]:file/figure/010b2_atlas.png
[010c]:file/figure/010c_badass_otd_v2022.png
[010d]:file/figure/010d_triggers_badassotd_2022.png
[011]:file/figure/011_mcd_bati.png
[012]:file/figure/012_plugin_badass.png
