# Les faits

Deux couches gèrent les faits : ***t_fait***, dans le groupe Enregistrement Archéologique, et ***poly***, dans le groupe des 6 couches. 

## Structuration : 

Les champs des deux tables sont les suivants : 

**Couche *t_fait* :**
- **id_fait** : attribué automatiquement par la base (nombre entier)
- **numfait** : numéro du fait attribué par l'équipe (nombre entier unique)
- **interpret** : interprétation fonctionnelle du fait. Liste de valeur dans le thésaurus (texte)
- **interpret_alter** : interprétation alternative. Liste de valeur dans le thésaurus (texte)
- **douteux** : renseigne le caractère douteux (1) ou non (0) du fait (nombre entier)
- **equiv_diag** : renseigne, en fouille, le numéro attribué au diagnostic (texte)
- **statut** : statut de fouille du fait. S'appuie sur une liste de valeur du thésaurus (texte)
- **rais_annule** : raison de l'annulation du fait (texte)
- **fouille** : mode de fouille du fait (texte)
- **enr_fini** :  0 : enregistrement en cours ; 1 : enregistrement achevé (entier)
- **relev_fini** : 0 : non relevé ; 1 : relevé (entier)
- **photo_fini** : 0 : non photographié ; 1 : photographié (entier)
- **profil** : description du profil du fait. Peut s'appuyer sur une liste de valeur du thésaurus (texte)
- **forme** : description de la forme en plan du fait. Peut s'appuyer sur une liste de valeur du thésaurus (texte)
- **orient** : orientation en plan du fait. Peut s'appuyer sur une liste de valeur du thésaurus (texte)
- **orient_calc** :  azimuth (angle par rapport au Nord à 0°) majoritaire du fait, exprimé en degrés (nombre réel)
- **descrip** : courte description du fait (texte)
- **progf_app** : profondeur d'apparition du fait, en mètres (nombre réel)
- **diam** : diamètre du fait, en mètres (nombre réel)
- **dim_max** : dimension maximale du fait, en mètres (nombre réel)
- **dim_min** : dimension minimale du fait, en mètres (nombre réel)
- **epais** : épaisseur maximale du fait, en mètres (dans le cas d'un mur par exemple). (nombre réel)
- **prof_haut** : profondeur ou hauteur conservée du fait, en mètres. (nombre réel)
- **periode** : période d'attribution du fait. Liste de valeur disponible dans le thésaurus. (texte)
- **note** : commentaire divers (texte)

**couche *poly* :**

- **id_poly** : identifiant unique de la couche (nombre entier)
- **numpoly** : numero unique du polygone, de 1 à n (nombre entier)
- **typoly** : type de polygine : fait, us (texte)
- **interpret** : interprétation du polygone : fossé, TP, remblai, démolition...(texte)
- **dadebut** : année inférieure (nombre entier)
- **datefin** : année supérieure (nombre entier)
- **date_leve** : date de la levée au format aaaammjj (entier)


## Fonctionnement des deux tables : 

Les faits sont numérotés de 1 à n sur le terrain au fur et à mesure de leur découverte et enregistrés dans la table t_fait. L'équipe complète les informations cette table au fir et à mesure de l'avancée des travaux. 
Notez que les US des faits sont renseignées dans la table t_us à l'aide du champ numfait.  

Lors de la levée, et à l'issue du traitement de ses données, le topographe fournit à l'équipe la table poly avec pour valeur minimale la géométrie, le numpoly et le typoly (fait, US, structure...)

Lorsque les données sont importées dans la table poly du projet badass, les géométries des faits seront répliquées de la table ***poly*** du groupe 6 couches vers la table ***t_fait*** du groupe EAT, si : 
- dans la table poly, la valeur de *typoly* est "fait" (ou "Fait")
- le numéro du fait enregistré dans "numpoly" de la couche *poly* est égal à celui enregistré dans "numfait" de la table *t_fait*.

