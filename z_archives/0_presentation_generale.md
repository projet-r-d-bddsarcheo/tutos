# Présentation générale

Badass est un système de Base de donnée relationnelle et spatiale qui permet d'associer les données attributaires d'une opération archéologique avec les données spatiales (plans généraux et relevés divers). <br> Badass rassemble, dans **une seule et même base de données**, et dans les mêmes tables, **les informations géométriques relevées par les topographes et les données attributaires enregistrées par les archéologues**. 

Badass **assure l'intégrité des données archéologiques** qui ne sont plus séparées entre différents supports ou formats, et, par l'emploi d'un format de base de données libre, permet de considérer l'interopérabilité et la pérénité de la donnée archéologique. 

## La base de données

La base de données est au format spatialite, qui se présente sous la forme d'un fichier unique, doté d'une extension .sqlite, et qui comporte l'ensemble des tables permettant l'enregistrement des données. 

Badass comporte 71 tables ainsi que 40 vues spatiales permettant de réaliser certains traitements automatiques. Le fonctionnement de la base s'appuie également sur des déclencheurs, qui automatisent certaines actions ou calculs. Nous avons ainsi intégré dans Badass des réplications de données d'une table à l'autre, répondant à certains des principes que nous avons retenu pour la création de la base. 

## Le projet qgis

Le projet Qgis est l'interface de Badass. C'est lui qui permet d'interagir avec les données. Il propose déjà une certaine organisation des différentes couches de données (toutes reliées à la base de données), ainsi que des styles par défaut (etiquetage, symbologie, formulaires). Bien entendu, ces éléments sont modifiables par l'utilisateur qui peut créer ses propres styles ou modifier ceux existants. 

Le projet Qgis distingue plusieurs groupes de couches. Les deux principaux sont : 
- **Enregistrement Archéologique de Terrain (EAT)**: il regroupe les couches permettant d'enregistrer les faits, les us, les tranchées, les sondages, le mobilier, les prélèvements, etc.  
Plusieurs sous groupe comportent les éléments nécessaires pour établir les relations entre les couches, enregistrer la documentation (photos, minutes), des informations sur l'équipe, et enfin des couches permettant de réaliser les coupes et logs dans Qgis
- **6 couches (6C)** : on retrouve les couches issues du modèle 6 couches proposé par l'Inrap

Le projet Qgis et son fonctionnement seront détaillés dans un chapitre ultérieur. 

## Important : le fonctionnement de Badass. 

L'un des principes fondateur de Badass est d'associer l'enregistrement archéologique purement attributaire avec l'enregistrement du topographe, spatial et attributaire. Il s'agit de permettre aux données attributaires de l'archéologue de récupérer les données spatiales du topographe.  

Il est **important** de respecter l'ordre de fonctionnement suivant **pour ne pas rencontrer de difficultés** lors de l'utilisation de la base. 

1) les archéologues dressent la liste de leurs observations dans les tables du groupe **Enregistrement Archéologique et Technique (EAT)**. Il s'agit des **faits, des US, des sondages, tranchées, isos mobilier,** etc. Reportez vous aux fiches techniques spécifiques pour la description précise de chaque table. 

2) Le topographe réalise la levée en récupérant les numéros et types des archéologues. Une fois traitées, ces données sont importées dans les tables des 6 couches (6C).  
Il peut être nécessaire d'importer les données du topographe dans des fichiers temporaires (des shapes par exemple) dans le projet Sig Badass pour vérifer qu'elles sont bien formatées selon les 6 couches d'une part, et que les données ont bien été créees dans les couches EAT d'autre part.  
Une fois ces éléments verifiés, il suffit alors de copier les données depuis les couches temporaires et de les coller dans les couches du groupe 6 couches du projet Badass.

3) La géométrie déposée dans les 6 couches est **automatiquement copiée dans les tables respectives du groupe EAT** : les poly sont versés dans les faits ou les us selon leur nature, les ouvertures dans les tranchées ou les sondages, etc. Des lors, sauf exception, l'archéologue n'a plus à intervenir que dans les couches du groupe Enregistrement Archéologique et Technique. 
