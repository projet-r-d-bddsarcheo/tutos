## La base de données

### caractéristiques générales

La base de données est au format **spatialite**, version cartographique de sqlite, un moteur de base de données libre. Il s'agit d'un fichier unique, doté d'une extension .sqlite, et qui comporte l'ensemble des tables permettant l'enregistrement des données recueillies sur le terrain, mais également des informations géométriques et spatiales collectées par le topographe. 

Badass comporte 71 **tables** ainsi que **40 vues spatiales** permettant de réaliser certains traitements automatiques. Le fonctionnement de la base s'appuie également sur des **déclencheurs**, qui automatisent certaines actions ou calculs. Nous avons ainsi intégré dans Badass des réplications de données d'une table à l'autre, afin d'assurer l'intégrité de la donnée spatiale et attributaire.  

Les détails de l'ensemble des tables, de leur hierachie, les codes des différentes vues peut être consulté [ici](https://projet-r-d-bddsarcheo.gitlab.io/bdds_archeo/reseauSIG_BADASS.html). 

L'ensemble du code permettant de générer la base de données peut être consulté ici

> **note** : la base de données spatialite est encodée en UTF_8. Or Windows utilise son propre encodage, 1252, qu'il impose à Qgis. Ainsi, dans Qgis, les données contenues dans les tables de Badass peuvent présenter des caractères étranges, dus à une mauvaise interprétation des accents. Vérifiez dans ce cas que les couches sont bien encodées en UTF_8 (dans les propriétés de la couche, onglet source). Il est également possible d'imposer à Windows l'usage de l'UTF_8, mais c'est une autre histoire...

### Les tables

Les tables de la base de données sont constituées de tables d'enregistrement archéologiques "classiques", telles que la table des faits, des us, des photos, etc. 
Des tables de jonction permettent de créer des relations de n à n entre les différentes tables, telles une table de relation pour relier les photos et les faits par exemple, ou les faits et les tranchées. 
Là encore, le détail des différentes tables peut être consulté [ici](https://projet-r-d-bddsarcheo.gitlab.io/bdds_archeo/reseauSIG_BADASS.html).  

### Les déclencheurs (triggers)

De nombreuses actions ont été automatisées dans la base de données, par le biais de déclencheurs qui peuvent mettre à jour ou créer des données dans une table. Ces déclencheurs sont activés lors des mises à jour, suppressions ou créations dans certaines tables et peuvent affecter d'autres tables.<br>
Via ces déclencheurs nous assurons **l'intégrité de la donnée**, et facilitons certaines opérations. 

**L'un des principes fondateur de Badass** est la réplication des données géométriques depuis les tables "6 couches" vers les tables tables attributaires dédiées (t_fait, t_us, t_sondage, etc...).  

Par exemple, si un fait est crée dans la table poly, sa forme géométrique sera automatiquement ajoutée dans la table t_fait **si, et seulement si, un enregistrement correspondant existe dans t_fait**. Toute modification sur la forme géométrique dans poly sera également répercutée. 
> *Exemple : Un fait numéroté 10 a été crée dans la table t_fait. Si on crée, dans la table poly, une forme géométrique dont le numéro (numpoly) est 10 et dont l'interprétation (typoly) est "fait", alors, la forme géométrique créee sera copiée dans la table t_fait. Cette action sera également réalisée en cas de mise à jour de la forme. En cas d'absence d'enregistrement concordant dans t_fait, aucune création ou modification de la géométrie ne sera effectuée. 

### Des vues

Des **vues** permettent d'obtenir des tableaux récapitulatifs (les us des faits par exemple, ou les relations entre us...) qui peuvent être utilisés en postfouille. Trois vues sont également prêtes à être exportées pour être utilisées dans le stratifiant.

> Si une table peut être comparée à une feuille de tableur sans aucune mise en forme, une vue serait alors un tableau croisé dynamique. Une vue peut interroger différentes tables et les croiser, effectuer des calculs, etc. Il est possible d'intégrer les données géométriques à une vue. On parle alors de vue spatiale. 
>

