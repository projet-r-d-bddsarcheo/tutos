## Eviter les erreurs et se simplifier la vie

badass reste un projet relativement complexe, et qui nécessite un minimum de rigueur et méthode. 

### Importer les styles pour les tables attributaires EAT

Avant de commencer à traiter les données collectées, il peut être nécessaire d'adapter le projet Badass à vos besoins. 

Si vous avez déjà travaillé avec Badass, et crée des styles, n'hésitez pas à les copier depuis un ancien projet, et à les coller dans le nouveau. 

Sinon, **commencez par créer un style par défaut pour chaque couche**, notamment d'adapter les formulaires à votre propre utilisation.  
Une fois ceci fait, si vous êtes amené à créer un nouveau style pour une couche,  le formulaire sera bien répliqué. 

Enfin, il est également possible de partir d'un ancien projet Badass. Enregistrez ce projet à côté de votre base de données puis changez la source de donnée de vos couches. Notez qu'un plugin peut vous aider : changeDataSource. 

### compléter et corriger les tables attributaires

Nous ne saurions trop vous conseiller de compléter au mieux les donénes dans vos tables attributaires, en particulier celles du groupe EAT.  
Veillez à ce que ces données soit bien complétées, sans erreur. Si vos tables attributaires sont bien renseignées, que l'ensemble des champs contient bien les données nécessaires, alors votre projet devrait vous permettre de répondre déjà à plusieurs questions classiques : plan des faits période, iventaire des faits, des US, du mobilier, etc. 

### Réalisez vos figures

Ce n'est qu'une fois que tout est bien complété que vous pouvez réaliser vos figures. 