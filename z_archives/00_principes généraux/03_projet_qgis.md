# Le projet Qgis

Le projet Qgis sert d'interface à la base de donnée et permet de créer, modifier, supprimer les données stockées dans la base Badass. 
Il permet d'interagir avec les données attributaires, mais également avec les données spatiales. 
Le projet propose pour chaque table un ou des formulaires de saisie, simplifiés. Chaque table dispose donc de ses propres formulaires, qui peuvent être modifiés et paramétrés par l'utilisateur. Les formulaires étant gérés par les styles de Qgis, il est donc possible de créer de très nombreux formulaires selon les besoins de l'utilisateur. 

## Les couches du projet

Les tables de la base de données Badass sont présentes dans le projet Qgis : les données attributaires, les 6 couches, les tables de relation, les tables pour les coupes, les vues. 
Si nous présenterons ces couches succinctement, ainsi que quelques champs au gré de fiches techniques, le détail de l'ensemble des champs pour chaque table peut-être consulté [ici](https://projet-r-d-bddsarcheo.gitlab.io/bdds_archeo/reseauSIG_BADASS.html). 

S'il est possible de rajouter des couches, comme dans tout projet qgis, tel des fonds de carte IGN ou d'autres shape, **il est fortement déconseillé de supprimer des couches**. Ceci entrainerait le  disfonctionnement du projet, et conduirait à la perte d'informations.  
Cependant, il est possible de masquer les couches, en les décochant, de réorganiser le projet, les groupes, et de ne laisser visibles que les couches dont vous pourriez avoir besoin. 

> **En bref, réorganisez, déplacez, groupez, dégroupez, cochez ou décochez, mais ne supprimez aucune des couches du projet**.

![couches d'un projet Badass](ressources/couches_projet_badass.png)


### Le groupe CREATION RAPIDE 
Sont rassemblées ici des tables permettant de créer des enregistrements plus rapidement qu'avec les tables du groupe "Enregistrement archéologique et technique". 

- entite_rapide : pour créer rapidement une entité tel un fait, une us, du mobilier 
- serie : pour définir la numérotation des faits, us, mobilier qui seront crées
- vue_numaximum_serie : vue qui permet de calculer le numéro maximal d'une serie donnée
- vector_layers : couche système qui liste les couches possedant une géométrie.

### la couche erreur_geometry

Il s'agit d'une vue, c'est à dire une couche calculée, qui permet d'identifier les erreurs de géométrie sur l'ensemble des couches géométriques du projet. 

### Le groupe Enregistrement archéologique et technique

Il comporte les couches permettant l'enregistrement des données attributaires de terrain : 

* t_axe : enregistrement des axes
* t_fait : enregistrement des faits
* t_log : enregistrement des logs
* t_mobilier : enregistrement primaire du mobilier, avant études spécialistes.
* t_prelevements : enregistrement des prélèvements
* t_sondage : enregistrement des sondages
* t_ens : enregistrement des ensembles
* t_tranchee : enregistrement des tranchées
* t_us : enregistrement des Unités Stratigraphiques

L'icone devant le nom de certaines de ces tables indique la présence d'une géométrie. C'est le cas par exemple pour la couche t_fait, ou la couche t_us.

#### Le sous-groupe documentation

Il rassemble les couches permettant l'inventaire des photos, des minutes, ainsi que deux tables concernant les levées photogrammetriques. 

* t_minute : enregistrement des minutes (pas de géométrie)
* t_photo : enregistrement des photos (pas de géométrie)
* t_photogram : enregistrement des modèles photogramétriques
* t_prodphotogram : table d'enregistrement des rasters issus des traitements photogrammétriques

La couche thesaurus_badass permet de lister l'ensemble du vocabulaire utilisé. 

#### le sous-groupe chronostratigraphie

Dans le groupe Chrono-Stratigraphie, trois couches, non géométriques, permettent d'enregistrer les séquences, phases et périodes. Il s'agit d'un système simple de liste, sous forme de tableau ou de formulaire. 

- On ajoutera une séquence en enregistrant son numéro, son nom et sa datation
- On ajoutera une phase en enregistrant son numéro, son nom, sa datation, un TPQ et un TAQ
- On ajoutera une période en renseignant son numéro, son nom, sa datation, un TPQ et un TAQ. 

#### Le sous-groupe Equipe

Il comporte : 
- une couche pour inventorier les membres de l'équipe de fouille et de post fouille
- une couche pour lister les taches effectuées soit sur le terrain, soit en post fouille. 

#### Le sous-groupe coupe

Il rassemble les tables permettant le dessin des coupes dans Qgis. La méthodologie concernant leur utilisation est présentée dans un tutoriel complet : 
[Les coupes dans Qgis](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/coupe_SIG/coupes_QGIS.html)

#### Le sous groupe Table de jonction
Le groupe table de jonction rassemble les tables permettant d'établir les relations entre les tables du groupe Enregistrement archéologique. 

* j_equipe : relation entre les membres de l'équipe et les photos, prélevements, minutes...
* j_equipe_tache : relation entre les membres de l'équipe et les différentes taches
* j_seq_phase : relations entre les séquences et les phases
* j_phase_per : relations entre les phases et périodes
* j_rel_axe : relations entre les axes et les faits, les sondages, les tranchées, les us
* j_rel_ens : relations entre les ensembles, les faits, les us 
* j_us_log : relation entre les logs et les us
* j_rel_us : relations entre les US.
* j_rel_fait : relations entre les faits (recoupements)
* j_rel_minute : relations entre les minutes et axes, ensembles, faits, minutes, iso, logs, sondages, tranchées, et us (quelle us, fait, iso sur quelle(s) minute(s)...)
* j_rel_photo : relations entre les photos et les ensembles, fait, iso, log, modeles photogrametriques, sondages, tranchées, us 
* j_rel_photogram : relations entre les photogramétries et les points d'appuis
* j_rel_sondage : relations entre les sondages, et les faits, les logs, ou les us. 
* j_rel_tranchee : relations entre les tranchées et les faits, les logs, ou les us. * 

### Le groupe 6 couches

Il s'agit des **6 couches** du modèle utilisé à l'Inrap, à savoir les couches Poly, Axe, Point, Log, Ouverture, et emprise. **Les données de ces tables sont crées par le topographe.** Plus de détails sur ces couches peuvent être trouvés [ici](https://docs.google.com/a/inrap.fr/viewer?a=v&pid=sites&srcid=aW5yYXAuZnJ8cmVzZWF1LXJlZmVyZW50cy1zaWd8Z3g6MTY2NTliYzRjNzc1Yjk4Yg). 

### Les requêtes

Nous avons rassemblé ici un certain nombre de requêtes précalculées. Il s'agit de vues, générées par la base de données spatialite, qui syntétisent certaines données. 

> contrairement aux couches, les vues ne sont pas modifiables. Il s'agit en quelque sorte de l'équivalent d'un tableau croisé dynamique dans excel. Vous pouvez les supprimer du projet si elles ne vous intéressent pas. 

Vous pouvez toujours supprimer ces requêtes, ce qui peut alléger votre projet Qgis, ces requêtes étant calculées en temps réel. Si vous en avez besoin, vous pourrez toujours les réimporter ultérieurement. 