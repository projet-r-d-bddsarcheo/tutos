# BADASs, principes généraux

## Présentation générale

BADASs, ou Base Archéologique de Données Attributaires et SpatialeS couple **un projet Qgis et une base de données Spatialite**. BADASs est destinée à répondre à une problématique principale, à savoir l'enregistrement des **données de terrain** et leur utilisation pour l'étude en post fouille lors de la réalisation d'une opération d'archéologie. 

### La base de données

La base de données est au format spatialite, qui se présente sous la forme d'un fichier unique, doté d'une extension .sqlite, et qui comporte l'ensemble des tables permettant l'enregistrement des données. 

Badass comporte 71 tables ainsi que 40 vues spatiales qui réalisent certains traitements automatiques. Le fonctionnement de la base s'appuie également sur des déclencheurs, qui automatisent certaines actions ou calculs. Nous avons ainsi intégré dans BADASs des réplications de données d'une table à l'autre, répondant à certains des principes que nous avons retenu pour la création de la base. 

### Le projet qgis

Le projet Qgis est l'interface de Badass. C'est lui qui permet d'interagir avec les données. Il propose déjà une certaine organisation des différentes couches de données (toutes issues de la base de données), ainsi que des styles par défaut (etiquetage, symbologie, formulaires). Bien entendu, ces éléments sont modifiables par l'utilisateur qui peut créer ses propres styles ou modifier ceux existants. 

Le projet Qgis distingue plusieurs groupes de couches. Les deux principaux sont : 
- Enregistrement Archéologique de Terrain (EAT): il regroupe plusieurs tables permettant d'enregistrer les faits, les us, les tranchées, les sondages, le mobilier, les prélèvements, etc.  
Plusieurs sous groupe comportent les tables nécessaires pour établir les relations entre les couches, enregistrer la documentation (photos, minutes), des informations sur l'équipe, et enfin des couches permettant de réaliser les coupes et logs dans Qgis
- 6 couches : on retrouve les couches issues du modèle 6 couches proposé par l'Inrap

Il est recommandé de ne pas supprimer de couches issues de ces deux groupes.  

## fonctionnement de BADASs. 

L'un des principes fondateur de BADASs est d'associer l'enregistrement archéologique purement attributaire avec l'enregistrement du topographe, spatial et attributaire. Il s'agit de permettre aux données attributaires de l'archéologue d'intégrer les données spatiales du topographe.

Il est **important** de respecter l'ordre de fonctionnement suivant **pour ne pas rencontrer de difficultés** lors de l'utilisation de la base. 

1) les archéologues dressent la liste de leurs observations dans les tables du groupe **Enregistrement Archéologique et Technique (EAT)**. Il s'agit des **faits, des US, des sondages, tranchées, isos mobilier,** etc. Reportez vous aux fiches techniques spécifiques pour la description précise de chaque table. Chaque couche de ce groupe est relié à une table spécifique de la base de données. 

2) Le topographe réalise la levée. **Il ne peut relever que les éléments enregistrés par les archéologues**.   
A l'issue de la levée, les données sont déposées dans les tables des 6 couches (6C), soit directement par le topographe, soit par les utilisateurs du projet.  
Il peut être nécessaire de vérifier les données levées par le topographe avant de les importer dans le projet. On vérifiera ainsi qu'elles sont bien formatées selon les 6 couches d'une part, et que les données ont bien été crées dans les couches EAT d'autre part.  
Une fois ces éléments verifiés, il suffit alors de copier les données depuis les couches du topographe et de les coller dans les couches du groupe 6 couches du projet Badass. 

3) **La géométrie des enregistrements déposés dans les 6 couches est automatiquement copiée dans les tables respectives du groupe EAT** : les poly sont versés dans les faits ou les us selon leur nature, les ouvertures dans les tranchées ou les sondages, etc.  On répètera ce processus pour chaque levée Topo. 

4) Le traitement des données, par les archéologues, **continuera de se dérouler dans les couches du groupe EAT**. Si des difficultés apparaissent, il est très probable que ceci soit dû à des informations manquantes ou inexactes entre les couches du groupe EAT et celle du groupe des 6 couches. 