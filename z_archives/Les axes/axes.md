# Les axes

Deux couches permettent de gérer les axes implantés : ***axe***, dans le groupe 6 couches, et ***t_axe***, dans le groupe Enregistrement Archéologique. 

**La numérotation de 1 à n des differents axes** implantés sur le terrain peut être un moyen de résoudre un certain nombre de difficultés que nous rencontrons habituellement, notamment pour des axes qui se recoupent, ou des axes qui recoupent plusieurs faits, ou de multiples axes au sein d'un fait, tel un fossé.  
Afin de faire correspondre ces axes avec les éléments levés par le topographe, et dans l'optique de réaliser les coupes dans Qgis, **le topographe doit alors relever ces axes selon le sens de lecture de la coupe (de gauche à droite face à la coupe) et en utilisant le numéro attribué par les archéologues**. Ces valeurs intègrent la table axe du modèle des 6 couches.  
Lors de l'import des données dans les 6 couches du projet Badass, les géométries des axes seront répliquées depuis la table axe du groupe 6 couches vers la table t_axe du groupe EAT, selon le numéro d'axe.  

## structuration

La structuration des tables est la suivante : 

**Couche *t_axe* :** 
- id_axe : attribué automatiquement par la base (nombre entier)
- numaxe (n° Axe) : le numéro de l'axe (nombre entier)
- nomaxe : le nom donné à l'axe. (texte libre)
- note (Commentaire) : texte libre de commentaire
- long (longueur (m)) : longueur de l'axe, peut être calculée
- alti (Altitude (m, NGF-IGN69)) : altitude moyenne ou maximale de l'axe

**couche *axe* :** 
- id_axe : attribué automatiquement par la base (nombre entier)
- numaxe : le numéro d'axe (texte)
- typaxe : le type d'axe relevé (texte)
- z_axe : l'atitude de l'axe (nombre réel)
- long_axe : la longueur de l'axe (nombre réel)
- date_leve : la date de la levée de l'axe (nombre entier)

## Fonctionnement des deux tables

Le principe général demeure : 
- Les données sont enregistrées par les archéologues dans t_axe, sans la donnée spatiale.  
- Puis, la géométrie des axes, relevée par le topographe, est importée dans la base de données, dans la couche axe des 6 couches. La géométrie est automatiquement crée dans t_axe selon le numéro d'axe.

Ainsi, pour créer un axe, il convient : de créer d'abord ses valeurs attributaires dans t_axe, puis de créer sa géométrie dans axe. 

Lors de la mise à jour d'une géométrie ou d'une information attributaire, dans les tables axe ou t_axe, les informations sont automatiquement répercutées de l'un à l'autre (de t_axe vers axe, de axe vers t_axe). 

La synchronisation entre les deux tables est assurée par comparaison entre les valeurs présentes dans le champ **numaxe** des deux tables. **Il est donc indispensable que le numéro d'axe donné par les archéologues soit bien le même que le numéro d'axe enregistré par le topographe**. 

## Cas de figures importants

- **modifier la géométrie d'un axe** : ceci peut être exécuté depuis la table axe ou t_axe, indifféremment, à partir du moment où les gémoétries existent bien dans les deux tables. 
- **ajouter un axe** : On enregistre d'abord ses valeurs attributaires dans t_axe, notamment son numéro, sans la géométrie. Puis, on ajoute sa géométrie dans la table axe, en repercutant bien le numéro d'axe attribué dans t_axe dans le champ numaxe de la table axe. Ex : on crée l'axe dont le numaxe est 17 dans t_axe, puis on dessine un axe que l'on nommera 17 (champ numaxe) dans axe. 
- **un axe est présent dans axe, mais pas dans t_axe** : aucune mise à jour n'est effectuée entre les axes, la base de donnée ne trouvant pas de correspondance entre les tables, même si on crée à posteriori un axe dans t_axe. Une solution peut être de : 
  - couper l'axe depuis la couche *axe* 
  - sauvegarder la couche *axe*
  - créer les valeurs attributaires (sans la géométrie) de l'axe dans *t_axe* 
  - sauvegarder la couche *t_axe*
  - coller l'axe dans la couche *axe*, en veillant à bien respecter les valeurs des champs num_axe de *t_axe*.  
- **Enregistrement de l'altitude** : la valeur d'altitude de l'axe (champs alti et z_axe) n'est répliquée que dans le sens *axe* -> *t_axe*. Il faut donc modifier les valeurs présentes dans la couche *axe* pour que soient mises à jour les valeurs d'altitude de la couche *t_axe*. Aucune modification ou création de valeur dans le champ altitude de *t_axe* ne sera conservée. 
