## Création du projet Qgis et de la base de données

Une extension Qgis est proposée pour créer un projet type et une base de données Badass. Elle permet ainsi de fournir à l'utilisateur un outil prêt à l'utilisation. 

### Installer l'extension

> **L'extension n'est fonctionnelle qu'à partir de la version 3.26 de Qgis**

Au sein du gestionnaire d'extension de Qgis : 
- Dans les paramètres du gestionnaire, si ce n'est pas encore le cas, activez les fonctionnalités expérimentales. 
- Recherchez, parmi les extensions non installées, l'extension Badass et installez là. 

![paramétrer les extensions expérimentales](ressources/extensions_experimentales.png)

### Mise à jour de l'extension

Si vous disposez d'une ancienne version de l'extension Badass et que vous souhaitez **la mettre à jour**, il faut d'abord desinstaller l'extension :  
- Ouvrez le gestionnaire d'extension
- Dans ***Installées***, selectionnez l'extension Badass, et cliquez en bas à droite sur ***Desinstaller***

Pour faire la mise à jour, il va falloir indiquer à Qgis de charger les dernières versions des extensions 
- Dans le gestionnaire d'extension, ouvrez l'onglet "Paramètres".
- Cliquez sur le bouton "Recharger le dépôt". 

Enfin, reinstallez l'extension (cf supra). 

### Ouvrir l'extension

Dans le menu Extension de Qgis, cliquer sur Dialog BADASS > BADASS extension. 
La fenêtre suivante apparait . 

![](ressources/dialog_badass1.png)

- 1 : permet de créer une nouvelle base de donnée et le projet associé. 
- 2 : Modifier une base de donnée existante - **Non Fonctionnel actuellement**
- 3 : Fermer la fenêtre

### Creer une nouvelle base de données 

Lorsqu'on a cliqué sur le bouton Créer une nouvelle base, la fenêtre suivante apparaît : 

![](ressources/dialog_badass2.png)

- 1 : Barre de progression
- 2 : Emplacement du dossier qui contiendra le nouveau projet et la base de données associée. Les trois petits points à droite permettent de choisi son emplacement. **Nous vous conseillons d'enregistrer votre base de données et votre projet Badass à la racine de votre dossier d'opération, et pas dans un sous répertoire.**
- 3 : Nom du projet. Il s'agit du nom que prendra le fichier de projet qgis (.qgz) généré par l'extension
- 4 : Nom de la base de données. L'extension génère la base spatialite et lui attribue le nom choisi dans ce champ. 
- 5 : Non fonctionnel - Des extensions pourront être ajoutées à la base de données pour traiter certains aspects particuliers

Une fois ces champs complétés, Vous pouvez cliquer sur le bouton **Créer**. 

**Créer** :  la base de données et le projet Qgis sont crées dans le répertoire indiqué en 2. La barre de progression montre l'avancée de ce travail. Une fois la barre de progression à 100%, **la fenêtre ne se ferme pas automatiquement. Il faut la fermer** (croix en haut à droite).  

**Retour** permet de revenir à la fenêtre précédente, sans conserver les informations renseignées dans les champs 1 à 4. 

**Attention !! Le projet ne s'ouvre pas automatiquement dans Qgis**. Il faut aller ouvrir le fichier de projet nommé en 3 dans le repertoire désigné en 2.  
