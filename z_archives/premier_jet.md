[toc]


# Présentation générale

Badass est un système de Base de donnée relationnelle et spatiale qui permet d'associer les données attributaires d'une opération archéologique avec les données spatiales (plans généraux et relevés divers). <br> Badass rassemble, dans **une seule et même base de données**, et dans les mêmes tables, **les informations géométriques relevées par les topographes et les données attributaires enregistrées par les archéologues**. 

Badass **assure l'intégrité des données archéologiques** qui ne sont plus séparées entre différents supports ou formats, et, par l'emploi d'un format de base de données libre, permet de considérer l'interopérabilité et la pérénité de la donnée archéologique. 

## La base de données

### caractéristiques générales

La base de données est au format **spatialite**, version cartographique de sqlite, un moteur de base de données libre. Cependant, Spatialite est fourni sans interface, qu'il faut donc créer. Qgis pouvant lire les données contenues dans une base spatialite, nous avons interfacé la base de données Badass avec Qgis pour alimenter et interroger la base de données. 

> **note** : la base de données spatialite est encodée en UTF_8. Or Windows utilise son propre encodage, 1252, qu'il impose à Qgis. Ainsi, dans Qgis, les données contenues dans les tables de Badass peuvent présenter des caractères étranges, dus à une mauvaise interprétation des accents. Vérifiez dans ce cas que les couches sont bien encodées en UTF_8 (dans les propriétés de la couche, onglet source). Il est également possible d'imposer à Windows l'usage de l'UTF_8, mais c'est une autre histoire...

Les différentes tables de la base de données étant affichées comme des couches dans Qgis, elles seront décrites lors de la présentation du projet (cf infra).

### Les déclencheurs (triggers)

De nombreuses actions ont été automatisées dans la base de données, par le biais de déclencheurs qui peuvent mettre à jour ou créer des données dans une table. Ces déclencheurs sont activés lors des mises à jour, suppressions ou créations dans certaines tables et peuvent affecter d'autres tables.<br>
Via ces déclencheurs nous assurons **l'intégrité de la donnée**, et facilitons certaines opérations. 

**L'un des principes fondateur de Badass** est la réplication des données géométriques depuis les tables "6 couches" vers les tables tables attributaires dédiées (t_fait, t_us, t_sondage, etc...).  

Par exemple, si un fait est crée dans la table poly, sa forme géométrique sera automatiquement ajoutée dans la table t_fait **si, et seulement si, un enregistrement correspondant existe dans t_fait**. Toute modification sur la forme géométrique dans poly sera également répercutée. 
> *Exemple : Un fait numéroté 10 a été crée dans la table t_fait. Si on crée une forme géométrique dans poly dont le numéro (numpoly) est 10 et dont l'interprétation (typoly) est "fait", alors, la forme géométrique créee sera copiée dans la table t_fait. Cette action sera également réalisée en cas de mise à jour de la forme. En cas d'absence d'enregistrement concordant dans t_fait, aucune création ou modification de la géométrie ne sera effectuée. 

### Des vues

Des **vues** permettent d'obtenir des tableaux récapitulatifs (les us des faits par exemple, ou les relations entre us...) qui peuvent être utilisés en postfouille. Trois vues sont également prêtes à être exportées pour être utilisées dans le stratifiant.

> Si une table peut être comparée à une feuille de tableur sans aucune mise en forme, une vue serait alors un tableau croisé dynamique. Une vue peut interroger différentes tables et les croiser, effectuer des calculs, etc. Il est possible d'intégrer les données géométriques à une vue. On parle alors de vue spatiale. 

# Le projet Qgis

Le projet Qgis sert d'interface à la base de donnée et permet de créer, modifier, supprimer les données stockées dans la base Badass. 
Il permet d'interagir avec les données attributaires, mais également avec les données spatiales. 
Le projet propose pour chaque table un ou des formulaires de saisie, simplifiés. Chaque table dispose donc de ses propres formulaires, qui peuvent être modifiés et paramétrés par l'utilisateur. Les formulaires étant gérés par les styles de Qgis, il est donc possible de créer de très nombreux formulaires selon les besoins de l'utilisateur. 

## Création du projet Qgis et de la base de données

Une extension Qgis est proposée pour créer un projet type et une base de données Badass. Elle permet ainsi de fournir à l'utilisateur un outil prêt à l'utilisation. 

### Installer l'extension

> **L'extension n'est fonctionnelle qu'à partir de la version 3.26 de Qgis**

Au sein du gestionnaire d'extension de Qgis : 
- dans les paramètres du gestionnaire, si ce n'est pas encore le cas, activez les fonctionnalités expérimentales. 
- rechercher, parmi les extensions non installées, l'extension Badass et l'installer. 

![paramétrer les extensions expérimentales](ressources/extensions_experimentales.png)

### Mise à jour de l'extension

Si vous disposez d'une ancienne version de l'extension Badass et que vous souhaitez **la mettre à jour**, il faut d'abord desinstaller l'extension. 
- ouvrez le gestionnaire d'extension
- selectionnez l'extension Badass précedemment installée, et cliquez en bas à droite sur "desinstallez"

Pour faire la mise à jour, il va falloir indiquer à Qgis de charger les dernières versions des extensions 
- dans le gestionnaire d'extension, ouvrez l'onglet "Paramètres".
- Cliquez sur le bouton "Recharger le dépôt". 
- Enfin, reinstallez l'extension (cf supra). 

### Ouvrir l'extension

Dans le menu Extension de Qgis, cliquer sur Dialog BADASS > BADASS extension. 
La fenêtre suivante apparait . 

![](ressources/dialog_badass1.png)

- 1 : permet de créer une nouvelle base de donnée et le projet associé. 
- 2 : Modifier une base de donnée existante - **Non Fonctionnel actuellement**
- 3 : Fermer la fenêtre

### Creer une nouvelle base de données 

Lorsqu'on a cliqué sur le bouton Créer une nouvelle base, la fenêtre suivante apparaît : 

![](ressources/dialog_badass2.png)

- 1 : Barre de progression
- 2 : Emplacement du dossier qui contiendra le nouveau projet et la base de donnée associée. Les trois petits points à droite permettent d'accéder à la navigation au sein de répertoires
- 3 : Nom du projet. Il s'agit du nom que prendra le fichier qgz généré par l'extension
- 4 : Nom de la base de données. L'extension génère la base spatialite et lui attribue le nom choisi dans ce champ. La base se situe dans le même répertoire que le projet. 
- 5 : Non fonctionnel - Il sera possible d'ajouter de nouvelles tables à la base de donnée pour traiter certains aspects

**Créer** :  la base de données et le projet Qgis sont crées dans le répertoire indiqué en 2. La barre de progression montre l'avancée de ce travail. Une fois la barre de progression à 100%, **la fenêtre ne se ferme pas automatiquement. Il faut la fermer** (croix en haut à droite).  

**Retour** permet de revenir à la fenêtre précédente, sans conserver les informations renseignées dans les champs. 

**Attention !! Le projet ne s'ouvre pas automatiquement dans Qgis**. Il faut aller chercher le fichier de projet nommé en 3 dans le repertoire désigné.  


## Présentation du projet Qgis

## Les propriétés du projet : les relations. 

Parmi les propriétés du projet badass.qgz, les Relations (Projet>Propriétés>Relations) indiquent quelle table est liée à quelle autre, quels sont les champs qui unissent les deux tables, et quel est le type de cardinalité.  

**La cardinalité** definit le **type de relation** qui unit deux tables.  

Ainsi, une table "Fait" est unie par une **cardinalité de 1 à n** (ou 1 à plusieurs) à une table "us" (pour un fait, il peut y avoir plusieurs us). Dans ce cas, on dit que la table US est une table fille de la table fait, elle même considérée comme la table parent. 

Une table photo est liée à la table fait par une **cardinalité de n à n** : pour un fait, il peut y avoir plusieurs photos, et pour une photo, il peut y avoir plusieurs faits.  La cardinalité de n à n est renseignée par **l'utilisation d'une table spécifique**, dite "table de relation", qui renseigne, pour poursuivre notre exemple, dans un champ le numéro du fait concerné, et dans un autre, le numéro de la photo concernée. 

Grâce à ces relations, il est possible, par exemple de renseigner les US d'un fait depuis le formulaire des faits.  

Le projet Qgis comporte déjà un certain nombre de relations, qui, normalement, couvrent l'ensemble des usages. 

S'il est possible de modifier, ajouter, ou supprimer des relations, **nous vous conseillons de ne le faire qu'en parfaite connaissance de cause**. Vous pouvez, pour ce faire, vous reporter au [guide complet de la base de données](https://projet-r-d-bddsarcheo.gitlab.io/bdds_archeo/reseauSIG_BADASS.html).

## Les couches du projet

Les tables de la base de données Badass sont présentes dans le projet Qgis : les données attributaires, les 6 couches, les tables de relation, les tables pour les coupes, les vues. 
Si nous présenterons ces couches succinctement, ainsi que quelques champs au gré de fiches techniques, le détail de l'ensemble des champs pour chaque table peut-être consulté [ici](https://projet-r-d-bddsarcheo.gitlab.io/bdds_archeo/reseauSIG_BADASS.html). 

Comme dans tout projet, il vous est possible de rajouter des couches, tel des fonds de carte IGN ou d'autres shape. La suppression de couche est également possible, mais ceci ne doit pas être réalisé sur les couches du groupe EAT, et sur les couches du groupe 6 couches.  
Il est possible de masquer les couches, en les décochant, de réorganiser le projet, les groupes, et de ne laisser visibles que les couches dont vous pourriez avoir besoin. 

> **En bref, réorganisez, déplacez, groupez, dégroupez, cochez ou décochez, mais ne supprimez des couches du projet qu'en pleine connaissance de cause**.

![couches d'un projet Badass](ressources/couches_projet_badass.png)


### Le groupe CREATION RAPIDE 
Sont rassemblées ici des tables permettant de créer des enregistrements plus rapidement qu'avec les tables du groupe "Enregistrement archéologique et technique". 

- entite_rapide : pour créer rapidement une entité tel un fait, une us, du mobilier 
- serie : pour définir la numérotation des faits, us, mobilier qui seront crées
- vue_numaximum_serie : vue qui permet de calculer le numéro maximal d'une serie donnée
- vector_layers : couche système qui liste les couches possedant une géométrie.

### la couche erreur_geometry

Il s'agit d'une vue, c'est à dire une couche calculée, qui permet d'identifier les erreurs de géométrie sur l'ensemble des couches géométriques du projet. 

### Le groupe Enregistrement archéologique et technique

Il comporte les couches permettant l'enregistrement des données attributaires de terrain : 

* t_axe : enregistrement des axes
* t_fait : enregistrement des faits
* t_log : enregistrement des logs
* t_mobilier : enregistrement primaire du mobilier, avant études spécialistes.
* t_prelevements : enregistrement des prélèvements
* t_sondage : enregistrement des sondages
* t_ens : enregistrement des ensembles
* t_tranchee : enregistrement des tranchées
* t_us : enregistrement des Unités Stratigraphiques

L'icone devant le nom de certaines de ces tables indique la présence d'une géométrie. C'est le cas par exemple pour la couche t_fait, ou la couche t_us.

#### Le sous-groupe documentation

Il rassemble les couches permettant l'inventaire des photos, des minutes, ainsi que deux tables concernant les levées photogrammetriques. 

* t_minute : enregistrement des minutes (pas de géométrie)
* t_photo : enregistrement des photos (pas de géométrie)
* t_photogram : enregistrement des modèles photogramétriques
* t_prodphotogram : table d'enregistrement des rasters issus des traitements photogrammétriques

La couche thesaurus_badass permet de lister l'ensemble du vocabulaire utilisé. 

#### le sous-groupe chronostratigraphie

Dans le groupe Chrono-Stratigraphie, trois couches, non géométriques, permettent d'enregistrer les séquences, phases et périodes. Il s'agit d'un système simple de liste, sous forme de tableau ou de formulaire. 

- On ajoutera une séquence en enregistrant son numéro, son nom et sa datation
- On ajoutera une phase en enregistrant son numéro, son nom, sa datation, un TPQ et un TAQ
- On ajoutera une période en renseignant son numéro, son nom, sa datation, un TPQ et un TAQ. 

#### Le sous-groupe Equipe

Il comporte : 
- une couche pour inventorier les membres de l'équipe de fouille et de post fouille
- une couche pour lister les taches effectuées soit sur le terrain, soit en post fouille. 

#### Le sous-groupe coupe

Il rassemble les tables permettant le dessin des coupes dans Qgis. La méthodologie concernant leur utilisation est présentée dans un tutoriel complet : 
[Les coupes dans Qgis](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/coupe_SIG/coupes_QGIS.html)

#### Le sous groupe Table de jonction
Le groupe table de jonction rassemble les tables permettant d'établir les relations entre les tables du groupe Enregistrement archéologique. 

* j_equipe : relation entre les membres de l'équipe et les photos, prélevements, minutes...
* j_equipe_tache : relation entre les membres de l'équipe et les différentes taches
* j_seq_phase : relations entre les séquences et les phases
* j_phase_per : relations entre les phases et périodes
* j_rel_axe : relations entre les axes et les faits, les sondages, les tranchées, les us
* j_rel_ens : relations entre les ensembles, les faits, les us 
* j_us_log : relation entre les logs et les us
* j_rel_us : relations entre les US.
* j_rel_fait : relations entre les faits (recoupements)
* j_rel_minute : relations entre les minutes et axes, ensembles, faits, minutes, iso, logs, sondages, tranchées, et us (quelle us, fait, iso sur quelle(s) minute(s)...)
* j_rel_photo : relations entre les photos et les ensembles, fait, iso, log, modeles photogrametriques, sondages, tranchées, us 
* j_rel_photogram : relations entre les photogramétries et les points d'appuis
* j_rel_sondage : relations entre les sondages, et les faits, les logs, ou les us. 
* j_rel_tranchee : relations entre les tranchées et les faits, les logs, ou les us. * 

### Le groupe 6 couches

Il s'agit des **6 couches** du modèle utilisé à l'Inrap, à savoir les couches Poly, Axe, Point, Log, Ouverture, et emprise. **Les données de ces tables sont crées par le topographe.** Plus de détails sur ces couches peuvent être trouvés [ici](https://docs.google.com/a/inrap.fr/viewer?a=v&pid=sites&srcid=aW5yYXAuZnJ8cmVzZWF1LXJlZmVyZW50cy1zaWd8Z3g6MTY2NTliYzRjNzc1Yjk4Yg). 

### Les requêtes

Nous avons rassemblé ici un certain nombre de requêtes précalculées. Il s'agit de vues, générées par la base de données spatialite, qui syntétisent certaines données. 

> contrairement aux couches, les vues ne sont pas modifiable. Il s'agit en quelque sorte de l'équivalent d'un tableau croisé dynamique dans excel. Vous pouvez les supprimer du projet si elles ne vous intéressent pas. 


## Principe de fonctionnement du projet Qgis

Au cours de l'opération de terrain, **on enregistre d'abord les données dans les tables archéologiques (enregistrement archéologique et technique) avant d'intégrer les données topographiques dans les 6 couches**. 

Ainsi : 

1. **Au cours de la fouille**, les équipes procèdent à la numérotation des vestiges identifiés. Ils attribuent des numéros de faits, d'US, de sondage, de tranchées, d'iso et enregistrent leurs caractéristiques, leurs relations. L'ensemble de ces informations sont saisies dans les couches dont le nom commence par T (t_fait, t_us, t_tranchée...). Si ceci n'est pas effectué sur le terrain, l'intégration des données topographiques ne pourra être réalisé. 
2. Les éléments relevés par le topographe sont transmis à l'équipe **sous forme de shapefile**. Ces données sont **copiées et collées dans les 6 couches du projet qgis**. Cette action permet la mise à jour de certaines données grâce à l'action des déclencheurs (mise à jour ou création de géométrie).
3. En **post fouille**, les informations recueillies dans les tables "t_" sont complétées par l'équipe si nécessaire. Il est également possible de réaliser les coupes grâce aux couches du projet prévues à cet effet (coupe_axe, coupe_line et coupe_poly). 

Notons que, comme tout projet Qgis, il est possible d'ajouter des couches provenant d'autres sources. 



## Les formulaires du projet

Chaque couche dispose de son propre formulaire qui a été paramétré afin de proposer une mise en page plus organisée, et permettre l'usage de listes déroulantes ou d'afficher les fiches en relation. Nous pourrons ainsi afficher les us présentes dans le fait, ou les photos du fait, etc. Ces formulaires peuvent être modifiés par l'utilisateur selon ses besoins (propriétés>formulaire d'attributs) après la cration d'un nouveau style. 




## fiches techniques

### Creer une entité rapide
#### la couche "serie"
On définit tout d'abord une série, dans la table serie. Ceci permet de paramétrer la table "entite rapide". 

![formulaire de la table serie](ressources/table_serie.png)

- "table cible" permet de définir la table dans laquelle seront crées les enregistrements. Les tables pouvant recevoir cette information sont t_axe, t_ens, t_fait, t_log, t_mobilier, t_prelevement, t_sondage, t_tranchee et t_us
- "Nombre de Départ" permet de définir la valeur minimale (on peut décider de commencer à 1000 par exemple)
- "Nombre d'arrivée" permet de définir la valeur maximale
- "Pas d'incrémentation" définit le pas, qui peut être de 1 ou de 2, ou autre. Ainsi, une série de 1 à 10 avec un pas de 2 créera les enregistrements 1, 3, 5, 7 et 9
- "Commentaires" est un texte libre. 

#### La couche "entite rapide"
Elle permet, au sein d'une série, d'ajouter un numéro d'enregistrement. 


### Enregistrer une entité fille

Pour créer une entité fille au sein d'un sous formulaire, il faut activer le mode édition (1), puis créer une nouvelle entité (3). On peut egalement lier une entité fille avec l'enregistrement en cours (4). Lorsque les modifications ont été réalisées, il faut enregistrer les modifications (2) et quitter le mode edition (1).

![entite fille](ressources/entite_fille.png)

**Attention !** Lors de la création d'une entité fille, depuis le sous formulaire, juste après sa création, qgis sélectionne la première entité enregistrée dans le sous formulaire, et non l'entité nouvellement créée. **L'utilisateur se retrouve donc à modifier des valeurs déjà enregistrées, et non créer de nouvelles valeurs**. Nous conseillons d'utiliser les sous formulaires en mode tableau, et de formater le tableau selon ce que l'on souhaite afficher, comme dans l'exemple ci-dessous. 

![entite fille](ressources/entite_fille_liste.png)

### Fiche technique 1 : enregistrer un fait

Ouvrir le formulaire de la table t_fait et se placer en mode édition pour créer un nouvel enregistrement ou modifier un enregistrement existant. On utilisera dans ce cas l'icone d'ajout d'entité (2) 

<img src="ressources/ajout_entite.png"  width="500">

* **Le numéro du fait** (nombre) est renseigné en haut. 
* **L'interprétation du fait** est renseignée en dessous, avec deux champs d'interprétation possibles, et une case à cocher en cas de caractère douteux. 
Un second groupe de champs permet de réaliser le suivi de la fouille et de l'enregistrement des faits : fouillé, relevé, topographié, photographié, annulé, sous forme de cases à cocher ou listes déroulantes

Un groupe d'onglet permet l'enregistrement des informations métriques du fait, de réaliser la description du fait (formes et profils), de renseigner la datation estimée, et récapitule les us contenues dans le fait ainsi que les photos réalisées. 
Ces deux derniers onglets se présentent sous la forme de sous formulaires qui permettent la consultation des données liées, mais également leur modification ou leur création. 

### Fiche technique 2 : enregistrer une us

La fiche US comprend : 
* **le numéro d'US** : nombre entier à renseigner obligatoirement.
* **le numéro du fait** : si l'us appartient à un fait, la liste des faits déjà enregistrés dans la table t_fait est proposée. 
* **Le résumé stratigraphique**. Il montre les relations US saisies. Il n'est pas possible de modifier ces informations à ce niveau.    
* **Onglet identification** : 
    * un champ "type" avec trois valeurs possibles : "couche", pour les US positives, "négatif" pour les US négatives, et "altération". 
    * un champ "nature" qui permet de qualifier le type de couche choisi (abandon, construction, destruction...). 
    * un champ "interprétation" pour préciser la nature de la couche. 
* **Onglet "relations us"** : il permet de renseigner les relations de l'us, qui sont enregistrées dans la table j_rel_us. Cette table 
    * 
**Attention ! pour un bon fonctionnement de la base, il ne faut pas supprimer les US, mais les annuler.** 

### fiche technique : importer les données topo. 

#### Principe général
Les données topographiques peuvent être fournies par le topographe sous de nombreux formats, dont le shp. 
Pour permettre la conservation de l'ensemble des données sous un seul et même format, ainsi qu'une bonne intégration des données topographiques, il convient de les importer dans la base de donnée Badass, de préférence dans un groupe spécifique.  

Si ces données **respectent le format des 6 couches**, on vérifiera tout d'abord que les champs "typoly" de la table poly et "typouvert" de la ouverture sont correctement renseignés. "Typoly" doit être renseigné 'fait' ou 'us', "typouvert" 'tranchee' ou 'sondage'. 

On veillera également à ce que la numérotation des odifférent objets relevés par le topographe (faits, us, ouvertures, isos) et la numérotation des même objet des couches "t_" du projet badass enregistrées par les équipes de terrain concordent. Ains, si un fait a été numéroté "10" dans t_fait, il doit également être numéroté "10" dans la couche poly du topographe (et non F10, ou F.10, ou FO10..) 

Il suffira ensuite de copier et coller les données depuis les couches fournies par le topographe vers les couches du projet badass : 
* poly du topo vers poly badass
* emprise du topo vers emprise badass
* ouverture du topo vers ouverture badas
* etc... 

A l'issue de cette opération, les déclencheurs copieront les données geométriques dans les table prefixées "t_" en fonction de la nature des objets géométriques. 
* Les données de type 'fait' dans la couche poly seront copiées dans la couche t_fait. 
* Les données de type 'us' dans la couche poly seront copiées dans le couche t_us.
* les données de type 'sondage' dans la couche ouverture seront copiées dans la couche t_sondage.
* les données de type 'tranchée' dans la couche ouverture seront copiées dans la couche t_tranchée. 

La mise à jour ou la suppression d'entités dans les tables poly et ouvertures seront répercutées dans les tables t_fait, t_us, t_sondage et t_tranchee selon leur type. 

Si les données **ne respectent pas le format des 6 couches**, il faudra intégrer les données topographiques dans les 6 couches, manuellement. On pourra, par exemple, importer les points topo dans une nouvelle couche (topo par exemple) et dessiner, dans chacune des 6 couches les entités selon leur type. 

#### exemple d'importation des données topo



### Fiche technique : modifier le thésaurus

Le thésaurus de Badass permet de générer les listes de valeurs présentes dans les différents formulaires du projet. 
Il fournit une liste de mots qui peuvent s'appliquer à certains champs

La création d'une liste de valeur, à partir des données du thésaurus, est effectuée dans l'outil de génération des formulaires, dans les propriétés de la couche. Rappelons que les formulaires, à l'instar de la symbologie, sont gérés par un style. En multipliant les styles, on peut donc concevoir de nouveaux formulaires, sans effacer les anciens. 

Pour créer une liste de valeur s'appuyant sur les données contenues dans le thésaurus, il est nécessaire de connaître la structure du thésaurus
 
* id : numéro d'id de l'enregistrement. Ce champ est obligatoire et aucun enregistrement ne sera effectué tant que le champ n'aura pas été renseigné par une valeur unique. 
* tabl_thes : il s'agit de renseigner le nom de la table concernée. C'est une indication qui permettra de filtrer la table
* field_thes : indique à quoi est destinée la valeur renseignée. Ce nom sera appelé par le champ qui contient la liste de valeur. 
* val_thes : il s'agit de la valeur qui sera affichée dans le champ 
* def_thes : permet de décrire la valeur. 
* cat_thes : catégorie de regroupement. Ces catégories peuvent être utilisées pour affiner les filtres d'affichage. C'est particulièrement le cas pour le mobilier, cat_thes renseignant le type de mobilier (céramique, lithique, etc.).
* comment : commentaire divers. 

Il est ainsi possible de modifier une des valeurs, ou en créer une nouvelle. 
Il faut alors récupérer le nom de la table concernée, le nom du champ (pas son alias), et renseigner la valeur souhaitée dans le champ val_thes. 

> **Exemple :** 
On souhaite modifier dans les faits la liste de valeur permettant l'interprétation du fait.
on ouvre la table attributaire de Thesaurus Badass.  
Il n'est pas nécessaire de renseigner le champ id_thes.  
On renseigne le nom de la table dans tabl_thes. Ici, t_fait.  
Le nom du champ concerné est renseigné dans field_thes. Le nom du champ peut être trouvé dans les propriétés de la couche, onglet "champs". Ici, il s'agit du champ interpret.  
La valeur du champ est renseignée dans val_thes. Nous créons la valeur "Fosse polylobée".  
Ici, ni def_thes ni cat_thes ne sont renseignés, et aucun commentaire particulier ne semblent nécessaires. 

![Création d'un nouvel enregistrement dans le thesaurus](ressources/modif_thesaurus_badass.png)


### Créer une liste de valeur à partir des éléments du thésaurus

Pour appliquer une liste de valeur à un champ, basée sur les éléments enregistrés dans le thésaurus, il faut modifier le formulaire. 

    propriété de la couche > onglet formulaire > choisir le champ. 

Notons que les listes de valeur sont générées par les formulaires des couches, en utilisant le type d'outil "valeur relationnelle". 

<img src="ressources/modif_formulaire_relationnel.png"  width="1200">

**Les paramètres de l'outil valeur relationnelle sont les suivants :**
- couche : thesaurus_badass, 
- clé (la valeur affichée) : "val_thes"
- colonne de valeurs (valeur enregistrée) : "val_thes". 

Il faut ensuite trier les valeurs du thésaurus, sans quoi, toutes les valeurs présentes dans le thésaurus seront affichées.  
Dans l'exemple ci-dessus, dans "filtrer l'expression", nous indiquons : "tabl_thes" like 't_fait' and "field_thes" like 'interpret'. 
C'est à dire, nous souhaitons n'afficher que les enregistrements qui comportent la valeur 't_fait' dans le champ "tabl_thes" ET la valeur 'interpret' dans le champ "field_thes". 


![creer une liste de valeur](ressources/valeur_relationnelle.png)

### Fiche technique : modifier les formulaires

voir [Les formulaires](https://formationsig.gitlab.io/sig31/pas_a_pas/SIG31_BDD.html#4-cr%C3%A9ation-de-formulaires)

Référence de la relation : affiche les valeurs parents possibles. Ex : dans une table us, en sélectionnant "Référence de la relation" dans le champs "fait", et en indiquant comme expression d'affichage "numfait", permet d'afficher l'ensemble des numéros de fait puisqu'il existe une relation entre les faits et les US. 

### Fiche technique : les relations stratigraphiques. 




### Fiche technique : les photos





##Thesaurus : 

**Base de donnée relationnelle** : une base de donnée de relationnel est un ensemble de table liées les unes avec les autres par la présence, dans chacune des tables, d'éléments communs. 

**SGBDR** : un Système de Gestion de Base de Donnée Relationnelle est un logiciel qui permet de créer utiliser et maintenir des bases de donnée relationnelle. Parmi les SGBDR, nous pouvons citer FileMaker, Access, Sqlite ou Postgresql. 

**Table** : dans une base de donnée, une table est un ensemble de données organisées sous forme d'un tableau où les colonnes correspondent à des catégories d'information et les lignes à des enregistrements. On peut comparer une table à une feuille de classeur excel ou libre office. 



