## Les photos dans Badass

### Les couches concernées

Plusieurs couches permettent de gérer les photos. 

La première se trouve dans **le sous groupe documentation du groupe Enregistrement Archéologique de Terrain**, et s'appelle **t_photo**. 

Elle comprend les champs :
- **id_photo** : id numérique de 1 à n, crée automatiquement
- **numphoto** (N°Photo) : votre numéro de photo de 1 à n
- **nomfichier** (Nom de fichier) : c'est le nom de votre fichier photo auquel vous souhaitez faire référence. Ex : D123456_0010.JPG. Pour des questions de praticité, nous vous recommandons de nommer vos fichiers photos en conservant le n° de la photo. 
- **chemin_url** (chemin vers le fichier) : ce champ est destiné à ceontenir l'adresse complète du chemin vers le fichier, tel que /donnee/erengistrement/D123456_0010.JPG
- **Legend** (Légende) : il s'agit de la légende de la photo, telle qu'elle devrait être présentée dans le rapport final
- **vue_vers** (vue vers) : désigne l'orientation de la photo. 
- **creator** (Auteur) : le nom de l'auteur de la photo
- **datephoto** (Date du Cliché) : la date à laquelle la photo a été prise
- **type** (type) : ceci décrit l'objet générique de la photo : ambiance, coupe, plan, détail, mobilier... La liste des valeurs employées est définie par le thésaurus 
- **support** (support) : le support de la photo, soit argentique, soit numérique
- **destination** (Destination/objectifs du cliché) : ce à quoi devrait servir la photo (valorisation, figure...)
- **note** (commentaires) : remarques et commentaires concernant la photo. 

Au sein de ces champs, le seul champ obligatoire est l'id_photo, qui doit en outre être unique. Le numphoto . 



### Visualiser les photos

Il est possible **d'utiliser Qgis pour visualiser les photos**. Quelques conditions doivent cependant être remplies en amont.  

Pour simplifier la tache, et plutôt que d'avoir à aller chercher chaque photo (ce qui serait long et fastidieux), il est plus facile d'utiliser les données présentes dans l'inventaire. Celui-ci doit donc être complété et les champs "nomfichier" et "chemin_url" renseignés.  Qgis utilisera ces données pour aller chercher les photos dans les repertoire de l'opération, et les affichera. 

Chemin_url doit être le chemin relatif des photos par rapport à l'emplacement du projet Qgis. Ainsi, si vos photos sont dans le sous repertoire donnée/image, vous indiquerez, dans ce champ : donnee/image/le_nom_de_votre_fichier.jpg

> exemple : donnee/image/D134387_0001.JPG

Si ce n'est pas déjà fait, vous devez ensuite paramétrer le formulaire (clic droit sur la couche > propriétés > formulaire d'attributs). 

Selectionnez le champ "Chemin_Url" et choisissez le type "Pièce Jointe".  
Indiquez le chemin par défaut (le répertoire où se trouve votre projet Qgis).   Choisissez "Relatif au chemin du projet" face à "Stocke le chemin comme", et "Chemins des Fichiers" comme "Mode d'enregistrement".

> Vous pouvez, dans chemin par défaut, écrire : @project_folder. C'est l'expression qui permet de dire à Qgis qu'il doit chercher à partir du dossier où se situe le projet. 

Les cases *Afficher le chemin de la ressource*, *Afficher un bouton de sélection de fichier*, *Utiliser un hyperlien pour le chemin du document* sont cochées. 

Le visualisateur de document interne sera sélectionné sur Image, avec une largeur Auto et une auteur que vous choisirez (ou inversement)

![](ressources/piece_jointe.png)

Dès lors, si vous n'avez pas d'erreur quant au nom de fichier ou à leur emplacement dans votre table atributaire (chemin_url), vous pourrez voir vos photos dans le formulaire photo, et les ouvrir en cliquant sur le lien. 

### Les éléments à renseigner



### Lier des photos et des faits




### FAQ

#### Comment dois-je nommer mes photos ? 
De la façon la plus simple possible. Contrairement à ce qui est parfois préconisé, il ne faut pas indiquer dans le nom de la photo ce qu'elle contient. C'est l'inventaire photo qui précise le contenu de la photo.  
La préconisation est de nommer les photos de 1 à n (0001.jpg, 0002.jpg...) en ajoutant en prefixe du nom le code de l'opération. Ex : D123456_0001.jpg

#### Enregistrer 3517 photos, ca va être long...

En effet, Qgis n'est pas très ergonomique pour cela. Excel (ou mieux, LibreOffice Calc) est votre ami. 

- Créez une feuille excel comportant les mêmes champs que la table attributaire de t_photo, sauf id_photo : numphoto, nomfichier, chemin_url, legend, vue_vers, creator, datephoto, type, support, destination, note. 
- Remplissez votre tableau excel. 
- Une fois votre fichier excel complété, vous pouvez simplement copier les données de votre tableau excel, ouvrir la table attributaire dans Qgis, passer en mode édition, et coller les données dans la table attributaire. Vérifiez que l'ordre d'affichage des champs dans Qgis est bien le même que dans votre tableau excel/libreOffice.  
Il se peut que Qgis vous mette un message d'alerte concernant des enregistrements invalides. Vous pouvez coller en ignorant les enregistrements invalides. Quelques corrections seront alors sans doute nécessaires.

### J'ai 4501 photos, je ne vais pas tout renommer à la main. 
Rassurez vous, nous non plus.  
Il existe un grand nombre de logiciels permettant le renommage de fichiers. L'un d'entre eux, NB_renammer, disponible sur le microsoft Store, vous permet de remplacer des caractères à la volée, rajouter des caractères en début ou fin de nom de fichier, etc.  
Vous pouvez, à l'aide de ce genre de logiciel, rapidement renommer toutes vos photos en intégrant votre numéro d'opération, un numéro de série de 1 à n, etc.  
Dans certains cas, il est également possible de paramétrer la façon dont votre appareil photo numérique nomme les photos. 